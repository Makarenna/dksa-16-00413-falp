﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.OleDb;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using library.common;
using library.xml;
using System.IO;
using System.Xml;

namespace WcfST
{
    public class carga_archivo_temporal
    {
        #region Funciones
        private string GetConnectionStringOldb(string RutaArchivo)
        {
            //return "Provider=Microsoft.Jet.OLEDB.4.0;Data Source=" + RutaArchivo + @";Extended Properties=""Excel 8.0;IMEX=1""";//parche para columnas con tipos de datos mesclados
            return "Provider=Microsoft.ACE.OLEDB.12.0;Data Source=" + RutaArchivo + @";Extended Properties=""Excel 12.0;HDR=YES;IMEX=1"";";
        }
        private DataColumn CreaColumnaIndice()
        {
            DataColumn column = new DataColumn();
            column.DataType = System.Type.GetType("System.Int32");
            column.ColumnName = "Fila";
            column.AutoIncrement = true;
            column.AutoIncrementSeed = 1;
            column.AutoIncrementStep = 1;
            return column;
        }
        private DataColumn CreaColumnaInt(string Nombre)
        {
            DataColumn column = new DataColumn();
            column.DataType = System.Type.GetType("System.Int32");
            column.ColumnName = Nombre;
            column.DefaultValue = 0;

            return column;
        }
        private DataColumn CreaColumnaString(string Nombre, string Default)
        {
            DataColumn column = new DataColumn();
            column.DataType = System.Type.GetType("System.String");
            column.ColumnName = Nombre;
            column.DefaultValue = Default;
            return column;
        }
        #endregion Funciones
        public string cargar_archivo_temporal(string NombreArchivoExcel, string TipoArchivo, string AgnoCarga, string PeriodoCarga, string MesCarga)
        {
            DataTable DatosArchivoExcel = new DataTable("table");
            string resultado = string.Empty;

            try
            {
                if (Convierte_Excel_Dataset(NombreArchivoExcel, TipoArchivo, AgnoCarga, PeriodoCarga, MesCarga, ref DatosArchivoExcel))
                    if (Carga_archivo_temporal_tabla(TipoArchivo, DatosArchivoExcel))
                    {
                        resultado = "OK";
                    }
            }
            catch (Exception ex)
            {
                resultado = "NOK";
                new Log().GuardaLog("cargar_archivo_temporal", ex.Source, ex.Message, ex.StackTrace);
            }

            return resultado;
        }
        private string SelectHoja(string hoja, string TipoArchivo)
        {
            string retorno = string.Empty;

            switch (TipoArchivo)
            {
                case "CAA_ASIGNATURAS_TRANS":
                    retorno = "select * from [" + hoja + "] WHERE NOT ([CARRERA código SAGE] = '')";
                    break;
                case "CAA_ASISTENCIA_DOCENTE":
                    retorno = "select * from [" + hoja + "] WHERE NOT ([Año] = '')";
                    break;
                case "CAA_CURRICULUMS_INN":
                    retorno = "select * from [" + hoja + "] WHERE NOT ([] = '')";
                    break;
                case "CAA_EERR":
                    retorno = "select * from [" + hoja + "] WHERE NOT ([] = '')";
                    break;
                case "CAA_ENCUESTA_CARACT":
                    retorno = "select * from [" + hoja + "] WHERE NOT ([Marca temporal] = '')";
                    break;
                case "CAA_ENCUESTA_PERCEP":
                    retorno = "select * from [" + hoja + "] WHERE NOT ([Marca temporal] = '')";
                    break;
                case "CAA_ENCUESTA_TITU":
                    retorno = "select * from [" + hoja + "] WHERE NOT ([Nombre de la carrera] = '')";
                    break;
                case "CAA_ESCUELAS_CARRERAS":
                    retorno = "select * from [" + hoja + "] WHERE NOT ([Escuela código SAGE] = '')";
                    break;
                case "CAA_EVAL_360_COO":
                    retorno = "select * from [" + hoja + "] WHERE NOT ([Sede] = '')";
                    break;
                case "CAA_EVAL_360_DIR":
                    retorno = "select * from [" + hoja + "] WHERE NOT ([Sede] = '')";
                    break;
                case "CAA_FORMACION_DOC":
                    retorno = "select * from [" + hoja + "] WHERE NOT ([RUT Docente] = '')";
                    break;
                case "CAA_GASTOS":
                    retorno = "select * from [" + hoja + "] WHERE NOT ([] = '')";
                    break;
                case "CAA_INFO_OF_NACIONAL":
                    retorno = "select * from [" + hoja + "] WHERE NOT ([] = '')";
                    break;
                case "CAA_NUEVOS_BECA_1RA":
                    retorno = "select * from [" + hoja + "] WHERE NOT ([] = '')";
                    break;
                case "CAA_NUEVOS_BECA_2DA":
                    retorno = "select * from [" + hoja + "] WHERE NOT ([] = '')";
                    break;
                case "CAA_NUEVOS_CAE":
                    retorno = "select * from [" + hoja + "] WHERE NOT ([] = '')";
                    break;
                case "CAA_PERSONAL_SIES":
                    retorno = "select * from [" + hoja + "] WHERE NOT ([Algoritmo DV] = '')";//segunda columna
                    break;
                case "CAA_PRACTICA_ENCUESTA":
                    retorno = "select * from [" + hoja + "] WHERE NOT ([AP_PATERNO] = '')";
                    break;
                case "CAA_PRACTICA_PRACTICA":
                    retorno = "select * from [" + hoja + "] WHERE NOT ([TIPO DE PRÁCTICA] = '')";
                    break;
                case "CAA_RENOVANTES_BECA":
                    retorno = "select * from [" + hoja + "] WHERE NOT ([] = '')";
                    break;
                case "CAA_RENOVANTES_CAE":
                    retorno = "select * from [" + hoja + "] WHERE NOT ([RUT] = '')";
                    break;
                case "CAA_RENOVANTES_HISTO_CAE":
                    retorno = "select * from [" + hoja + "] WHERE NOT ([] = '')";
                    break;
                case "CAA_SATISF_TUTORIA":
                    retorno = "select * from [" + hoja + "] WHERE NOT ([] = '')";
                    break;
                case "CAA_SIES_BIBLIOTECAS":
                    retorno = "select * from [" + hoja + "] WHERE NOT ([Cód. Institución] = '')";
                    break;
                case "CAA_SIES_DOCENTES":
                    retorno = "select * from [" + hoja + "] WHERE NOT ([Cód. Institución] = '')";
                    break;
                case "CAA_SIES_EXTRANJEROS_INT":
                    retorno = "select * from [" + hoja + "] WHERE NOT ([DV] = '')";
                    break;
                case "CAA_SIES_EXTRANJEROS_REG":
                    retorno = "select * from [" + hoja + "] WHERE NOT ([DV] = '')";
                    break;
                case "CAA_SIES_INMUEBLES":
                    retorno = "select * from [" + hoja + "] WHERE NOT ([Cód. Institución] = '')";
                    break;
                case "CAA_SIES_LABS_TALLERES":
                    retorno = "select * from [" + hoja + "] WHERE NOT ([Cód. Institución] = '')";
                    break;
                case "CAA_SIES_MATRICULADOS":
                    retorno = "select * from [" + hoja + "] WHERE NOT ([Algoritmo DV] = '')";
                    break;
                case "CAA_SIES_PLANES_PROGRAMAS":
                    retorno = "select * from [" + hoja + "] WHERE NOT ([Nombre de la Institución] = '')";
                    break;
                case "CAA_TITULADOS_SIES":
                    retorno = "select * from [" + hoja + "] WHERE NOT ([DV] = '')";
                    break;
                case "CAA_PROYECCION_EERR":
                    retorno = "select * from [" + hoja + "] WHERE NOT ([Sede] = '')";
                    break;
                case "CAA_EERR_SANTIAGO":
                    retorno = "select * from [" + hoja + "] WHERE NOT ([Cuenta Contable] = '')";
                    break;
                case "CAA_EERR_VIÑA":
                    retorno = "select * from [" + hoja + "] WHERE NOT ([Cuenta Contable] = '')";
                    break;
                case "CAA_INF_FIN_INT":
                    retorno = "select * from [" + hoja + "] WHERE NOT ([Concepto] = '')";
                    break;
                case "CAA_HABERES":
                    retorno = "select * from [" + hoja + "] WHERE NOT ([Rut] = '')";
                    break;
                case "CAA_PLANIF_DOCENTE":
                    retorno = "select * from [" + hoja + "] WHERE NOT ([Escuela] = '')";
                    break;
                case "CAA_DEVENGADO_ESCUELA":
                    retorno = "select * from [" + hoja + "] WHERE NOT ([Escuela] = '')";
                    break;
                case "CAA_ESTUDIANTE_TUTORIA":
                    retorno = "select * from [" + hoja + "] WHERE NOT ([RUT] = '')";
                    break;
                case "CAA_DEUDORES":
                    retorno = "select * from [" + hoja + "] WHERE NOT ([Fecha Emis.] = '')";
                    break;
            };

            return retorno; ;
        }
        private string BuscaHojaDatosArchivo(DataTable dt, string TipoArchivo)
        {
            string resultado = string.Empty;

            switch (TipoArchivo)
            {
                case "CAA_SIES_INMUEBLES":
                    resultado = dt.Rows[0]["TABLE_NAME"].ToString(); ;
                    break;
                case "CAA_SIES_LABS_TALLERES":
                    resultado = dt.Rows[0]["TABLE_NAME"].ToString(); ;
                    break;
                case "CAA_SIES_BIBLIOTECAS":
                    resultado = dt.Rows[0]["TABLE_NAME"].ToString(); ;
                    break;
                case "CAA_SIES_DOCENTES":
                    resultado = dt.Rows[0]["TABLE_NAME"].ToString(); ;
                    break;
                case "CAA_SIES_EXTRANJEROS_REG":
                    resultado = dt.Rows[0]["TABLE_NAME"].ToString(); ;
                    break;
                case "CAA_SIES_EXTRANJEROS_INT":
                    resultado = dt.Rows[0]["TABLE_NAME"].ToString(); ;
                    break;
                default:
                    //resultado = dt.Rows[dt.Rows.Count - 1]["TABLE_NAME"].ToString();
                    resultado = dt.Rows[0]["TABLE_NAME"].ToString();
                    break;
            }

            return resultado;
        }
        private bool Convierte_Excel_Dataset(string NombreExcel, string TipoArchivo, string AgnoCarga, string PeriodoCarga, string MesCarga, ref DataTable DatosArchivoExcel)
        {
            bool retorno = true;
            string RutaCargaArchivoRoot = cnfHelper.GetConfigAppVariable("RutaCargaArchivo");
            string RutaArchivoExcel = Path.Combine(RutaCargaArchivoRoot, NombreExcel);
            DataTable dtSelectExcel = new DataTable("selectExcel");

            dtSelectExcel.Columns.Add(CreaColumnaIndice());
            dtSelectExcel.Columns.Add(CreaColumnaString("FechaCarga", "2016-07-20"));
            dtSelectExcel.Columns.Add(CreaColumnaString("AgnoCarga", AgnoCarga));
            dtSelectExcel.Columns.Add(CreaColumnaString("PeriodoCarga", PeriodoCarga));
            dtSelectExcel.Columns.Add(CreaColumnaString("MesCarga", MesCarga));

            string CnnExcel = GetConnectionStringOldb(RutaArchivoExcel);

            try
            {
                using (OleDbConnection con = new OleDbConnection(CnnExcel))
                {
                    con.Open();
                    DataTable dt = con.GetSchema("Tables");
                    string Hoja = BuscaHojaDatosArchivo(dt, TipoArchivo);
                    string SelectExcel = SelectHoja(Hoja, TipoArchivo);

                    OleDbDataAdapter cmd = new System.Data.OleDb.OleDbDataAdapter(SelectExcel, con);

                    cmd.Fill(dtSelectExcel);

                    dtSelectExcel.Columns[5].ColumnName = dtSelectExcel.Columns[5].ColumnName.Replace(" ", string.Empty);

                    for (int i = 6; i < dtSelectExcel.Columns.Count; i++)
                    {
                        dtSelectExcel.Columns[i].ColumnName = string.Format("C{0}", i);
                    }

                    DatosArchivoExcel = dtSelectExcel.Clone();

                    foreach (DataColumn dc in DatosArchivoExcel.Columns)
                    {
                        //if(dc.DataType == typeof(double))
                        //{
                        //    dc.DataType = typeof(decimal);
                        //}
                        dc.DataType = typeof(string);
                    }

                    foreach (DataRow row in dtSelectExcel.Rows)
                    {
                        DatosArchivoExcel.ImportRow(row);
                    }

                    con.Close();
                }
            }
            catch (Exception ex)
            {
                retorno = false;
                new Log().GuardaLog("Convierte_Excel_Dataset", ex.Source, ex.Message, ex.StackTrace);
            }

            return retorno;
        }
        private bool Carga_archivo_temporal_tabla(string TipoArchivo, DataTable OrigenDatos)
        {
            bool retorno = true;
            List<string> MapeoColumnas = new List<string>();
            string TablaDestino = string.Empty;

            MapeoColumnas.Add("Fila|FILA");
            MapeoColumnas.Add("FechaCarga|FECHA_CARGA");
            MapeoColumnas.Add("AgnoCarga|ANIO_CARGA");
            MapeoColumnas.Add("PeriodoCarga|PERIODO_CARGA");
            MapeoColumnas.Add("MesCarga|MES_CARGA");

            TablaDestino = TipoArchivo.ToUpper();

            //Origen -- Destino
            switch (TipoArchivo.ToUpper())
            {
                case "CAA_ASIGNATURAS_TRANS":
                    #region Mapeo
                    MapeoColumnas.Add("CARRERAcódigoSAGE|CODIGO_CARRERA");
                    MapeoColumnas.Add("C6|CODIGO_ASIGNATURA");
                    #endregion Mapeo
                    break;
                case "CAA_ASISTENCIA_DOCENTE":
                    #region Mapeo
                    MapeoColumnas.Add("|ANIO");
                    MapeoColumnas.Add("C6|SEMESTRE");
                    MapeoColumnas.Add("C7|FECHA");
                    MapeoColumnas.Add("C8|RUT");
                    MapeoColumnas.Add("C9|DV");
                    MapeoColumnas.Add("C10|CODIGO_ASIGNATURA");
                    MapeoColumnas.Add("C11|HC_TOTAL");
                    MapeoColumnas.Add("C12|HC_SESION");
                    MapeoColumnas.Add("C13|ASISTE");
                    MapeoColumnas.Add("C14|REEMPLAZADO");
                    MapeoColumnas.Add("C15|RECUPERACION");
                    #endregion Mapeo
                    break;
                case "CAA_CURRICULUMS_INN":
                    #region Mapeo
                    MapeoColumnas.Add("|CODIGO_CARRERA");
                    #endregion Mapeo
                    break;
                case "CAA_EERR":
                    #region Mapeo
                    MapeoColumnas.Add("|IE");
                    MapeoColumnas.Add("C6|CLASIFICACIÓN");
                    MapeoColumnas.Add("C7|DETALLE");
                    MapeoColumnas.Add("C8|ENERO");
                    MapeoColumnas.Add("C9|FEBRERO");
                    MapeoColumnas.Add("C10|MARZO");
                    MapeoColumnas.Add("C11|ABRIL");
                    MapeoColumnas.Add("C12|MAYO");
                    MapeoColumnas.Add("C13|JUNIO");
                    MapeoColumnas.Add("C14|JULIO");
                    MapeoColumnas.Add("C15|AGOSTO");
                    MapeoColumnas.Add("C16|SEPTIEMBRE");
                    MapeoColumnas.Add("C17|OCTUBRE");
                    MapeoColumnas.Add("C18|NOVIEMBRE");
                    MapeoColumnas.Add("C19|DICIEMBRE");
                    MapeoColumnas.Add("C20|TOTAL");
                    #endregion Mapeo
                    break;
                case "CAA_ENCUESTA_CARACT":
                    #region Mapeo
                    MapeoColumnas.Add("|RUT");
                    MapeoColumnas.Add("C6|DV");
                    MapeoColumnas.Add("C7|SEDE");
                    MapeoColumnas.Add("C8|CARRERA");
                    MapeoColumnas.Add("C9|C1");
                    MapeoColumnas.Add("C10|C2");
                    MapeoColumnas.Add("C11|C3");
                    MapeoColumnas.Add("C12|C4");
                    MapeoColumnas.Add("C13|C5");
                    MapeoColumnas.Add("C14|C6");
                    MapeoColumnas.Add("C15|C7");
                    MapeoColumnas.Add("C16|C8");
                    MapeoColumnas.Add("C17|C9");
                    MapeoColumnas.Add("C18|C10");
                    MapeoColumnas.Add("C19|C11");
                    MapeoColumnas.Add("C20|C12");

                    #endregion Mapeo
                    break;
                case "CAA_ENCUESTA_PERCEP":
                    #region Mapeo
                    MapeoColumnas.Add("|RUT");
                    MapeoColumnas.Add("C6|NOMBRE_CARRERA");
                    MapeoColumnas.Add("C7|EVAL_1");
                    MapeoColumnas.Add("C8|EVAL_2");
                    MapeoColumnas.Add("C9|EVAL_3");
                    MapeoColumnas.Add("C10|EVAL_4");
                    MapeoColumnas.Add("C11|TUTORADO");

                    #endregion Mapeo
                    break;
                case "CAA_ENCUESTA_TITU":
                    #region Mapeo
                    MapeoColumnas.Add("|NOMBRE_CARRERA");
                    MapeoColumnas.Add("C6|RUT");
                    MapeoColumnas.Add("C7|ANIO_INGRESO");
                    MapeoColumnas.Add("C8|ANIO_EGRESO");
                    MapeoColumnas.Add("C9|ANIO_TITULACION");
                    MapeoColumnas.Add("C10|ENC_TIT_1_4");
                    MapeoColumnas.Add("C11|ENC_TIT_1_9");
                    MapeoColumnas.Add("C12|ENC_TIT_1_10");
                    MapeoColumnas.Add("C13|ENC_TIT_1_11");
                    MapeoColumnas.Add("C14|ENC_TIT_1_13");
                    MapeoColumnas.Add("C15|ENC_TIT_1_14");
                    MapeoColumnas.Add("C16|ENC_TIT_1_15");
                    MapeoColumnas.Add("C17|ENC_TIT_1_3");
                    MapeoColumnas.Add("C18|ENC_TIT_1_1");
                    MapeoColumnas.Add("C19|ENC_TIT_1_2");
                    MapeoColumnas.Add("C20|ENC_TIT_2_1");
                    MapeoColumnas.Add("C21|ENC_TIT_2_2");
                    MapeoColumnas.Add("C22|ENC_TIT_2_3");
                    MapeoColumnas.Add("C23|ENC_TIT_2_4");
                    MapeoColumnas.Add("C24|ENC_TIT_2_5");
                    MapeoColumnas.Add("C25|ENC_TIT_2_6");
                    MapeoColumnas.Add("C26|ENC_TIT_2_7");
                    MapeoColumnas.Add("C27|ENC_TIT_3_1");
                    MapeoColumnas.Add("C28|ENC_TIT_3_2");
                    MapeoColumnas.Add("C29|ENC_TIT_3_3");
                    MapeoColumnas.Add("C30|ENC_TIT_3_4");
                    MapeoColumnas.Add("C31|ENC_TIT_3_5");
                    MapeoColumnas.Add("C32|ENC_TIT_3_6");
                    MapeoColumnas.Add("C33|ENC_TIT_4_1");
                    MapeoColumnas.Add("C34|ENC_TIT_4_2");
                    MapeoColumnas.Add("C35|ENC_TIT_4_3");
                    MapeoColumnas.Add("C36|ENC_TIT_4_4");
                    MapeoColumnas.Add("C37|ENC_TIT_4_5");
                    MapeoColumnas.Add("C38|ENC_TIT_4_6");
                    MapeoColumnas.Add("C39|ENC_TIT_4_7");
                    MapeoColumnas.Add("C40|ENC_TIT_4_8");
                    MapeoColumnas.Add("C41|ENC_TIT_4_9");
                    MapeoColumnas.Add("C42|ENC_TIT_4_10");
                    MapeoColumnas.Add("C43|ENC_TIT_4_11");

                    #endregion Mapeo
                    break;
                case "CAA_ESCUELAS_CARRERAS":
                    #region Mapeo
                    MapeoColumnas.Add("|CODIGO_CARRERA");
                    MapeoColumnas.Add("C6|CODIGO_ESCUELA");

                    #endregion Mapeo
                    break;
                case "CAA_EVAL_360_COO":
                    #region Mapeo
                    MapeoColumnas.Add("Sede|NOMBRE_SEDE");
                    MapeoColumnas.Add("C6|NOMBRE_ESCUELA");
                    MapeoColumnas.Add("C7|RUT");
                    MapeoColumnas.Add("C8|DV");
                    MapeoColumnas.Add("C9|PLANIF_PREP_CLASE");
                    MapeoColumnas.Add("C10|FAC_EST_APREND");
                    MapeoColumnas.Add("C11|REL_APRE_RESP");
                    MapeoColumnas.Add("C12|COMP_CON_EST");
                    MapeoColumnas.Add("C13|ACT_EN_ASIG");
                    MapeoColumnas.Add("C14|SABE_ENSENIAR");
                    MapeoColumnas.Add("C15|COMPR_MIS_ARCOS");
                    MapeoColumnas.Add("C16|EST_CUMPLE_COMP");
                    #endregion Mapeo
                    break;
                case "CAA_EVAL_360_DIR":
                    #region Mapeo
                    MapeoColumnas.Add("|NOMBRE_SEDE");
                    MapeoColumnas.Add("C6|NOMBRE_ESCUELA");
                    MapeoColumnas.Add("C7|RUT");
                    MapeoColumnas.Add("C8|DV");
                    MapeoColumnas.Add("C9|PLANIF_PREP_CLASE");
                    MapeoColumnas.Add("C10|FAC_EST_APREND");
                    MapeoColumnas.Add("C11|REL_APRE_RESP");
                    MapeoColumnas.Add("C12|COMP_CON_EST");
                    MapeoColumnas.Add("C13|ACT_EN_ASIG");
                    MapeoColumnas.Add("C14|SABE_ENSENIAR");
                    MapeoColumnas.Add("C15|COMPR_MIS_ARCOS");
                    MapeoColumnas.Add("C16|EST_CUMPLE_COMP");

                    #endregion Mapeo
                    break;
                case "CAA_FORMACION_DOC":
                    #region Mapeo
                    MapeoColumnas.Add("|RUT_DOCENTE");
                    MapeoColumnas.Add("C6|ANIO");
                    MapeoColumnas.Add("C7|SEMESTRE");
                    MapeoColumnas.Add("C8|NRO_HRS_APRENDIZAJE");
                    MapeoColumnas.Add("C9|NRO_HRS_ACTUALIZACION");

                    #endregion Mapeo
                    break;
                case "CAA_GASTOS":
                    #region Mapeo
                    MapeoColumnas.Add("|GASTOS");
                    MapeoColumnas.Add("C6|TIPO");
                    MapeoColumnas.Add("C7|UN");
                    MapeoColumnas.Add("C8|CUENTAS");
                    MapeoColumnas.Add("C9|MONTO");
                    MapeoColumnas.Add("C10|MATRIZ");
                    MapeoColumnas.Add("C11|FOTOGRAFIA");
                    MapeoColumnas.Add("C12|CINE");
                    MapeoColumnas.Add("C13|DISENIO");
                    MapeoColumnas.Add("C14|SONIDO");
                    MapeoColumnas.Add("C15|COMUNICACIÓN");
                    MapeoColumnas.Add("C16|ACTUACION");
                    MapeoColumnas.Add("C17|OTEC");
                    MapeoColumnas.Add("C18|PROYECTOS");
                    MapeoColumnas.Add("C19|RUT");

                    #endregion Mapeo
                    break;
                case "CAA_INFO_OF_NACIONAL":
                    #region Mapeo
                    MapeoColumnas.Add("|NOMBRE_CARRERA_AR");
                    MapeoColumnas.Add("C6|ANIO");
                    MapeoColumnas.Add("C7|TOTAL_MATRI");
                    MapeoColumnas.Add("C8|TOTAL_MATRI_FEME");
                    MapeoColumnas.Add("C9|TOTAL_MATRI_MASC");
                    MapeoColumnas.Add("C10|TOTAL_MATRI_PRIMER");
                    MapeoColumnas.Add("C11|TOTAL_MATRI_PRIMER_FEME");
                    MapeoColumnas.Add("C12|TOTAL_MATRI_PRIMER_MASC");
                    MapeoColumnas.Add("C13|CLASIF_N1");
                    MapeoColumnas.Add("C14|CLASIF_N2");
                    MapeoColumnas.Add("C15|CLASIF_N3");
                    MapeoColumnas.Add("C16|NOMBRE_INSTITUCION");
                    MapeoColumnas.Add("C17|CIUDAD");
                    MapeoColumnas.Add("C18|REGION");
                    MapeoColumnas.Add("C19|NOMBRE_CARRERA");
                    MapeoColumnas.Add("C20|AREA_CINE_UNESCO");
                    MapeoColumnas.Add("C21|AREA_OCDE");
                    MapeoColumnas.Add("C22|SUBAREA_OCDE");
                    MapeoColumnas.Add("C23|AREA_CARR_GEN");
                    MapeoColumnas.Add("C24|NIVEL_GLOBAL");
                    MapeoColumnas.Add("C25|CARRERA_CLASIF_N1");
                    MapeoColumnas.Add("C26|CARRERA_CLASIF_N2");
                    MapeoColumnas.Add("C27|MODALIDAD");
                    MapeoColumnas.Add("C28|JORNADA");
                    MapeoColumnas.Add("C29|TIPO_PLAN_CARR");
                    MapeoColumnas.Add("C30|DURACION_EST_CARR");
                    MapeoColumnas.Add("C31|DURACION_TOTAL_CARR");

                    #endregion Mapeo
                    break;
                case "CAA_NUEVOS_BECA_1RA":
                    #region Mapeo
                    MapeoColumnas.Add("|RUT");
                    MapeoColumnas.Add("C6|DV");
                    MapeoColumnas.Add("C7|DECIL");
                    MapeoColumnas.Add("C8|NOMBRE_IES");
                    MapeoColumnas.Add("C9|NOMBRE_SEDE");
                    MapeoColumnas.Add("C10|NOMBRE_CARRERA");
                    MapeoColumnas.Add("C11|GLOSA_BPSU");
                    MapeoColumnas.Add("C12|MONTO_BPSU");
                    MapeoColumnas.Add("C13|GLOSA_BEA");
                    MapeoColumnas.Add("C14|MONTO_BEA");
                    MapeoColumnas.Add("C15|GLOSA_BHPE");
                    MapeoColumnas.Add("C16|MONTO_BHPE");
                    MapeoColumnas.Add("C17|GLOSA_BVP1");
                    MapeoColumnas.Add("C18|MONTO_BVP1");
                    MapeoColumnas.Add("C19|GLOSA_BJGM");
                    MapeoColumnas.Add("C20|MONTO_BJGM");
                    MapeoColumnas.Add("C21|GLOSA_BJGME");
                    MapeoColumnas.Add("C22|MONTO_BJGME");
                    MapeoColumnas.Add("C23|GLOSA_BET");
                    MapeoColumnas.Add("C24|MONTO_BET");
                    MapeoColumnas.Add("C25|GLOSA_BNM");
                    MapeoColumnas.Add("C26|MONTO_BNM");
                    MapeoColumnas.Add("C27|GLOSA_BB");
                    MapeoColumnas.Add("C28|MONTO_BB");
                    MapeoColumnas.Add("C29|GLOSA_BB_DISC");
                    MapeoColumnas.Add("C30|MONTO_BB_DISC");
                    MapeoColumnas.Add("C31|GLOSA_BJGM_DISC");
                    MapeoColumnas.Add("C32|MONTO_BJGM_DISC");
                    MapeoColumnas.Add("C33|GLOSA_BNM_DISC");
                    MapeoColumnas.Add("C34|MONTO_BNM_DISC");
                    MapeoColumnas.Add("C35|GLOSA_BA");
                    MapeoColumnas.Add("C36|MONTO_BA");
                    MapeoColumnas.Add("C37|GLOSA_FSCU");
                    MapeoColumnas.Add("C38|MONTO_FSCU");
                    MapeoColumnas.Add("C39|PORCENTAJE_FSCU");
                    MapeoColumnas.Add("C40|FECHA_PUBLICACION");
                    MapeoColumnas.Add("C41|OBSERVACIONES");

                    #endregion Mapeo
                    break;
                case "CAA_NUEVOS_BECA_2DA":
                    #region Mapeo
                    MapeoColumnas.Add("|RUT");
                    MapeoColumnas.Add("C6|DV");
                    MapeoColumnas.Add("C7|DECIL");
                    MapeoColumnas.Add("C8|NOMBRE_IES");
                    MapeoColumnas.Add("C9|NOMBRE_SEDE");
                    MapeoColumnas.Add("C10|NOMBRE_CARRERA");
                    MapeoColumnas.Add("C11|GLOSA_BPSU");
                    MapeoColumnas.Add("C12|MONTO_BPSU");
                    MapeoColumnas.Add("C13|GLOSA_BEA");
                    MapeoColumnas.Add("C14|MONTO_BEA");
                    MapeoColumnas.Add("C15|GLOSA_BHPE");
                    MapeoColumnas.Add("C16|MONTO_BHPE");
                    MapeoColumnas.Add("C17|GLOSA_BJGM");
                    MapeoColumnas.Add("C18|MONTO_BJGM");
                    MapeoColumnas.Add("C19|GLOSA_BNM");
                    MapeoColumnas.Add("C20|MONTO_BNM");
                    MapeoColumnas.Add("C21|GLOSA_BB");
                    MapeoColumnas.Add("C22|MONTO_BB");
                    MapeoColumnas.Add("C23|GLOSA_FSCU");
                    MapeoColumnas.Add("C24|MONTO_FSCU");
                    MapeoColumnas.Add("C25|PORCENTAJE_FSCU");
                    MapeoColumnas.Add("C26|FECHA_PUBLICACION");

                    #endregion Mapeo
                    break;
                case "CAA_NUEVOS_CAE":
                    #region Mapeo
                    MapeoColumnas.Add("|RUT");
                    MapeoColumnas.Add("C6|DV");
                    MapeoColumnas.Add("C7|FECHA_CARGA_INICIAL");

                    #endregion Mapeo
                    break;
                case "CAA_PERSONAL_SIES":
                    #region Mapeo
                    MapeoColumnas.Add("|RUT");
                    MapeoColumnas.Add("C6|DV");
                    MapeoColumnas.Add("C7|CTD_ANIOS_EN_INST");
                    MapeoColumnas.Add("C8|CARGO");
                    MapeoColumnas.Add("C9|UA_PRINCIPAL");
                    MapeoColumnas.Add("C10|NIVEL_FORMACION");
                    MapeoColumnas.Add("C11|NOMBRE_TITULO");
                    MapeoColumnas.Add("C12|NOMBRE_INSTITUCION");
                    MapeoColumnas.Add("C13|PAIS_TITULO");
                    MapeoColumnas.Add("C14|FECHA_TITULO");
                    MapeoColumnas.Add("C15|HRS_INDEFINIDO");
                    MapeoColumnas.Add("C16|HRS_FIJO");
                    MapeoColumnas.Add("C17|HRS_HONORARIOS");
                    MapeoColumnas.Add("C18|TOTAL");

                    #endregion Mapeo
                    break;
                case "CAA_PRACTICA_ENCUESTA":
                    #region Mapeo
                    MapeoColumnas.Add("|RUT");
                    MapeoColumnas.Add("C6|DV");
                    MapeoColumnas.Add("C7|NOMBRE_CARRERA");
                    MapeoColumnas.Add("C8|NOMBRE_EMP_INST");
                    MapeoColumnas.Add("C9|FECHA_INGRESO");
                    MapeoColumnas.Add("C10|FECHA_TERMINO");
                    MapeoColumnas.Add("C11|TOTAL_HRS_R");
                    MapeoColumnas.Add("C12|INTEG_EQUIP_TRAB");
                    MapeoColumnas.Add("C13|CUMPLE_HORARIO");
                    MapeoColumnas.Add("C14|CUMPLE_ENTR_EJEC");
                    MapeoColumnas.Add("C15|DEM_RESP");
                    MapeoColumnas.Add("C16|DEM_INICIATIVA");
                    MapeoColumnas.Add("C17|CAPTA_INDIC");
                    MapeoColumnas.Add("C18|RESPONDE_NEC");
                    MapeoColumnas.Add("C19|APORTA_SOLUCION");
                    MapeoColumnas.Add("C20|DOMINIO_TEC");
                    MapeoColumnas.Add("C21|DEM_INTERES");
                    MapeoColumnas.Add("C22|PREFIERE_ARCOS");
                    MapeoColumnas.Add("C23|TIENE_COMP_ESP");
                    MapeoColumnas.Add("C24|COMPARA_FAVORABLE");

                    #endregion Mapeo
                    break;
                case "CAA_PRACTICA_PRACTICA":
                    #region Mapeo
                    MapeoColumnas.Add("|TIPO");
                    MapeoColumnas.Add("C6|RUT");
                    MapeoColumnas.Add("C7|DV");
                    MapeoColumnas.Add("C8|NOMBRE_CARRERA");
                    MapeoColumnas.Add("C9|LUGAR_PRACTICA");
                    MapeoColumnas.Add("C10|FECHA_INICIO");
                    MapeoColumnas.Add("C11|FECHA_APROBACION");
                    MapeoColumnas.Add("C12|NOTA");
                    MapeoColumnas.Add("C13|HRS_PRACT_COMPL");
                    MapeoColumnas.Add("C14|HRS_PRACT_TEMPR");

                    #endregion Mapeo
                    break;
                case "CAA_RENOVANTES_BECA":
                    #region Mapeo
                    MapeoColumnas.Add("|CIN_RUT_NUM");
                    MapeoColumnas.Add("C6|CIN_DV_TXT");
                    MapeoColumnas.Add("C7|CIN_NOMBRE_IES_TXT");
                    MapeoColumnas.Add("C8|CIN_NOMBRE_SEDE_TXT");
                    MapeoColumnas.Add("C9|CIN_NOMBRE_CARRERA_TXT");
                    MapeoColumnas.Add("C10|CIN_GLOSA_BHPE_TXT");
                    MapeoColumnas.Add("C11|CIN_MONTO_BHPE_NUM");
                    MapeoColumnas.Add("C12|CIN_GLOSA_BPSU_TXT");
                    MapeoColumnas.Add("C13|CIN_MONTO_BPSU_NUM");
                    MapeoColumnas.Add("C14|CIN_GLOSA_BEA_TXT");
                    MapeoColumnas.Add("C15|CIN_MONTO_BEA_NUM");
                    MapeoColumnas.Add("C16|CIN_GLOSA_BPED_TXT");
                    MapeoColumnas.Add("C17|CIN_MONTO_BPED_NUM");
                    MapeoColumnas.Add("C18|CIN_GLOSA_BVP1_TXT");
                    MapeoColumnas.Add("C19|CIN_MONTO_BVP1_NUM");
                    MapeoColumnas.Add("C20|CIN_GLOSA_BVP2_TXT");
                    MapeoColumnas.Add("C21|CIN_MONTO_BVP2_NUM");
                    MapeoColumnas.Add("C22|CIN_GLOSA_BJGM_TXT");
                    MapeoColumnas.Add("C23|CIN_MONTO_BJGM_NUM");
                    MapeoColumnas.Add("C24|CIN_GLOSA_BJGME_TXT");
                    MapeoColumnas.Add("C25|CIN_MONTO_BJGME_NUM");
                    MapeoColumnas.Add("C26|CIN_GLOSA_BBIC_TXT");
                    MapeoColumnas.Add("C27|CIN_MONTO_BBIC_NUM");
                    MapeoColumnas.Add("C28|CIN_GLOSA_BNM_TXT");
                    MapeoColumnas.Add("C29|CIN_MONTO_BNM_NUM");
                    MapeoColumnas.Add("C30|CIN_GLOSA_BET_TXT");
                    MapeoColumnas.Add("C31|CIN_MONTO_BET_NUM");
                    MapeoColumnas.Add("C32|CIN_GLOSA_FSCU_TXT");
                    MapeoColumnas.Add("C33|CIN_MONTO_FSCU_NUM");
                    MapeoColumnas.Add("C34|CIN_PORCENTAJE_FSCU_NUM");
                    MapeoColumnas.Add("C35|CIN_GLOSA_TITULAR_TXT");
                    MapeoColumnas.Add("C36|CIN_MONTO_TITULAR_NUM");
                    MapeoColumnas.Add("C37|CIN_GLOSA_TRASPASO_TXT");
                    MapeoColumnas.Add("C38|CIN_MONTO_TRASPASO_NUM");
                    MapeoColumnas.Add("C39|CIN_GLOSA_ART_TXT");
                    MapeoColumnas.Add("C40|CIN_MONTO_ART_NUM");
                    MapeoColumnas.Add("C41|CIN_GLOSA_REUBICA_TXT");
                    MapeoColumnas.Add("C42|CIN_MONTO_REUBICA_NUM");
                    MapeoColumnas.Add("C43|CIN_FECHA_REGISTRO_TXT");

                    #endregion Mapeo
                    break;
                case "CAA_RENOVANTES_CAE":
                    #region Mapeo
                    MapeoColumnas.Add("|RUT");
                    MapeoColumnas.Add("C6|DV");
                    MapeoColumnas.Add("C7|NOMBRE_IES");
                    MapeoColumnas.Add("C8|NOMBRE_CARRERA");
                    MapeoColumnas.Add("C9|ARANCEL_SOLICITADO_PESOS");
                    MapeoColumnas.Add("C10|RUT_BANCO");

                    #endregion Mapeo
                    break;
                case "CAA_RENOVANTES_HISTO_CAE":
                    #region Mapeo
                    MapeoColumnas.Add("|RUT");
                    MapeoColumnas.Add("C6|ANIO_LICITACION");
                    MapeoColumnas.Add("C7|ANIO_OPERACION");
                    MapeoColumnas.Add("C8|DV");
                    MapeoColumnas.Add("C9|ESTADO");
                    MapeoColumnas.Add("C10|NOMBRE_IES");

                    #endregion Mapeo
                    break;
                case "CAA_SATISF_TUTORIA":
                    #region Mapeo
                    MapeoColumnas.Add("|RUT");
                    MapeoColumnas.Add("C6|NOMBRE_CARRERA");
                    MapeoColumnas.Add("C7|EVAL_1");
                    MapeoColumnas.Add("C8|EVAL_2");
                    MapeoColumnas.Add("C9|EVAL_3");
                    MapeoColumnas.Add("C10|EVAL_4");
                    MapeoColumnas.Add("C11|EVAL_5");
                    MapeoColumnas.Add("C12|EVAL_FINAL");

                    #endregion Mapeo
                    break;
                case "CAA_SIES_BIBLIOTECAS":
                    #region Mapeo
                    MapeoColumnas.Add("|NOMBRE_INSTITUCION");
                    MapeoColumnas.Add("C6|NOMBRE_SEDE");
                    MapeoColumnas.Add("C7|ANIO_PROCESO");
                    MapeoColumnas.Add("C8|NRO_BIBLIOTECAS");
                    MapeoColumnas.Add("C9|HRS_SEM_FUNC");
                    MapeoColumnas.Add("C10|NRO_PUESTO_TRAB");
                    MapeoColumnas.Add("C11|M2_CONSTRUIDO");
                    MapeoColumnas.Add("C12|M2_SALAS_EST");
                    MapeoColumnas.Add("C13|NRO_PREST_USU_INT");
                    MapeoColumnas.Add("C14|TOTAL_USU");
                    MapeoColumnas.Add("C15|NRO_BIBLIOTECNOLOGOS");
                    MapeoColumnas.Add("C16|NRO_OTROS_PROF");
                    MapeoColumnas.Add("C17|NRO_AYUDANTES");
                    MapeoColumnas.Add("C18|NRO_SUSC_ACTUALIDAD");
                    MapeoColumnas.Add("C19|NRO_SUSC_ESPECIALIZADAS");
                    MapeoColumnas.Add("C20|NRO_SUSC_ELECTRONICAS");
                    MapeoColumnas.Add("C21|NRO_TITULOS");
                    MapeoColumnas.Add("C22|NRO_EJEMPLARES");
                    MapeoColumnas.Add("C23|MAT_AUD_MULTIMED");
                    MapeoColumnas.Add("C24|NRO_PCS_BUSQ");
                    MapeoColumnas.Add("C25|NRO_PCS_INTERNET");

                    #endregion Mapeo
                    break;
                case "CAA_SIES_DOCENTES":
                    #region Mapeo
                    MapeoColumnas.Add("|NOMBRE_INSTITUCION");
                    MapeoColumnas.Add("C6|NOMBRE_SEDE");
                    MapeoColumnas.Add("C7|ANIO_PROCESO");
                    MapeoColumnas.Add("C8|NRO_DOCE_JOR_COMPL");
                    MapeoColumnas.Add("C9|NRO_DOCE_JOR_MEDIA");
                    MapeoColumnas.Add("C10|NRO_DOCE_JOR_HORA");
                    MapeoColumnas.Add("C11|NRO_HR_JOR_COMPL");
                    MapeoColumnas.Add("C12|NRO_HR_JOR_MEDIA");
                    MapeoColumnas.Add("C13|NRO_HR_JOR_HORA");
                    MapeoColumnas.Add("C14|NRO_DOCT_JORCOMPL");
                    MapeoColumnas.Add("C15|NRO_DOCT_JORMEDIA");
                    MapeoColumnas.Add("C16|NRO_DOCT_JORHORA");
                    MapeoColumnas.Add("C17|NRO_MAGI_JOR_COMPL");
                    MapeoColumnas.Add("C18|NRO_MAGI_JOR_MEDIA");
                    MapeoColumnas.Add("C19|NRO_MAGI_JOR_HORA");
                    MapeoColumnas.Add("C20|NRO_ESPE_JOR_COMPL");
                    MapeoColumnas.Add("C21|NRO_ESPE_JOR_MEDIA");
                    MapeoColumnas.Add("C22|NRO_ESPE_JOR_HORA");
                    MapeoColumnas.Add("C23|NRO_PROF_JOR_COMPL");
                    MapeoColumnas.Add("C24|NRO_PROF_JOR_MEDIA");
                    MapeoColumnas.Add("C25|NRO_PROF_JOR_HORA");
                    MapeoColumnas.Add("C26|NRO_TECN_JOR_COMPL");
                    MapeoColumnas.Add("C27|NRO_TECN_JOR_MEDIA");
                    MapeoColumnas.Add("C28|NRO_TECN_JOR_HORA");
                    MapeoColumnas.Add("C29|NRO_OTRO_JOR_COMPL");
                    MapeoColumnas.Add("C30|NRO_OTRO_JOR_MEDIA");
                    MapeoColumnas.Add("C31|NRO_OTRO_JOR_HORA");
                    MapeoColumnas.Add("C32|NRO_HR_DOCT_JOR_COMPL");
                    MapeoColumnas.Add("C33|NRO_HR_DOCT_JOR_MEDIA");
                    MapeoColumnas.Add("C34|NRO_HR_DOCT_JOR_HORA");
                    MapeoColumnas.Add("C35|NRO_HR_MAGI_JOR_COMPL");
                    MapeoColumnas.Add("C36|NRO_HR_MAGI_JOR_MEDIA");
                    MapeoColumnas.Add("C37|NRO_HR_MAGI_JOR_HORA");
                    MapeoColumnas.Add("C38|NRO_HR_PROF_JOR_COMPL");
                    MapeoColumnas.Add("C39|NRO_HR_PROF_JOR_MEDIA");
                    MapeoColumnas.Add("C40|NRO_HR_PROF_JOR_HORA");
                    MapeoColumnas.Add("C41|NRO_HR_TECN_JOR_COMPL");
                    MapeoColumnas.Add("C42|NRO_HR_TECN_JOR_MEDIA");
                    MapeoColumnas.Add("C43|NRO_HR_TECN_JOR_HORA");
                    MapeoColumnas.Add("C44|NRO_HR_OTRO_JOR_COMPL");
                    MapeoColumnas.Add("C45|NRO_HR_OTRO_JOR_MEDIA");
                    MapeoColumnas.Add("C46|NRO_HR_OTRO_JOR_HORA");
                    MapeoColumnas.Add("C47|NRO_HR_ESPE_JOR_COMPL");
                    MapeoColumnas.Add("C48|NRO_HR_ESPE_JOR_MEDIA");
                    MapeoColumnas.Add("C49|NRO_HR_ESPE_JOR_HORA");
                    MapeoColumnas.Add("C50|NRO_DOCE");
                    MapeoColumnas.Add("C51|NRO_HRS");
                    MapeoColumnas.Add("C52|NRO_DOCE_MAS");
                    MapeoColumnas.Add("C53|NRO_DOCE_FEM");

                    #endregion Mapeo
                    break;
                case "CAA_SIES_EXTRANJEROS_INT":
                    #region Mapeo
                    MapeoColumnas.Add("|RUT");
                    MapeoColumnas.Add("C6|DV");
                    MapeoColumnas.Add("C7|TIPO_RESIDENCIA");
                    MapeoColumnas.Add("C8|PAIS_ESTUDIOS_PREVIOS");
                    MapeoColumnas.Add("C9|NOMBRE_PROGRAMA");
                    MapeoColumnas.Add("C10|TIPO_PROGRAMA");
                    MapeoColumnas.Add("C11|ESPEC_TIPO_PROGRAMA");
                    MapeoColumnas.Add("C12|JORNADA");
                    MapeoColumnas.Add("C13|DURACION_PLAN");
                    MapeoColumnas.Add("C14|REGION");
                    MapeoColumnas.Add("C15|CIUDAD");
                    MapeoColumnas.Add("C16|COMUNA");
                    MapeoColumnas.Add("C17|FECHA_INICIO");
                    MapeoColumnas.Add("C18|FECHA_TERMINO");
                    MapeoColumnas.Add("C19|CONVENIO");
                    MapeoColumnas.Add("C20|NOMBRE_UNIV_ORIGEN");
                    MapeoColumnas.Add("C21|PAIS_UNIV_ORIGEN");

                    #endregion Mapeo
                    break;
                case "CAA_SIES_EXTRANJEROS_REG":
                    #region Mapeo
                    MapeoColumnas.Add("|RUT");
                    MapeoColumnas.Add("C6|DV");
                    MapeoColumnas.Add("C7|TIPO_RESIDENCIA");
                    MapeoColumnas.Add("C8|PAIS_ESTUDIOS_PREVIOS");
                    MapeoColumnas.Add("C9|PAIS_ESTUDIOS_SECUND");
                    MapeoColumnas.Add("C10|NOMBRE_CARRERA");
                    MapeoColumnas.Add("C11|CODIGO_SIES");
                    MapeoColumnas.Add("C12|NIVEL_GLOBAL");
                    MapeoColumnas.Add("C13|NIVEL_ESPECIFICO");
                    MapeoColumnas.Add("C14|JORNADA");
                    MapeoColumnas.Add("C15|DURACION_TOTAL_CARR");
                    MapeoColumnas.Add("C16|REGION");
                    MapeoColumnas.Add("C17|CIUDAD");
                    MapeoColumnas.Add("C18|COMUNA");
                    MapeoColumnas.Add("C19|ANIO_INGRESO_1ER_ANIO");
                    MapeoColumnas.Add("C20|SEM_INGRESO_1ER_ANIO");
                    MapeoColumnas.Add("C21|ANIO_INGRESO");
                    MapeoColumnas.Add("C22|SEM_INGRESO");
                    MapeoColumnas.Add("C23|NOMBRE_UNIV_ORIGEN");
                    MapeoColumnas.Add("C24|PAIS_UNIV_ORIGEN");

                    #endregion Mapeo
                    break;
                case "CAA_SIES_INMUEBLES":
                    #region Mapeo
                    MapeoColumnas.Add("|NOMBRE_INSTITUCION");
                    MapeoColumnas.Add("C6|NOMBRE_SEDE");
                    MapeoColumnas.Add("C7|ANIO_INFO");
                    MapeoColumnas.Add("C8|NRO_INMUEBLES");
                    MapeoColumnas.Add("C9|NRO_OFICINAS");
                    MapeoColumnas.Add("C10|NRO_SALAS");
                    MapeoColumnas.Add("C11|M2_TERRENO");
                    MapeoColumnas.Add("C12|M2_CONSTRUIDO");
                    MapeoColumnas.Add("C13|M2_SALAS");

                    #endregion Mapeo
                    break;
                case "CAA_SIES_LABS_TALLERES":
                    #region Mapeo
                    MapeoColumnas.Add("|NOMBRE_INSTITUCION");
                    MapeoColumnas.Add("C6|NOMBRE_SEDE");
                    MapeoColumnas.Add("C7|ANIO_PROCESO");
                    MapeoColumnas.Add("C8|NRO_LABS");
                    MapeoColumnas.Add("C9|M2_CONSTRUIDO");
                    MapeoColumnas.Add("C10|NRO_PCS_ALUMNOS");
                    MapeoColumnas.Add("C11|NRO_PCS_INTERNET");

                    #endregion Mapeo
                    break;
                case "CAA_SIES_MATRICULADOS":
                    #region Mapeo
                    MapeoColumnas.Add("|RUT");
                    MapeoColumnas.Add("C6|DV");
                    MapeoColumnas.Add("C7|NRO_PASAPORTE");
                    MapeoColumnas.Add("C8|TIPO_ESTUDIANTE");
                    MapeoColumnas.Add("C9|TIPO_RESIDENCIA");
                    MapeoColumnas.Add("C10|PAIS_ESTUDIOS_PREVIOS");
                    MapeoColumnas.Add("C11|CODIGO_CARRERA");
                    MapeoColumnas.Add("C12|NOMBRE_CARRERA");
                    MapeoColumnas.Add("C13|JORNADA_CARRERA");
                    MapeoColumnas.Add("C14|ANIO_INGRESO_1ER_ANIO");
                    MapeoColumnas.Add("C15|SEM_INGRESO_1ER_ANIO");
                    MapeoColumnas.Add("C16|ANIO_INGRESO");
                    MapeoColumnas.Add("C17|SEM_INGRESO");
                    MapeoColumnas.Add("C18|CTD_SEM_SUSPENCION");
                    MapeoColumnas.Add("C19|TERMINAL");
                    MapeoColumnas.Add("C20|ANIO_EGRESOPR");
                    MapeoColumnas.Add("C21|SEM_EGRESO");

                    #endregion Mapeo
                    break;
                case "CAA_SIES_PLANES_PROGRAMAS":
                    #region Mapeo
                    MapeoColumnas.Add("|NOMBRE_INSTITUCION");
                    MapeoColumnas.Add("C6|CODIGO_CARRERA");
                    MapeoColumnas.Add("C7|JORNADA_CARRERA");
                    MapeoColumnas.Add("C8|NOMBRE_CARRERA");
                    MapeoColumnas.Add("C9|MODALIDAD");
                    MapeoColumnas.Add("C10|NOMBRE_SEDE");
                    MapeoColumnas.Add("C11|TIPO_PLAN");
                    MapeoColumnas.Add("C12|CARACTERISTICA_PE");
                    MapeoColumnas.Add("C13|DUR_ESTUDIOS_SEM");
                    MapeoColumnas.Add("C14|DUR_PROC_TIT_SEM");
                    MapeoColumnas.Add("C15|DUR_TOTAL");
                    MapeoColumnas.Add("C16|COMUNA_SEDE");
                    MapeoColumnas.Add("C17|CIUDAD_SEDE");
                    MapeoColumnas.Add("C18|REGION_SEDE");
                    MapeoColumnas.Add("C19|CLASIF_CARRER_ANG");
                    MapeoColumnas.Add("C20|CLASIF_CARRER_ANC");
                    MapeoColumnas.Add("C21|VALOR_MATRICULA");
                    MapeoColumnas.Add("C22|ARANCEL_1ER_ANIO");
                    MapeoColumnas.Add("C23|ACREDITADA");
                    MapeoColumnas.Add("C24|NOMBRE_AG_ACRED");
                    MapeoColumnas.Add("C25|FECHA_ACRED");
                    MapeoColumnas.Add("C26|CTS_ANIOS_ACRED");
                    MapeoColumnas.Add("C27|CODIGO_DEMRE");
                    MapeoColumnas.Add("C28|COSTO_PROC_TIT");
                    MapeoColumnas.Add("C29|COSTO_OBT_TIT");

                    #endregion Mapeo
                    break;
                case "CAA_TITULADOS_SIES":
                    #region Mapeo
                    MapeoColumnas.Add("|RUT");
                    MapeoColumnas.Add("C6|DV");
                    MapeoColumnas.Add("C7|JORNADA_CARRERA");
                    MapeoColumnas.Add("C8|NOMBRE_CARRERA");
                    MapeoColumnas.Add("C9|TITULO");
                    MapeoColumnas.Add("C10|FECHA_TITULO");
                    MapeoColumnas.Add("C11|CTD_SEM_SUSPENCION");
                    MapeoColumnas.Add("C12|ANIO_INGRESO_1ER_ANIO");
                    MapeoColumnas.Add("C13|SEM_INGRESO_1ER_ANIO");
                    MapeoColumnas.Add("C14|ANIO_INGRESO");
                    MapeoColumnas.Add("C15|SEM_INGRESO");
                    MapeoColumnas.Add("C16|ANIO_EGRESOPR");
                    MapeoColumnas.Add("C17|SEM_EGRESO");
                    #endregion Mapeo
                    break;
                case "CAA_PROYECCION_EERR":
                    #region Mapeo
                    MapeoColumnas.Add("|SEDE");
                    MapeoColumnas.Add("C6|TIPO");
                    MapeoColumnas.Add("C7|CUENTA");
                    MapeoColumnas.Add("C8|ENERO");
                    MapeoColumnas.Add("C9|FEBRERO");
                    MapeoColumnas.Add("C10|MARZO");
                    MapeoColumnas.Add("C11|ABRIL");
                    MapeoColumnas.Add("C12|MAYO");
                    MapeoColumnas.Add("C13|JUNIO");
                    MapeoColumnas.Add("C14|JULIO");
                    MapeoColumnas.Add("C15|AGOSTO");
                    MapeoColumnas.Add("C16|SEPTIEMBRE");
                    MapeoColumnas.Add("C17|OCTUBRE");
                    MapeoColumnas.Add("C18|NOVIEMBRE");
                    MapeoColumnas.Add("C19|DICIEMBRE");
                    #endregion Mapeo
                    break;
                case "CAA_EERR_SANTIAGO":
                    #region Mapeo
                    MapeoColumnas.Add("|CUENTA_CONTABLE");
                    MapeoColumnas.Add("C6|PERDIDA");
                    MapeoColumnas.Add("C7|GANANCIA");
                    MapeoColumnas.Add("C8|CUENTA");
                    MapeoColumnas.Add("C9|IE");
                    MapeoColumnas.Add("C10|CUENTA_B");
                    #endregion Mapeo
                    break;
                case "CAA_EERR_VIÑA":
                    #region Mapeo
                    MapeoColumnas.Add("|CUENTA_CONTABLE");
                    MapeoColumnas.Add("C6|PERDIDA");
                    MapeoColumnas.Add("C7|GANANCIA");
                    MapeoColumnas.Add("C8|CUENTA");
                    MapeoColumnas.Add("C9|IE");
                    MapeoColumnas.Add("C10|CUENTA_B");
                    #endregion Mapeo
                    break;
                case "CAA_INF_FIN_INT":
                    #region Mapeo
                    MapeoColumnas.Add("|CONCEPTO");
                    MapeoColumnas.Add("C6|DETALLE");
                    MapeoColumnas.Add("C7|AGNO_CARGA");
                    MapeoColumnas.Add("C8|MONTO");
                    MapeoColumnas.Add("C9|UNIDAD");

                    #endregion Mapeo
                    break;
                case "CAA_HABERES":
                    #region Mapeo
                    MapeoColumnas.Add("|RUT");
                    MapeoColumnas.Add("C6|SANTIAGO");
                    MapeoColumnas.Add("C7|VIGNA");
                    MapeoColumnas.Add("C8|MATRIZ");
                    MapeoColumnas.Add("C9|CINE");
                    MapeoColumnas.Add("C10|FOTOGRAFIA");
                    MapeoColumnas.Add("C11|DISEGNO");
                    MapeoColumnas.Add("C12|SONIDO");
                    MapeoColumnas.Add("C13|ACTUACION");
                    MapeoColumnas.Add("C14|COMUNICACION");
                    MapeoColumnas.Add("C15|PROYECTOS");
                    MapeoColumnas.Add("C16|OTC");

                    #endregion Mapeo
                    break;
                case "CAA_PLANIF_DOCENTE":
                    #region Mapeo
                    MapeoColumnas.Add("|ESCUELA");
                    MapeoColumnas.Add("C6|TOTAL_HC_STGO");
                    MapeoColumnas.Add("C7|PORC_STGO");
                    MapeoColumnas.Add("C8|TOTAL_HC_VM");
                    MapeoColumnas.Add("C9|PORC_VM");

                    #endregion Mapeo
                    break;
                case "CAA_DEVENGADO_ESCUELA":
                    #region Mapeo
                    MapeoColumnas.Add("|ESCUELA");
                    MapeoColumnas.Add("C6|ARANCEL_STGO");
                    MapeoColumnas.Add("C7|MATRICULA_STGO");
                    MapeoColumnas.Add("C8|ARANCEL_VM");
                    MapeoColumnas.Add("C9|MATRICULA_VM");

                    #endregion Mapeo
                    break;
                case "CAA_ESTUDIANTE_TUTORIA":
                    #region Mapeo
                    MapeoColumnas.Add("|RUT");
                    MapeoColumnas.Add("C6|DV");
                    MapeoColumnas.Add("C7|APELLIDOS");
                    MapeoColumnas.Add("C8|NOMBRES");
                    MapeoColumnas.Add("C9|SEXO");
                    MapeoColumnas.Add("C10|COHORTE");
                    MapeoColumnas.Add("C11|BECA");
                    MapeoColumnas.Add("C12|CAE");
                    MapeoColumnas.Add("C13|CARRERA");
                    MapeoColumnas.Add("C14|TUTOR");

                    #endregion Mapeo
                    break;
                case "CAA_DEUDORES":
                    #region Mapeo
                    MapeoColumnas.Add("|DOCUMENTO");
                    MapeoColumnas.Add("C6|FECHA_EMISION");
                    MapeoColumnas.Add("C7|FECHA_VENCI");
                    MapeoColumnas.Add("C8|MONTO_ORIGINAL");
                    MapeoColumnas.Add("C9|SALDO");
                    MapeoColumnas.Add("C10|RUT");
                    MapeoColumnas.Add("C11|CARRERA");
                    MapeoColumnas.Add("C12|MATRICULADO");
                    MapeoColumnas.Add("C13|CAE");
                    MapeoColumnas.Add("C14|COMPROMISO");

                    #endregion Mapeo
                    break;
            }

            retorno = Carga_archivo_temporal_Bulk(OrigenDatos, TablaDestino, MapeoColumnas);

            return retorno;
        }
        private bool Carga_archivo_temporal_Bulk(DataTable origenDatos, string tablaDestino, List<string> mapeoColumnas)
        {
            bool retorno = false;

            SqlConnection connectionString = new SqlConnection(cnfHelper.GetConfigAppVariable("SqlBulk"));

            int i = origenDatos.Rows.Count;
            using (SqlConnection destinationConnection = connectionString)
            {
                try
                {
                    destinationConnection.Open();

                    using (SqlBulkCopy bulkCopy = new SqlBulkCopy(destinationConnection.ConnectionString, SqlBulkCopyOptions.TableLock))
                    {
                        bulkCopy.NotifyAfter = 100;
                        bulkCopy.BatchSize = 50;

                        foreach (var mapping in mapeoColumnas)
                        {
                            var split = mapping.Split(new[] { '|' });
                            bulkCopy.ColumnMappings.Add(split.First(), split.Last());
                        }

                        if (!BorrarTablaDestino(tablaDestino))
                        {
                            return false;
                        }

                        bulkCopy.DestinationTableName = tablaDestino;

                        DataSet ds = new DataSet();

                        ds.Tables.Add(origenDatos);

                        bulkCopy.WriteToServer(origenDatos);
                    }
                }
                catch (Exception ex)
                {
                    retorno = false;
                    new Log().GuardaLog("Carga_archivo_temporal_Bulk", ex.Source, ex.Message, ex.StackTrace);
                }
                return retorno;
            }
        }
        private bool BorrarTablaDestino(string tabla)
        {
            bool resultado = true;

            return resultado;
        }
    }
}