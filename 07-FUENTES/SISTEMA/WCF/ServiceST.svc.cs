﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.ServiceModel.Web;
using System.Text;

using System.Data;
using System.Data.SqlClient;
using System.Data.SqlTypes;


//using System.Data.OracleClient;

using library.common;
using library.xml;
using System.IO;
using System.Xml;
using System.Net.Mail;
using System.Data.OracleClient;

namespace WcfST
{
    // NOTE: You can use the "Rename" command on the "Refactor" menu to change the class name "Service1" in code, svc and config file together.
    // NOTE: In order to launch WCF Test Client for testing this service, please select Service1.svc or Service1.svc.cs at the Solution Explorer and start debugging.
    public class ServiceST : IServiceST
    {
        public string GetData(int value)
        {
            return string.Format("You entered: {0}", value);
        }
        public CompositeType GetDataUsingDataContract(CompositeType composite)
        {
            if (composite == null)
            {
                throw new ArgumentNullException("composite");
            }
            if (composite.BoolValue)
            {
                composite.StringValue += "Suffix";
            }
            return composite;
        }
        public string EjecutaPA(string XML, string nombrePA)
        {
            string getxml = "";

            string tracear = cnfHelper.GetConfigAppVariable("Tracear");

            if(tracear.Equals("SI"))
            {
                new Log().GuardaTrace("Ejecuta_PA_Oracle",nombrePA,XML );
            }
            try
            {
                SqlConnection conn = new SqlConnection(cnfHelper.GetConfigAppVariable("connSQL"));
                SqlDataAdapter sda = new SqlDataAdapter(nombrePA, conn);
                sda.SelectCommand.CommandType = CommandType.StoredProcedure;
                SqlParameter param = new SqlParameter("@StrXMLDatos", SqlDbType.VarChar);
                param.Value = XML;
                sda.SelectCommand.Parameters.Add(param);
                DataSet ds = new DataSet();
                sda.Fill(ds);
                //getxml = new DsCtlHelper(ds).GetDsForXmlRaw();

                ds.DataSetName = "Datos";

                for (int contador = 0; contador <= ds.Tables.Count - 1; contador++)
                {
                    if (contador > 0)
                    {
                        ds.Tables[contador].TableName = "row" + contador.ToString();

                    }
                    else
                    {
                        ds.Tables[contador].TableName = "row";

                    }

                    foreach (DataColumn column in ds.Tables[contador].Columns)
                    {
                        column.ColumnMapping = MappingType.Attribute;
                    }
                }
                getxml = ds.GetXml();
            }
            catch (Exception ex)
            {
                new Log().GuardaLog("EjecutaPA", ex.Source, ex.Message, ex.StackTrace);
                getxml = CDError(ex);
            }
            return getxml;
        }
        public string Ejecuta_PA_Oracle(string XML, string nombrePA)
        {
            string getxml = "";
            string tracear = cnfHelper.GetConfigAppVariable("Tracear");

            if(tracear.Equals("SI"))
            {
                new Log().GuardaTrace("Ejecuta_PA_Oracle",nombrePA,XML );
            }

            try
            {
                using (OracleConnection cn = new OracleConnection(cnfHelper.GetConfigAppVariable("connORACLE")))
                {
                    cn.Open();
                    OracleDataAdapter da = new OracleDataAdapter();
                    OracleCommand cmd = new OracleCommand();
                    cmd.Connection = cn;
                    cmd.CommandText = nombrePA;
                    cmd.CommandType = CommandType.StoredProcedure;

                    OracleParameter parami;
                    parami = cmd.Parameters.Add(new OracleParameter("xml_document_i", OracleType.VarChar));
                    parami.Direction = ParameterDirection.Input;
                    parami.Size = 4000;
                    parami.Value = XML;

                    OracleParameter param;
                    param = cmd.Parameters.Add(new OracleParameter("vCURSOR", OracleType.Cursor));
                    param.Direction = ParameterDirection.Output;

                    OracleParameter param2;
                    param2 = cmd.Parameters.Add(new OracleParameter("vCURSOR2", OracleType.Cursor));
                    param2.Direction = ParameterDirection.Output;

                    da.SelectCommand = cmd;

                    DataSet ds = new DataSet();
                    da.Fill(ds);
                    ds.DataSetName = "Datos";
                    cn.Close();
                    for (int contador = 0; contador <= ds.Tables.Count - 1; contador++)
                    {
                        if (contador > 0)
                        {
                            ds.Tables[contador].TableName = "row" + contador.ToString();

                        }
                        else
                        {
                            ds.Tables[contador].TableName = "row";

                        }

                        foreach (DataColumn column in ds.Tables[contador].Columns)
                        {
                            column.ColumnMapping = MappingType.Attribute;
                        }
                    }

                    getxml = ds.GetXml();
                }
            }
            catch (Exception ex)
            {
                new Log().GuardaLog("EjecutaPA", ex.Source, ex.Message, ex.StackTrace);
                getxml = CDError(ex);
            }
            return getxml;
        }
        private string CDError(Exception e)
        {
            string getxml = "<row error='" + cnfHelper.GetConfigAppVariable("MensajeError") + "' ";
            getxml += "errordet='" + cnfHelper.GetConfigAppVariable("MensajeError2") + "' ";
            getxml += "errordet2='" + e.Message.Replace("'", "\"") + " (" + e.GetType().ToString() + ")'/>";
            return getxml;
        }
    }
}