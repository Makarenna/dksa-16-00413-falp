if (typeof String.prototype.trim !== 'function') {
    String.prototype.trim = function () {
        return this.replace(/^\s+|\s+$/g, '');
    }
}
function SerializaAlias(divs) {
    var items = {};    
    jQuery_1_9("input[type=text],input[type=hidden], input[type=password],input[type=radio]:checked", jQuery_1_9(divs)).each(function () {
        var attr = jQuery_1_9(this).attr('alias');

        if (typeof attr !== typeof undefined && attr !== false) {
            items[jQuery_1_9(this).attr("alias")] = this.value;
        }
    });
    jQuery_1_9("input[type=date]", jQuery_1_9(divs)).each(function () {

        var attr = jQuery_1_9(this).attr('alias');

        if (typeof attr !== typeof undefined && attr !== false) {
            items[jQuery_1_9(this).attr("alias")] = this.value;
        }
    });
    jQuery_1_9("select", jQuery_1_9(divs)).each(function () {
        var attr = jQuery_1_9(this).attr('alias');

        if (typeof attr !== typeof undefined && attr !== false) {
            items[jQuery_1_9(this).attr("alias")] = this.value;
        }
    });
    jQuery_1_9("input[type=checkbox]:checked", jQuery_1_9(divs)).each(function () {
        var attr = jQuery_1_9(this).attr('alias');

        if (typeof attr !== typeof undefined && attr !== false) {
            items[jQuery_1_9(this).attr("alias")] = this.value;
        }
    });
    jQuery_1_9("textarea", jQuery_1_9(divs)).each(function () {

        var attr = jQuery_1_9(this).attr('alias');

        if (typeof attr !== typeof undefined && attr !== false) {
            items[jQuery_1_9(this).attr("alias")] = this.innerHTML;
        }
    });

    //console.log(JSON.stringify(items));

    return JSON.stringify(items);
};
//function SerializaAliasCheck(divs) {
//    var Row = [];

//    jQuery_1_9("input[type=checkbox]:checked:first", jQuery_1_9(divs)).each(function () {
//        items = {};
//        var alias = jQuery_1_9(this).attr("alias");
//        var valor = jQuery_1_9(this).attr("value");
//        items[alias] = valor;
//        Row.push(items);
//    });
//    console.log(JSON.stringify(Row));

//    return JSON.stringify(Row);
//};
function SerializaAliasTabla(tabla) {
    var Row = [];

    jQuery_1_9("tbody tr", jQuery_1_9('#' + tabla)).each(function () {
        items = {};
        jQuery_1_9(this).find("td[tcol='data']").each(function () {
            var alias = jQuery_1_9(this).attr("alias");
            var valor = jQuery_1_9(this).attr("valor");
            items[alias] = valor;
        });
        Row.push(items);
    });
    //console.log(JSON.stringify(Row));
    return JSON.stringify(Row);
};
//function IrAPag(Pag, Accion) {
//    jQuery_1_9('#PagNumber' + Accion).val(Pag);
//    var f = SerializaAlias('#divFiltros,#divResultado');
//    jQuery_1_9("#divResultado").load("../PagComun/GrillaPaginada/GrillaPaginada.aspx", { Accion: Accion, AliasForm: f }, function () {
//        jQuery_1_9("#divAgregar").hide();
//        jQuery_1_9("#divResultado").show();
//        jQuery_1_9("#divFiltros").show();
//    });
//}
function IrAUrl(url,id)
{
    var form = jQuery_1_9('<form action="' + url + '" method="post"><input type="text" name="IdUrlRequest" id="IdUrlRequest" value="' + id + '" /></form>');
    jQuery_1_9('body').append(form);
    form.submit();
}
//function ChangeRegPag(Obj, Accion) {
//    jQuery_1_9('#PagNumber' + Accion).val('1');
//    var f = SerializaAlias('#divFiltros,#divResultado');
//    jQuery_1_9('#divResultado').load("../PagComun/GrillaPaginada/GrillaPaginada.aspx", { Accion: Accion, AliasForm: f }, function () {
//    });
//};
function ChangePadre(Obj, Combo) {
    var IdPadre = Obj.value;

    jQuery_1_9("#" + Combo + " option").each(function () {
        var IdHijo = jQuery_1_9(this).attr('idpadre');
        if (IdPadre == IdHijo || IdPadre == '' || IdHijo == '') {
            jQuery_1_9(this).css({ display: "block" });
            jQuery_1_9(this).prop('disabled', false);//IE
        }
        else {
            jQuery_1_9(this).css({ display: "none" });
            jQuery_1_9(this).prop('disabled', true);//IE
        }
    });

    jQuery_1_9("#" + Combo).prop("selectedIndex", 0);
}
function ChangePadreCheckBox(Obj, divCheckbox) {
    var IdPadre = Obj.value;
    jQuery_1_9("#" + divCheckbox).find('input[type=checkbox]').each(function () {
        var IdHijo = jQuery_1_9(this).attr('idpadre');

        jQuery_1_9(this).prop('checked', false);
        if (IdPadre == IdHijo || IdPadre == '') {
            jQuery_1_9(this).show();
            jQuery_1_9(this).parent().show();//label correspondiente
        }
        else {
            jQuery_1_9(this).hide();
            jQuery_1_9(this).parent().hide();//label correspondiente
        }
    });
}
function changeInputSerial(Obj) {
    var IdPadre = Obj.value;
    jQuery_1_9(Obj).parent().attr('iden', IdPadre);
    jQuery_1_9(Obj).parent().attr('value', IdPadre);
}
function trim(s) {
    while (s.substring(0, 1) == ' ') {
        s = s.substring(1, s.length);
    }
    while (s.substring(s.length - 1, s.length) == ' ') {
        s = s.substring(0, s.length - 1);
    }
    return s;
}
function explode_rut(theFieldRut) {

    var strText = theFieldRut.value;
    var ID = jQuery_1_9(theFieldRut).prop("id");
    var arrText = strText.split("-");
    //sin digit then to access denie
    if (arrText[1]) { //granted
        var ok = _validar_rut(arrText[0], arrText[1]);
    }
    else {	//denie
        var ok = false;
    };
    //do you submit ?
    if (ok) { //yes
        //as it is use in all site, do submit somewhere else.
    }
    else { //no
        jQuery_1_9('#myModalconsole.log h3').text('Validacion Rut');
        jQuery_1_9('#myModalconsole.log h4').text('El RUT ingresado no es v�lido, debe ser ingresado en formato 99999999-9');
        jQuery_1_9('#myModalconsole.log .modal-footer button').attr("onclick", "Limpiar('" + ID + "')");
        jQuery_1_9('#myModalconsole.log').modal('show');
    }
    return ok;
}
function _validar_rut(rut, dig) {
    if (rut < 1000000) {
        rut = "00" + rut;
    } else if (rut < 10000000) {
        rut = "0" + rut;
    }
    if (dig.toUpperCase() == "K") {
        dig = "10";
    }
    if (dig == 0) {
        dig = "11";
    }
    mirut = new Array(rut.substr(0, 1), rut.substr(1, 1), rut.substr(2, 1), rut.substr(3, 1), rut.substr(4, 1), rut.substr(5, 1), rut.substr(6, 1), rut.substr(7, 1));
    if ((11 - ((mirut[0] * 3 + mirut[1] * 2 + mirut[2] * 7 + mirut[3] * 6 + mirut[4] * 5 + mirut[5] * 4 + mirut[6] * 3 + mirut[7] * 2) % 11) != dig) || (dig > 11)) {
        return false;
    } else {
        return true;
    }
}
function ObtenerAlias() { }
//function ObtenerAlias() {

//    var msg = '';

//    jQuery_1_9("#form div").filter(function () { return jQuery_1_9(this).attr('Tipo') == 'Recorrer'; }).each(function () {

//        if (jQuery_1_9(this).prop('id') != '') {//v�lido que los elementos tengan ids
//            var elementos = jQuery_1_9('#' + jQuery_1_9(this).prop('id') + ' .form-control'); //solo los controles
//            jQuery_1_9.each(elementos, function (i, val) {
//                var tag = jQuery_1_9(val).prop('tagName');
//                var type = jQuery_1_9(val).prop('type');
//                switch (tag) {
//                    case "INPUT":
//                        switch (type) {
//                            case "hidden":
//                                msg += jQuery_1_9(val).attr('alias') + '|' + jQuery_1_9(val).val() + '~';
//                                break;
//                            case "text":
//                                msg += jQuery_1_9(val).attr('alias') + '|' + jQuery_1_9(val).val() + '~';
//                                break;
//                            case "checkbox":
//                                if (jQuery_1_9(val).prop('checked') == true) {
//                                    msg += jQuery_1_9(val).attr('alias') + '|' + jQuery_1_9(val).val() + '~';
//                                }
//                                break;
//                            case "radio":
//                                if (jQuery_1_9(val).is(':checked')) {
//                                    msg += jQuery_1_9(val).attr('alias') + '|' + jQuery_1_9(val).val() + '~';
//                                }
//                        }
//                        break;
//                    case "TEXTAREA":
//                        msg += jQuery_1_9(val).attr('alias') + '|' + jQuery_1_9(val).val() + '~';
//                        break;
//                    case "SELECT":
//                        msg += jQuery_1_9(val).attr('alias') + '|' + jQuery_1_9(val).val() + '~';
//                        break;
//                }
//            });
//        }
//        else {
//            console.log(jQuery_1_9(this));//muestro cual elemento no tiene id
//        }
//    })

//    return msg;
//}
//function ObtenerAliasGeneral(divAlias) {

//    var msg = '';
//    var elementos = jQuery_1_9('#divHidden .form-control');
//    jQuery_1_9.each(elementos, function (i, val) {
//        switch (jQuery_1_9(val).prop('tagName')) {
//            case "INPUT":
//                msg += jQuery_1_9(val).attr('alias') + '|' + jQuery_1_9(val).val() + '~';
//                break;
//            case "SELECT":
//                msg += jQuery_1_9(val).attr('alias') + '|' + jQuery_1_9(val).val() + '~';
//                break;
//        }
//    });
//    var elementos2 = jQuery_1_9(divAlias + ' .form-control');
//    jQuery_1_9.each(elementos2, function (i, val) {
//        switch (jQuery_1_9(val).prop('tagName')) {
//            case "INPUT":
//                if (jQuery_1_9(val).prop('type') == 'radio') {

//                    if (jQuery_1_9(val).is(':checked')) {
//                        msg += jQuery_1_9(val).attr('alias') + '|' + jQuery_1_9(val).val() + '~';

//                    }
//                }
//                else {
//                    if (jQuery_1_9(val).prop('type') == 'file') {

//                        var Nombre = jQuery_1_9(val).attr('alias');
//                        var NombreArchivo = jQuery_1_9(val).val().split("\\");
//                        var SeparaArchivo = NombreArchivo[NombreArchivo.length - 1].split(".");
//                        var Extension = SeparaArchivo[SeparaArchivo.length - 1];
//                        var Contenido = jQuery_1_9('#preview img').attr('src').split(",");
//                        var ContBase64 = Contenido[Contenido.length - 1];
//                        var tipocontenido = Contenido[Contenido.length - 2].split(";")[0].split(":")[1];

//                        msg += 'TipoContenido|' + tipocontenido + '~';
//                        msg += 'Extension|' + Extension + '~';
//                        msg += Nombre + '|' + ContBase64 + '~';
//                    }
//                    else {
//                        msg += jQuery_1_9(val).attr('alias') + '|' + jQuery_1_9(val).val() + '~';
//                    }
//                }
//                break;
//            case "SELECT":
//                msg += jQuery_1_9(val).attr('alias') + '|' + jQuery_1_9(val).val() + '~';
//                break;
//            case "TEXTAREA":
//                msg += jQuery_1_9(val).attr('alias') + '|' + trim(jQuery_1_9(val).val()) + '~';
//                break;
//        }
//    });
//    var elementos3 = jQuery_1_9(divAlias + ' .FileFalso');
//    jQuery_1_9.each(elementos3, function (i, val) {
//        switch (jQuery_1_9(val).prop('tagName')) {
//            case "INPUT":

//                msg += 'Nombre' + jQuery_1_9(val).attr('alias') + '|' + jQuery_1_9(val).attr('Valor') + '~';
//                msg += 'Size' + jQuery_1_9(val).attr('alias') + '|' + jQuery_1_9(val).attr('Tamano') + '~';
//                break;
//        }
//    });

//    return msg;
//}
//function ObtenerAliasEspecial(divAlias, Tipo) {
//    var msg = '';
//    var elementos

//    switch (Tipo) {
//        case 'TablaSerial'://tabla
//            elementos = jQuery_1_9(divAlias + ' .' + Tipo);
//            var TxtoFila = ""
//            jQuery_1_9.each(elementos, function (i, val) {

//                var IdTabla = jQuery_1_9(val).prop('id')
//                jQuery_1_9("#" + IdTabla + " .Graba").each(function () {//cada fila de la tabla
//                    if (jQuery_1_9(this).prop('id') != '') {

//                        var idFila = jQuery_1_9(this).prop('id');

//                        TxtoFila += "|"
//                        jQuery_1_9("#" + idFila + " .llave").each(function () {//cada columna de la fila

//                            var alias = jQuery_1_9(this).attr('alias');
//                            var idCol = jQuery_1_9(this).attr('iden');

//                            TxtoFila += "~" + alias + "=" + idCol
//                        });

//                    }
//                    else {
//                        console.log(jQuery_1_9(this));//muestro que elemento no tiene id
//                    }
//                });
//                msg += TxtoFila;
//            });
//            break;
//    }
//    return msg;
//}
//function ObtenerAliasAgregar() {
//    var msg = '';
//    var elementos = jQuery_1_9('#divHidden .form-control');
//    jQuery_1_9.each(elementos, function (i, val) {
//        switch (jQuery_1_9(val).prop('tagName')) {
//            case "INPUT":
//                msg += jQuery_1_9(val).attr('alias') + '=' + jQuery_1_9(val).val() + '~';
//                break;
//            case "SELECT":
//                msg += jQuery_1_9(val).attr('alias') + '=' + jQuery_1_9(val).val() + '~';
//                break;
//        }
//    });
//    var elementos2 = jQuery_1_9('#divAgregar .form-control');
//    jQuery_1_9.each(elementos2, function (i, val) {
//        switch (jQuery_1_9(val).prop('tagName')) {
//            case "INPUT":
//                if (jQuery_1_9(val).prop('type') == 'radio') {
//                    if (jQuery_1_9(val).is(':checked')) {
//                        msg += jQuery_1_9(val).attr('alias') + '=' + jQuery_1_9(val).val() + '~';
//                    }
//                }
//                else {
//                    msg += jQuery_1_9(val).attr('alias') + '=' + jQuery_1_9(val).val() + '~';
//                }
//                break;
//            case "SELECT":
//                msg += jQuery_1_9(val).attr('alias') + '=' + jQuery_1_9(val).val() + '~';
//                break;
//            case "TEXTAREA":
//                msg += jQuery_1_9(val).attr('alias') + '=' + jQuery_1_9(val).val() + '~';
//                break;
//        }
//    });
//    var elementos3 = jQuery_1_9('#divAgregar .obj');
//    jQuery_1_9.each(elementos3, function (i, val) {

//        switch (jQuery_1_9(val).prop('tagName')) {
//            case "IMG":
//                var Nombre = jQuery_1_9(this).parent().parent().children('label').html();
//                var NombreArchivo = jQuery_1_9(this).parent().parent().children('input').val().split("\\");
//                var SeparaArchivo = NombreArchivo[NombreArchivo.length - 1].split(".");
//                var Extension = SeparaArchivo[SeparaArchivo.length - 1];
//                var Contenido = jQuery_1_9(this).attr('src').split(",");
//                var ContBase64 = Contenido[Contenido.length - 1];
//                msg += 'Extension=' + Extension + '~';
//                msg += Nombre + '=' + ContBase64 + '~';
//                //msg += jQuery_1_9(val).attr('alias') + '=' + jQuery_1_9(val).val() + ':';
//                break;
//        }
//    });
//    return msg;

//}

function Buscar(Pag, Titulo) {

    var idTxt = '#divResultado #PagNumber' + Titulo;
    jQuery_1_9(idTxt).val(Pag);
    var f = ObtenerAlias();
    var selector = document.getElementById('divResultado');
    jQuery_1_9(selector).load("../PagComun/SabanaSinPag.aspx", { Accion: Titulo, alias: f }, function () {
        jQuery_1_9("#divAgregar").hide();
        jQuery_1_9("#divResultado").show();
        jQuery_1_9("#divFiltros").show();
        jQuery_1_9(document).scrollTop(jQuery_1_9("#divResultado").offset().top);
    });
}
function BuscarModal(Pag, Titulo) {

    var idTxt = '#divResultadoModal #PagNumber' + Titulo;
    jQuery_1_9(idTxt).val(Pag);
    var f = ObtenerAlias();
    var selector = document.getElementById('divResultadoModal');
    jQuery_1_9(selector).load("../PagComun/SabanaSinPag.aspx", { Accion: Titulo, alias: f }, function () {
        jQuery_1_9("#divAgregar").hide();
        jQuery_1_9("#divResultadoModal").show();
        jQuery_1_9("#divFiltros").show();
        jQuery_1_9(document).scrollTop(jQuery_1_9("#divResultadoModal").offset().top);
    });
}
function CerrarModal(MyModal) {
    jQuery_1_9(MyModal).modal('toggle');
}
function Nuevo(Titulo) {
    var idTxt1 = 'NUEVO'
    var f = '';
    var selector = document.getElementById('divAgregar');
    jQuery_1_9(selector).load("../PagComun/PagAcciones.aspx", { Accion: Titulo, Accion2: idTxt1, alias: f }, function () {
        jQuery_1_9("#divAgregar").show();
        jQuery_1_9("#divResultado").hide();
        jQuery_1_9("#divFiltros").hide();
    });
}
function NuevoBD(Titulo) {
    var idTxt1 = 'NUEVOBD'
    var f = ObtenerAliasAgregar();
    var idTxt = "'1'," + "'" + Titulo + "'";
    var selector = document.getElementById('divAgregar');
    jQuery_1_9(selector).load("../PagComun/PagAcciones.aspx", { Accion: Titulo, Accion2: idTxt1, alias: f }, function () {
        jQuery_1_9('#myModalconsole.log h3').text('Agregar ' + Titulo);
        jQuery_1_9('#myModalconsole.log h4').text('Registro Agregado Correctamente');
        jQuery_1_9('#myModalconsole.log .modal-footer button').attr("onclick", "IrAPag(" + idTxt + ")");
        jQuery_1_9('#myModalconsole.log').modal('show');
    });
}
function ModificarBD(Titulo) {
    var idTxt1 = 'MODIFICARBD'
    var f = ObtenerAliasAgregar();
    var idTxt2 = '#Id' + Titulo;
    jQuery_1_9(idTxt2).val('');
    var idTxt = "'1'," + "'" + Titulo + "'";
    var selector = document.getElementById('divAgregar');
    jQuery_1_9(selector).load("../PagComun/PagAcciones.aspx", { Accion: Titulo, Accion2: idTxt1, alias: f }, function () {
        jQuery_1_9('#myModalconsole.log h3').text('Modificar ' + Titulo);
        jQuery_1_9('#myModalconsole.log h4').text('Registro Modificado Correctamente');
        jQuery_1_9('#myModalconsole.log .modal-footer button').attr("onclick", "IrAPag(" + idTxt + ")");
        jQuery_1_9('#myModalconsole.log').modal('show');
    });
}
function Modificar(id, Titulo) {
    var idTxt1 = 'MODIFICAR'
    var idTxt = '#Id' + Titulo;
    jQuery_1_9(idTxt).val(id);
    var f = ObtenerAliasModEli();
    var selector = document.getElementById('divAgregar');
    jQuery_1_9(selector).load("../PagComun/PagAcciones.aspx", { Accion: Titulo, Accion2: idTxt1, alias: f }, function () {
        jQuery_1_9("#divAgregar").show();
        jQuery_1_9("#divResultado").hide();
        jQuery_1_9("#divFiltros").hide();
    });
}
function Eliminar(id, Accion) {
    var titulo
    switch (Accion) {
        case 'Asociacion':
            titulo = 'Asociaci�n'
            break;
        case 'Segmentos':
            titulo = 'Segmentos'
            break;
        default:
            titulo = Accion;
    }
    jQuery_1_9('#myModalConfirm h3').text('Eliminar ' + titulo);
    jQuery_1_9('#myModalConfirm h4').text('�Est� seguro desea eliminar el registro seleccionado?');
    jQuery_1_9('#myModalConfirm #btnAccion').attr("onclick", "EliminarBD('" + id + "','" + Accion + "')");
    jQuery_1_9('#myModalConfirm').modal('show');
}
function EliminarBD(id, Titulo) {
    var idTxt1 = 'BORRARBD'
    var idTxt = '#Id' + Titulo;
    var idTxt2 = "'1'," + "'" + Titulo + "'";
    jQuery_1_9(idTxt).val(id);
    var f = ObtenerAlias();
    var idTxt3 = '#Id' + Titulo;
    jQuery_1_9(idTxt3).val('');
    var selector = document.getElementById('divAgregar');
    jQuery_1_9(selector).load("../PagComun/ModalAcciones.aspx", { Accion: Titulo, Accion2: idTxt1, alias: f }, function () {
        jQuery_1_9('#myModalAlert h3').text('Eliminar ' + Titulo);
        jQuery_1_9('#myModalAlert h4').text('Registro Eliminado Correctamente');
        jQuery_1_9('#myModalAlert .modal-footer button').attr("onclick", "IrAPag(" + idTxt2 + ")");
        jQuery_1_9('#myModalAlert').modal('show');
    });
}
function LimpiarFiltros() {
    var elementos = jQuery_1_9('#divFiltros .form-control');
    jQuery_1_9.each(elementos, function (i, val) {
        switch (jQuery_1_9(val).prop('tagName')) {
            case "INPUT":
                jQuery_1_9(val).val('');
                break;
            case "SELECT":
                jQuery_1_9(val).val('');
                break;
        }
    });
}
function Limpiar(obj) {
    jQuery_1_9("#" + obj).val('');
}
function Cancelar() {
    jQuery_1_9("#divAgregar").hide();
    jQuery_1_9("#divResultado").show();
    jQuery_1_9("#divFiltros").show();
}
function Guardar(Titulo) {
    if (FormVal()) {
        NuevoBD(Titulo);
    }
}
function Guardar2(id, Titulo) {
    if (FormVal()) {
        ModificarBD(Titulo);
    }
}
function FormVal() {
    var msg = '';
    jQuery_1_9("#divAgregar .form-control").filter(function () { return jQuery_1_9(this).attr('Req') == 'S'; }).each(function () {
        if (trim(jQuery_1_9(this).val()) == '') {

            if (jQuery_1_9(this).attr('Tipo') == 'DatePicker') {
                msg += jQuery_1_9(this).parent().parent().children('label').html() + '\n';
            }
            else {
                msg += jQuery_1_9(this).parent().children('label').html() + '\n';
            }
        }
    })
    jQuery_1_9("#divAgregar .table-responsive tbody").filter(function () { return jQuery_1_9(this).attr('Req') == 'S'; }).each(function () {
        if (jQuery_1_9(this).attr('Tipo') == 'Filas') {
            var Cuantos = jQuery_1_9("#" + jQuery_1_9(this).prop("id") + " tr").length;
            if (Cuantos == 0) {
                msg += "Al Menos un " + jQuery_1_9("#divAgregar .table-responsive thead th").html() + '\n';
            }
        }
    })
    jQuery_1_9("#divAgregar .btn-group label input").filter(function () { return jQuery_1_9(this).attr('Req') == 'S'; }).each(function () {
        var mynombre = jQuery_1_9(this).prop("name");
        var Checkeado = '';
        jQuery_1_9("#divAgregar .btn-group label input").filter(function () { return jQuery_1_9(this).prop("name") == mynombre; }).each(function () {
            var valorID = jQuery_1_9(this).prop("id");
            if (jQuery_1_9("#" + valorID).is(':checked')) {
                Checkeado = 'S';
            }

        })
        if (Checkeado == '') {
            msg += jQuery_1_9(this).parent().parent().parent().children('label').html() + '\n';
        }
    })
    if (msg != '') {
        jQuery_1_9('#myModalconsole.log h3').text('console.loga');
        jQuery_1_9('#myModalconsole.log h4').html('Debe Ingresar los Siguientes Campos:\n' + msg);
        jQuery_1_9('#myModalconsole.log .modal-footer button').attr("onclick", "");
        jQuery_1_9('#myModalconsole.log').modal('show');
        return false;
    }
    return true;
}
function FormValF() {
    var msg = '';
    jQuery_1_9("#divFiltros .form-control").filter(function () { return jQuery_1_9(this).attr('Req') == 'S'; }).each(function () {
        if (trim(jQuery_1_9(this).val()) == '') {
            if (jQuery_1_9(this).attr('Tipo') == 'DatePicker') {
                msg += jQuery_1_9(this).parent().parent().children('label').html() + '\n';
            }
            else {
                msg += jQuery_1_9(this).parent().children('label').html() + '\n';
            }
        }
    })
    jQuery_1_9("#divFiltros .table-responsive tbody").filter(function () { return jQuery_1_9(this).attr('Req') == 'S'; }).each(function () {
        if (jQuery_1_9(this).attr('Tipo') == 'Filas') {
            var Cuantos = jQuery_1_9("#" + jQuery_1_9(this).prop("id") + " tr").length;
            if (Cuantos == 0) {
                msg += "Al Menos un " + jQuery_1_9("#divAgregar .table-responsive thead th").html() + '\n';
            }
        }
    })
    jQuery_1_9("#divFiltros .btn-group label input").filter(function () { return jQuery_1_9(this).attr('Req') == 'S'; }).each(function () {
        var mynombre = jQuery_1_9(this).prop("name");
        var Checkeado = '';
        jQuery_1_9("#divAgregar .btn-group label input").filter(function () { return jQuery_1_9(this).prop("name") == mynombre; }).each(function () {
            var valorID = jQuery_1_9(this).prop("id");
            if (jQuery_1_9("#" + valorID).is(':checked')) {
                Checkeado = 'S';
            }
        })
        if (Checkeado == '') {
            msg += jQuery_1_9(this).parent().parent().parent().children('label').html() + '\n';
        }
    })
    if (msg != '') {
        jQuery_1_9('#myModalconsole.log h3').text('console.loga');
        jQuery_1_9('#myModalconsole.log h4').html('Debe Ingresar los Siguientes Campos:\n' + msg);
        jQuery_1_9('#myModalconsole.log .modal-footer button').attr("onclick", "");
        jQuery_1_9('#myModalconsole.log').modal('show');
        return false;
    }
    return true;
}


function CambiaOrden(Obj, Titulo) {
    var OrdenarPor = jQuery_1_9(Obj).attr('OrdenarPor');
    var Ordenamiento = '';
    var Clase = jQuery_1_9(Obj).attr('TipoOrden');
    var Identificador = jQuery_1_9(Obj).prop('id');

    switch (Clase) {
        case "sorting":
            Ordenamiento = 'ASC';
            break;
        case "sorting_asc":
            Ordenamiento = 'DESC';
            break;
        case "sorting_desc":
            Ordenamiento = 'ASC';
            break;
    }

    jQuery_1_9('#ColOrder' + Titulo).val(OrdenarPor);
    jQuery_1_9('#Ordenamiento' + Titulo).val(Ordenamiento);
    var idTxt = '#divResultado #PagNumber' + Titulo;
    jQuery_1_9(idTxt).val('1');
    var f = ObtenerAlias();

    var selector = document.getElementById('divResultado');
    jQuery_1_9(selector).load("../PagComun/GrillaPaginada.aspx", { Accion: Titulo, alias: f }, function () {
        var Fila = jQuery_1_9(Obj).parent().prop('id');

        jQuery_1_9("#" + Fila + " th").each(function () {
            var Clase2 = jQuery_1_9(this).attr('TipoOrden');
            if (Clase2 != 'nosort') {
                jQuery_1_9(this).attr('TipoOrden', 'sorting');
            }
        });

        switch (Clase) {
            case "sorting":
                jQuery_1_9('#' + Identificador).attr('TipoOrden', 'sorting_asc');
                break;
            case "sorting_asc":
                jQuery_1_9('#' + Identificador).attr('TipoOrden', 'sorting_desc');
                break;
            case "sorting_desc":
                jQuery_1_9('#' + Identificador).attr('TipoOrden', 'sorting_asc');
                break;
        }
    });
}
function EliminarTemporal(Obj) {
    jQuery_1_9(Obj).parent().parent().remove();
}
function MostrarModal(MyModal, Titulo2, Titulo, ElDiv, divAlias) {
    var f = '';
    if (divAlias != '') {
        f = ObtenerAliasGeneral(divAlias);
    }
    var selector = document.getElementById(ElDiv);
    jQuery_1_9(selector).load("../PagComun/ModalAcciones.aspx", { Accion: Titulo, Accion2: Titulo2, alias: f }, function () {
        jQuery_1_9(MyModal).modal('show')
    });
}
function CerrarModal(modal) {
    jQuery_1_9('#' + modal).modal('hide');
    jQuery_1_9('.modal-backdrop').hide();
}
function SeteaMenu(id) {
    jQuery_1_9('#' + id).parent().addClass('nav in');
    jQuery_1_9(".nav li").removeClass("active");
    jQuery_1_9(".nav li a").removeClass('nav-li-a-Seccionado');
    jQuery_1_9('#' + id).addClass('active');
    jQuery_1_9('#' + id).find('a:first').addClass('nav-li-a-Seccionado');
}
function ChangePadreTabla(Obj, divTabla) {
    var IdPadre = jQuery_1_9('input[name=' + jQuery_1_9(Obj).attr('name') + ']:checked').val();;

    jQuery_1_9("#" + divTabla + " tr").each(function () {
        var IdHijo = jQuery_1_9(this).attr('idpadre');

        if (IdPadre == IdHijo) {
            jQuery_1_9(this).show();
        }
        else {
            jQuery_1_9(this).hide();
        }
    });
}
function CambiarInstitucionSede(Accion, divParametros) {

    if (!jQuery_1_9("#form").valid())
        return;

    var f = '';
    f = ObtenerAliasGeneral("#divform");
    var selector = document.getElementById(divParametros.replace('#', ''));
    jQuery_1_9(selector).load("../PagComun/ModalAcciones.aspx", { Accion2: Accion, alias: f }, function () {
        window.location.assign("../Admin/frm_inicio.aspx");
    });
}
function HideDiv(DivArray) {
    for (i = 0; i < DivArray.length; i++) {
        jQuery_1_9("#" + DivArray[i]).hide();
    }
}
function ClearHideDiv(DivArray) {
    for (i = 0; i < DivArray.length; i++) {
        jQuery_1_9("#" + DivArray[i]).hide();
        jQuery_1_9("#" + DivArray[i]).empty();
    }
}
function ShowDiv(DivArray) {
    for (i = 0; i < DivArray.length; i++) {
        jQuery_1_9("#" + DivArray[i]).show();
    }
}
function GetDate() {
    var today = new Date();
    var dd = today.getDate();
    var mm = today.getMonth() + 1;
    var yyyy = today.getFullYear();

    if (dd < 10) { dd = '0' + dd }
    if (mm < 10) { mm = '0' + mm }
    return dd + '/' + mm + '/' + yyyy;
}
function CerrarSesion(divParametros) {
    var f = '';
    var selector = document.getElementById(divParametros.replace('#', ''));
    jQuery_1_9(selector).load("../PagComun/PagAcciones.aspx", { Accion: 'CERRARSESION', Accion2: '', alias: f }, function () {
        jQuery_1_9(divParametros).modal('hide');
        jQuery_1_9('.modal-backdrop').hide();
        window.location.assign("../Admin/frm_sys_login.aspx");
    });
}
function ExportarExcel(Accion) {
    var f = SerializaAlias('#divFiltros,#divResultado');
    jQuery_1_9.post('../PagComun/Excel.aspx', { Accion: Accion, AliasForm: f },
    function (RutaArchivo) {
        if (RutaArchivo) {
            window.location.href = RutaArchivo
        }
    });
};

function disableF5(e) { if ((e.which || e.keyCode) == 116) e.preventDefault(); };
jQuery_1_9(document).on("keydown", disableF5);

function HabilitaBoton(boton) {
    jQuery_1_9('#' + boton).removeClass('disabled');
}

function SeleccionarTodos(Obj, divCheckbox) {
    jQuery_1_9("#" + divCheckbox).find('input[type=checkbox]').each(function () {
        this.checked = Obj.checked;
    });
}