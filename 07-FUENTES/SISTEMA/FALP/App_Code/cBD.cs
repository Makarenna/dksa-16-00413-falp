﻿using System;
using System.Data;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using System.Xml;

using System.Data.SqlClient;
using System.Data.SqlTypes;

using library.common;
using library.xml;
public class cBD
{
    public string EjecutaPA(string XML, string nombrePA)
    {
        string getxml = "";

        Proxy.ServiceSTClient ServicioProxy = new Proxy.ServiceSTClient();

        getxml = ServicioProxy.EjecutaPA(XML, nombrePA);

        ServicioProxy.Close();

        return getxml;
    }
    public string Ejecuta_PA_Oracle_Historico(string XML, string nombrePA)
    {
         string getxml = "";

        try
        {
            Proxy.ServiceSTClient ServicioProxy = new Proxy.ServiceSTClient();

            getxml = ServicioProxy.Ejecuta_PA_Oracle_Historico(XML, nombrePA);

            ServicioProxy.Close();
        }
        catch (Exception ex)
        {
            new Log().GuardaExcepcion("Ejecuta_PA_Oracle_Online", ex);
        }



        return getxml;
    }
    public string Ejecuta_PA_Oracle_Online(string XML, string nombrePA)
    {
        
        string getxml = "";

        try
        {
            Proxy.ServiceSTClient ServicioProxy = new Proxy.ServiceSTClient();

            getxml = ServicioProxy.Ejecuta_PA_Oracle(XML, nombrePA);

            ServicioProxy.Close();
        }
        catch (Exception ex)
        {
            new Log().GuardaExcepcion("Ejecuta_PA_Oracle_Online", ex);
        }



        return getxml;
    }
}