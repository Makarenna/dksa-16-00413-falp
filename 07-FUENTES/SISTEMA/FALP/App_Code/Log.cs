﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Web;


    public class Log
    {
         public void GuardaExcepcion(string clase, Exception ex)
        {
            string Ruta_Archivo_log = System.Configuration.ConfigurationManager.AppSettings["RutaLog"];

            string linea = string.Format("<hora>{0}</hora><clase>{1}</clase><Source>{2}<Source><Message>{3}</Message><StackTrace>{4}</StackTrace>", DateTime.Now.ToString("HH:mm:ss tt"), clase, ex.Source, ex.Message, ex.StackTrace);
            string nombreArchivo = string.Format("Front_FALP_Exception_{0}.txt", DateTime.Now.ToString("yyyy-MM-dd"));

            StringBuilder sb = new StringBuilder();
            sb.AppendLine(linea);

            File.AppendAllText(Path.Combine(Ruta_Archivo_log, nombreArchivo), sb.ToString());
            sb.Clear();
        }
        public void GuardaLog(string clase, string Source, string Message, string StackTrace)
        {
            string Ruta_Archivo_log = System.Configuration.ConfigurationManager.AppSettings["RutaLog"];

            string linea = string.Format("<hora>{0}</hora><clase>{1}</clase><Source>{2}<Source><Message>{3}</Message><StackTrace>{4}</StackTrace>", DateTime.Now.ToString("HH:mm:ss tt"), clase, Source, Message, StackTrace);
            string nombreArchivo = string.Format("Front_FALP_Log_{0}.txt", DateTime.Now.ToString("yyyy-MM-dd"));

            StringBuilder sb = new StringBuilder();
            sb.AppendLine(linea);

            File.AppendAllText(Path.Combine(Ruta_Archivo_log, nombreArchivo), sb.ToString());
            sb.Clear();
        }
    }