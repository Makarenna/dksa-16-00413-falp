﻿using System;
using System.Data;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using System.Xml;

using System.Data.SqlClient;
using System.Data.SqlTypes;

using library.common;
using library.xml;
using System.IO;
public class Wcf
{
    public string Carga_Archivo_Temporal(string NombreArchivo, string TipoArchivo, string AgnoCarga, string PeriodoCarga, string MesCarga)
    {
        string resultado = "";
        byte[] Archivo = null;
        Proxy.ServiceSTClient ServicioProxy = new Proxy.ServiceSTClient();

        try
        {
            string dirFullPath = HttpContext.Current.Server.MapPath("~/temp/");

            if (!File.Exists(dirFullPath + NombreArchivo))
            {
                return "Archivo no Existe";
            }

            Archivo = File.ReadAllBytes(dirFullPath + NombreArchivo);

            //resultado = ServicioProxy.Carga_Archivo_Temporal(Archivo, NombreArchivo, TipoArchivo, AgnoCarga, PeriodoCarga, MesCarga);

        }
        catch (Exception ex)
        {
            new Log().GuardaLog("Wcf:Carga_Archivo_Temporal", ex.Source, ex.Message, ex.StackTrace);
        }
        finally
        {
            ServicioProxy.Close();
        }
        return resultado;
    }
}