﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

using System.Xml;
using System.Xml.Xsl;
using System.IO;
using System.Data.SqlClient;

using library.xml;
using library.common;
using library.Security;
using System.Drawing;
using Newtonsoft.Json;

public class PaginaBase : System.Web.UI.Page
{
    public XmlDocument XmlBase { get; set; }
    public XmlDocument XmlParametrosSql { get; set; }
    public string sesID { get; set; }
    public string usuID { get; set; }
    public string usuNombre { get; set; }
    public string perID { get; set; }
    public string tipoID { get; set; }
    public string codCoo { get; set; }
    public string codSup { get; set; }
    
    protected void Page_Init(object sender, EventArgs e)
    {
        XmlDocument XmlAuxA = null, XmlAuxB = null;

        try
        {
            XmlBase = XmlHelper.CrearPaginaEsquemaAll(XmlBase, this);
            frm_sys_common Comunes = new frm_sys_common();

            XmlAuxB = Comunes.ValidaSesionPage(Request);

            String IsOK = XmlHelper.strGetNodoGetAttributo(XmlAuxB, "//LaSesion/Datos/row", "ISOK");

            sesID = XmlHelper.strGetNodoGetAttributo(XmlAuxB, "//LaSesion/Datos/row", "SESID");
            usuID = XmlHelper.strGetNodoGetAttributo(XmlAuxB, "//LaSesion/Datos/row", "USUID");
            perID= XmlHelper.strGetNodoGetAttributo(XmlAuxB, "//LaSesion/Datos/row", "PERID");
            tipoID = XmlHelper.strGetNodoGetAttributo(XmlAuxB, "//LaSesion/Datos/row", "TIPOID");
            codCoo = XmlHelper.strGetNodoGetAttributo(XmlAuxB, "//LaSesion/Datos/row", "COOID");
            codSup = XmlHelper.strGetNodoGetAttributo(XmlAuxB, "//LaSesion/Datos/row", "SUPID");
            usuNombre = XmlHelper.strGetNodoGetAttributo(XmlAuxB, "//LaSesion/Datos/row", "NOMBREUSUARIO");
        }
        catch (Exception ex)
        {
            new Log().GuardaExcepcion("PaginaBase:Page_Init", ex);
        }
    }
    public void SerializaJson(ref XmlDocument xml, string json)
    {
        if (json.Equals(string.Empty))//si el json viene vacio, salgo
            return;
        try
        {
            Dictionary<string, string> DiccionarioForm = JsonConvert.DeserializeObject<Dictionary<string, string>>(json);

            foreach (KeyValuePair<string, string> item in DiccionarioForm)
            {
                XmlHelper.StpAgregarNodoParametros(xml, item.Key, item.Value);
            }
        }
        catch (Exception ex)
        {
            new Log().GuardaLog("PaginaBase:SerializaJson", ex.Source, ex.Message, ex.StackTrace);
        }
    }
    public void SerializaDiccionarioJson(ref XmlDocument xml, string json)
    {
        if (json.Equals(string.Empty))//si el json viene vacio, salgo
            return;
        try
        {
            List<Dictionary<string, string>> DiccionarioTabla = JsonConvert.DeserializeObject<List<Dictionary<string, string>>>(json);
            XmlHelper.StpAgregarNodoParametros(xml, "tabla", "");

            foreach (Dictionary<string, string> registro in DiccionarioTabla)
            {
                XmlElement newElem = xml.CreateElement("fila");
                foreach (KeyValuePair<string, string> item in registro)
                {
                    XmlAttribute newAttr = xml.CreateAttribute(item.Key);
                    newAttr.Value = item.Value;
                    XmlAttributeCollection attrColl = newElem.Attributes;
                    attrColl.SetNamedItem(newAttr);
                    xml.SelectSingleNode("//tabla").AppendChild(newElem);
                }
            }
        }
        catch (Exception ex)
        {
            new Log().GuardaLog("PaginaBase:SerializaDiccionarioJson", ex.Source, ex.Message, ex.StackTrace);
        }
    }

    public void SerializaDiccionarioJsonTabla(ref XmlDocument xml, string json, string nombreTabla)
    {
        if (json.Equals(string.Empty))//si el json viene vacio, salgo
            return;
        try
        {
            List<Dictionary<string, string>> DiccionarioTabla = JsonConvert.DeserializeObject<List<Dictionary<string, string>>>(json);
            XmlHelper.StpAgregarNodoParametros(xml, nombreTabla, "");

            foreach (Dictionary<string, string> registro in DiccionarioTabla)
            {
                XmlElement newElem = xml.CreateElement("fila");
                foreach (KeyValuePair<string, string> item in registro)
                {
                    XmlAttribute newAttr = xml.CreateAttribute(item.Key);
                    newAttr.Value = item.Value;
                    XmlAttributeCollection attrColl = newElem.Attributes;
                    attrColl.SetNamedItem(newAttr);
                    xml.SelectSingleNode(string.Concat("//",nombreTabla)).AppendChild(newElem);
                }
            }
        }
        catch (Exception ex)
        {
            new Log().GuardaLog("PaginaBase:SerializaDiccionarioJson", ex.Source, ex.Message, ex.StackTrace);
        }
    }
}
