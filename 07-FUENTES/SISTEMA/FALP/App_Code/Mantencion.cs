﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;

/// <summary>
/// Descripción breve de Mantencion
/// </summary>
public class Mantencion
{
	public void LimpiarCarpeta(string RutaCarpeta, int Antiguedad)
	{
        string[] files = Directory.GetFiles(RutaCarpeta);

        foreach (string file in files)
        {
            FileInfo fi = new FileInfo(file);
            if (fi.CreationTime < DateTime.Now.AddMinutes(-Antiguedad))
                fi.Delete();
        }
	}
}