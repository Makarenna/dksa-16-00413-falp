﻿using System;
using System.Data;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;

//agregadas     
using System.Xml;
using System.Xml.Xsl;
using System.IO;
using System.Data.SqlClient;

using library.xml;
using library.common;
using library.Security;
using System.Web.Mail;


public class frm_sys_common : System.Web.UI.Page
{

    Page Pag = null;

    //public frm_sys_common() {
    //    Pag=this;
    //}

    public string getCuentaWindows()
    {
        string strName = HttpContext.Current.User.Identity.Name.ToString();
        string[] split = strHelper.SplitStrToArray(strName, "/");
        return strName;
    }

    /// <summary>
    /// If conditio is true, then return Object TheThen, else return object TheElse
    /// </summary>
    /// <param name="condition">conditio to test</param>
    /// <param name="TheThen">the object to return if condition is true</param>
    /// <param name="TheElse">the object to return if condition is true</param>
    /// <returns></returns>
    public object IsTrueThenElse(bool condition, object TheThen, object TheElse)
    {
        if (condition)
        {
            return TheThen;
        }
        else
        {
            return TheElse;
        }
    }


    //microsoft sql
    private string GetSQLConnectionString()
    {
        return cnfHelper.GetConfigAppVariable("connSQL");
    }

    /// <summary>
    /// Sube archivos directamente a la BD
    /// </summary>
    /// <param name="File1">Archivo</param>
    /// <param name="Sistema"></param>
    /// <param name="RutaVirtual"></param>
    /// <returns>Devuelve el ID del archivo subido a la BD, si no se subio devuelve -1</returns>
    //public string UploadFileBD(System.Web.HttpPostedFile File1, string Sistema, string RutaVirtual, bool MostrarProgreso) {
    //    int SizeBytes=File1.ContentLength;
    //    string Nombre, ext, ContentType;

    //    if(SizeBytes>0) {

    //        Nombre=Path.GetFileName(File1.FileName);
    //        ext=Path.GetExtension(File1.FileName);
    //        ContentType=File1.ContentType;

    //        try {

    //            //Get the binary array directly from the control
    //            byte[] binary=new byte[SizeBytes];
    //            Stream objStream=File1.InputStream;

    //            //aqui deberia demorarse en subir a server de APP 50% del progreso
    //            if(MostrarProgreso) {
    //                //calculo de avance, subida por streaming
    //                int PAva=0;
    //                const int ChunkSize=1024;
    //                byte[] buffer=new byte[ChunkSize];
    //                int SizeToWrite=ChunkSize;

    //                for(int i=0; i<SizeBytes-1; i=i+ChunkSize) {
    //                    if(!Response.IsClientConnected)
    //                        return "-1";
    //                    if(i+ChunkSize>=SizeBytes)
    //                        SizeToWrite=SizeBytes-i;
    //                    byte[] chunk=new byte[SizeToWrite];
    //                    objStream.Read(binary, i, SizeToWrite);

    //                    if(PAva!=(100*i/SizeBytes)) {
    //                        //********pausa solo debug*************
    //                        //System.Threading.Thread.Sleep(500);
    //                        //*************************************
    //                        PAva=(100*i/SizeBytes);
    //                        Response.Write("<script>SetProgBar("+PAva+");</script>");
    //                        Response.Flush();
    //                    }//fin_if
    //                }//fin_for
    //            } else
    //                objStream.Read(binary, 0, SizeBytes);//subida de una , sin avance


    //            //obtener datos de conección
    //            string connectionString=GetSQLConnectionString();

    //            //conectar
    //            using(SqlConnection connection=new SqlConnection(connectionString)) {
    //                connection.Open();

    //                SqlParameter param=null;
    //                SqlCommand cmd=new SqlCommand("RIS_I_Archivo", connection);
    //                cmd.CommandType=CommandType.StoredProcedure;

    //                //Parameters and values 
    //                param=new SqlParameter("@nombre", SqlDbType.VarChar);
    //                param.Value=Nombre;
    //                cmd.Parameters.Add(param);

    //                param=new SqlParameter("@data", SqlDbType.VarBinary);
    //                param.Direction=ParameterDirection.Input;
    //                param.Value=binary;
    //                cmd.Parameters.Add(param);

    //                param=new SqlParameter("@ext", SqlDbType.VarChar);
    //                param.Value=ext;
    //                cmd.Parameters.Add(param);

    //                param=new SqlParameter("@sistema", SqlDbType.VarChar);
    //                param.Value=Sistema;
    //                cmd.Parameters.Add(param);

    //                param=new SqlParameter("@ruta_virtual", SqlDbType.VarChar);
    //                param.Value=RutaVirtual;
    //                cmd.Parameters.Add(param);

    //                param=new SqlParameter("@size_bytes", SqlDbType.Int);
    //                param.Value=SizeBytes;
    //                cmd.Parameters.Add(param);

    //                param=new SqlParameter("@ContentType", SqlDbType.VarChar);
    //                param.Value=ContentType;
    //                cmd.Parameters.Add(param);

    //                param=new SqlParameter("@user_created", SqlDbType.VarChar);
    //                param.Value=sessionHelper.GetSession("UserId", "NoUser", this);
    //                cmd.Parameters.Add(param);

    //                //aca se demora en enviar al server de BD el otro 50% del progreso
    //                string r=cmd.ExecuteScalar().ToString();
    //                if(MostrarProgreso) {
    //                    Response.Write("<script>SetProgBar(100);</script>");
    //                    Response.Flush();
    //                }
    //                return r;//devuelve el id del archivo						
    //            }//fin_using 			

    //            //return true;
    //        } catch(Exception ex) {
    //            //no se copio el archivo mandar el error
    //            Response.Write(ex.Message);
    //            return "-1";
    //        }//fin TRY

    //    }//fin IF 

    //    return "-1";
    //}

    //public void DownloadFileBD(string id_arch) {
    //    DownloadFileBD(id_arch, false);
    //}

    //public void DownloadFileBD(string id_arch, bool bajar) {
    //    //Connection and Parameters
    //    SqlParameter param=null;
    //    SqlConnection conn=new SqlConnection(GetSQLConnectionString());

    //    SqlCommand cmd=new SqlCommand("RIS_S_Archivo", conn);
    //    cmd.CommandType=CommandType.StoredProcedure;
    //    param=new SqlParameter("@IdArch", SqlDbType.Decimal);
    //    param.Direction=ParameterDirection.Input;
    //    param.Value=id_arch;
    //    cmd.Parameters.Add(param);

    //    //Open connection and fetch the data with reader
    //    conn.Open();
    //    SqlDataReader reader=cmd.ExecuteReader(CommandBehavior.CloseConnection);

    //    if(reader.HasRows) {
    //        reader.Read();
    //        //
    //        string doctype=reader["ContentType"].ToString();
    //        string docname=reader["Nombre"].ToString();
    //        //
    //        Response.Buffer=false;
    //        Response.ClearHeaders();
    //        Response.ContentType=doctype;

    //        Response.AddHeader("Content-Length", reader["SizeBytes"].ToString());
    //        if(bajar)
    //            Response.AddHeader("Content-Disposition", "attachment; filename="+docname); //obliga a descargar o abrir el archivo, no lo muestra en la ventana.

    //        //Code for streaming the object while writing
    //        const int ChunkSize=1024;
    //        byte[] buffer=new byte[ChunkSize];
    //        byte[] binary=(reader["Data"]) as byte[];
    //        MemoryStream ms=new MemoryStream(binary);
    //        int SizeToWrite=ChunkSize;

    //        for(int i=0; i<binary.GetUpperBound(0)-1; i=i+ChunkSize) {
    //            if(!Response.IsClientConnected)
    //                return;
    //            if(i+ChunkSize>=binary.Length)
    //                SizeToWrite=binary.Length-i;
    //            byte[] chunk=new byte[SizeToWrite];
    //            ms.Read(chunk, 0, SizeToWrite);
    //            Response.BinaryWrite(chunk);
    //            Response.Flush();
    //        }
    //        Response.Close();
    //    } else {
    //        Response.Write("File not Found.");
    //        Response.End();
    //    }
    //}//fin_DownloadFileBD

    private void GetDates(ref System.DateTime stDate, ref System.DateTime endDate)
    {
        double offset = 0;
        switch (System.DateTime.Today.DayOfWeek)
        {
            case DayOfWeek.Monday:
                offset = 0;
                break;
            case DayOfWeek.Tuesday:
                offset = -1;
                break;
            case DayOfWeek.Wednesday:
                offset = -2;
                break;
            case DayOfWeek.Thursday:
                offset = -3;
                break;
            case DayOfWeek.Friday:
                offset = -4;
                break;
            case DayOfWeek.Saturday:
                offset = -5;
                break;
            case DayOfWeek.Sunday:
                offset = -6;
                break;
        }
        endDate = System.DateTime.Today.AddDays(offset - 1);
        stDate = System.DateTime.Today.AddDays(-7 + offset);
    }


    public string DT2CD(DateTime inFecha)
    {
        string fecha = DateTime.Now.Day + "/" + DateTime.Now.Month + "/" + DateTime.Now.Year;

        fecha = ((inFecha.Day < 10) ? "0" : "") + inFecha.Day + "/" + ((inFecha.Month < 10) ? "0" : "") + inFecha.Month + "/" + inFecha.Year;

        return fecha;
    }

    public DateTime CD2DT(string inFecha)
    {
        int dia = 1;
        int mes = 1;
        int anno = 1900;

        dia = Convert.ToInt32(inFecha.Substring(0, 2));
        mes = Convert.ToInt32(inFecha.Substring(3, 2));
        anno = Convert.ToInt32(inFecha.Substring(6, 4));

        return new DateTime(anno, mes, dia);
    }

    public void topframe(XmlDocument XmlFinal)
    {

        XmlDocument XmlAuxA = null, XmlAuxC = null;


        //String Perfil = XmlHelper.strGetNodoGetAttributo(XmlFinal, "//app/LaSesion/Datos/row", "perID");
        String sesID = XmlHelper.strGetNodoGetAttributo(XmlFinal, "//app/LaSesion/Datos/row", "sesID");

        XmlAuxA = XmlHelper.StpCrearParametros();
        XmlHelper.StpAgregarNodoParametros(XmlAuxA, "sesID", sesID);
        //XmlHelper.StpAgregarNodoParametros(XmlAuxA, "IdPerfil", "5");
        XmlAuxC = XmlHelper.CrearXmlDocumentConGetRow("Menus", new cBD().EjecutaPA(XmlAuxA.OuterXml, "MSS_S_AppPorPerfil"));
        XmlHelper.UnirArbol(XmlFinal, XmlAuxC, "//app");

    }

    public XmlDocument ValidaSesion(string url, string Direccion)
    {
        XmlDocument XmlAuxA = null, XmlAuxC = null, XmlAuxB = null, XmlAuxD = null;


        XmlAuxA = XmlHelper.StpCrearParametros();
        XmlHelper.StpAgregarNodoParametros(XmlAuxA, "Tipo", "Validar");
        try
        {
            XmlHelper.StpAgregarNodoParametros(XmlAuxA, "sesID", Session["sesID"].ToString());
        }
        catch (Exception)
        {
            XmlHelper.StpAgregarNodoParametros(XmlAuxA, "sesID", "");
        }

        XmlHelper.StpAgregarNodoParametros(XmlAuxA, "Url", url);
        XmlHelper.StpAgregarNodoParametros(XmlAuxA, "ses_remote_addr", Direccion);
        XmlAuxC = XmlHelper.CrearXmlDocumentConGetRow("LaSesion", new cBD().Ejecuta_PA_Oracle_Historico(XmlAuxA.OuterXml, "DK_VALIDA_SESION"));


        return XmlAuxC;

    }

    public XmlDocument ValidaSesionPage(System.Web.HttpRequest Request)
    {
        XmlDocument XmlAuxA = null, XmlAuxC = null, XmlAuxB = null, XmlAuxD = null;

        XmlAuxA = XmlHelper.StpCrearParametros();
         try
        {
            XmlHelper.StpAgregarNodoParametros(XmlAuxA, "sesID", Session["sesID"].ToString());
        }
        catch (Exception)
        {
            XmlHelper.StpAgregarNodoParametros(XmlAuxA, "sesID", "");
        }

        XmlHelper.StpAgregarNodoParametros(XmlAuxA, "ses_remote_addr", Request.ServerVariables["REMOTE_ADDR"].ToString());
        XmlHelper.StpAgregarNodoParametros(XmlAuxA, "ses_remote_user", Request.ServerVariables["REMOTE_USER"].ToString());
        XmlHelper.StpAgregarNodoParametros(XmlAuxA, "ses_remote_host", Request.ServerVariables["REMOTE_HOST"].ToString());
        XmlHelper.StpAgregarNodoParametros(XmlAuxA, "ses_user_agent", Request.ServerVariables["HTTP_USER_AGENT"].ToString());
        XmlHelper.StpAgregarNodoParametros(XmlAuxA, "ses_language", Request.ServerVariables["HTTP_ACCEPT_LANGUAGE"].ToString());
        XmlHelper.StpAgregarNodoParametros(XmlAuxA, "ses_referer", Request.ServerVariables["HTTP_REFERER"].ToString());
        XmlHelper.StpAgregarNodoParametros(XmlAuxA, "ses_cookie", (Request.ServerVariables["HTTP_COOKIE"] == null ? "" : Request.ServerVariables["HTTP_COOKIE"].ToString()));
        XmlHelper.StpAgregarNodoParametros(XmlAuxA, "ses_host", Request.ServerVariables["HTTP_HOST"].ToString());
        
        XmlAuxC = XmlHelper.CrearXmlDocumentConGetRow("LaSesion", new cBD().Ejecuta_PA_Oracle_Historico(XmlAuxA.OuterXml, "PCK_CONV_DKERNEL.DK_VALIDA_SESION"));
        
        return XmlAuxC;
    }
    public XmlDocument CerrarSesion()
    {
        XmlDocument XmlAuxA = null, XmlAuxC = null, XmlAuxB = null, XmlAuxD = null;


        XmlAuxA = XmlHelper.StpCrearParametros();
        XmlHelper.StpAgregarNodoParametros(XmlAuxA, "Tipo", "CerrarSesion");
        try
        {
            XmlHelper.StpAgregarNodoParametros(XmlAuxA, "sesID", Session["sesID"].ToString());
        }
        catch (Exception)
        {
            XmlHelper.StpAgregarNodoParametros(XmlAuxA, "sesID", "");
        }

        //XmlHelper.StpAgregarNodoParametros(XmlAuxA, "Url", url);
        //XmlHelper.StpAgregarNodoParametros(XmlAuxA, "ses_remote_addr", Direccion);
        XmlAuxC = XmlHelper.CrearXmlDocumentConGetRow("LaSesion", new cBD().EjecutaPA(XmlAuxA.OuterXml, "PCK_CONV_DKERNEL.MSS_SESION"));


        return XmlAuxC;

    }





    public XmlDocument GetLastYears(string year, string cuantos)
    {
        XmlDocument XmlAuxA = null;
        int anno = Convert.ToInt32(year);
        int anno2k4 = anno - Convert.ToInt32(cuantos);
        XmlAuxA = XmlHelper.CrearRaizXdoc("ANNOS");
        for (int i = anno; i >= anno2k4; i--)
        {
            XmlAuxA = XmlHelper.AgregaNodoConTexto(XmlAuxA, "//ANNOS", "ANNO", Convert.ToString(i));
        }

        return XmlAuxA;
    }   
}
