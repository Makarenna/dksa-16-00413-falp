<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
  <xsl:output method="html" version="1.0" encoding="ISO-8859-1" indent="no" omit-xml-declaration="yes" standalone="yes" media-type="text/html" />
  <xsl:include href="../XslComun/Encabezado.xsl"/>
  <xsl:include href="../XslComun/JavaScript.xsl"/>
  <xsl:template match="/">
    <xsl:choose>
      <xsl:when test="//app/Accion = 'HTML'">
        <xsl:text disable-output-escaping="yes">&lt;!DOCTYPE html&gt;</xsl:text>
        <html>
          <head>
            <script src="http://ajax.googleapis.com/ajax/libs/jquery/1.10.2/jquery.min.js"></script>
            <script src="../bower_components/bootstrap/dist/js/bootstrap.min.js"></script>
            <script src="../js/moment-with-locales.js"></script>
            <script src="../js/bootstrap-datetimepicker.js"></script>
            
            <!--<xsl:call-template name="DibujaJavaScript" />-->
            <script type="text/javascript">
              var jQuery_1_9 = $.noConflict(true);
            </script>
            
            <script src="../dist/js/sb-admin-2.js"></script>            
            <script src="../js/Comunes.js"></script>
            
            
            <!--<xsl:call-template name="DibujaJS_header" />-->            
            <xsl:call-template name="DibujaEncabezado" />
            <script src="inicio.js"></script>
          </head>
          <body style="zoom: 0.9;">
            <div id="page-wrapper" style="min-height: 626px;">
              <div id="DivHidden">
                <input type="hidden" id="h_coordinador" name="h_coordinador" alias="coordinador" value="">
                   <xsl:attribute name="value">
                    <xsl:value-of select="//app/Cod_coo" />
                  </xsl:attribute>
                </input>
                <input type="hidden" id="h_supervisor" name="h_supervisor" alias="supervisor" value="">
                   <xsl:attribute name="value">
                    <xsl:value-of select="//app/Cod_sup" />
                  </xsl:attribute>
                </input>
                <input type="hidden" id="h_perfil" name="h_perfil" alias="perfil" value="">
                  <xsl:attribute name="value">
                    <xsl:value-of select="//app/usuario_perfil" />
                  </xsl:attribute>
                </input>
                <input id="origen" type="hidden" value="origen">
                <xsl:attribute name="value">
                  <xsl:value-of select="//app/origen" />
                </xsl:attribute>
              </input>
              </div>
              <div class="row">
                <div class="col-lg-12 col-md-12 col-xs-12">
                  <h4 class="page-header">
                    <xsl:value-of select="//app/Tipo"/> | <xsl:value-of select="//app/usuario_perfil"/> | <xsl:value-of select="//app/usuario_nombre"/>
                  </h4>
                </div>
              </div>
              <div class="row">
                <div class="col-lg-4 col-md-4 col-xs-4 ">
                  <div class="panel panel-default">
                    <div class="panel-body">
                      <a class="btn btn-block btn-social btn-bitbucket" onclick="IrALink(this);">
                        <xsl:attribute name="url">
                          <xsl:value-of select="//app/LinkContratos" />
                        </xsl:attribute>
                        <i class="fa fa-suitcase"></i> Consulta Contrato
                      </a>
                    </div>
                  </div>
                </div>
                <div class="col-lg-4 col-md-4 col-xs-4 ">
                  <div class="panel panel-default">
                    <div class="panel-body">
                      <a class="btn btn-block btn-social btn-bitbucket" onclick="IrALink(this);">
                        <xsl:attribute name="url">
                          <xsl:value-of select="//app/LinkEmpresas"/>
                        </xsl:attribute>
                        <xsl:attribute name="tipo">
                          <xsl:value-of select="//app/Tipo"/>
                        </xsl:attribute>
                        <xsl:attribute name="cod_coo">
                          <xsl:value-of select="//app/Cod_coo"/>
                        </xsl:attribute>
                        <xsl:attribute name="cod_sup">
                          <xsl:value-of select="//app/Cod_sup"/>
                        </xsl:attribute>
                        <i class="fa fa-institution"></i> Consulta Empresa
                      </a>
                    </div>
                  </div>
                </div>
                <div class="col-lg-4 col-md-4 col-xs-4 ">
                  <div class="panel panel-default">
                    <div class="panel-body">
                      <a class="btn btn-block btn-social btn-bitbucket " onclick="IrALink(this);">
                        <xsl:attribute name="url">
                          <xsl:value-of select="//app/LinkEmpresas"/>
                        </xsl:attribute>
                        <xsl:attribute name="tipo">
                          <xsl:value-of select="//app/Tipo"/>
                        </xsl:attribute>
                        <xsl:attribute name="cod_coo">
                          <xsl:value-of select="//app/Cod_coo"/>
                        </xsl:attribute>
                        <xsl:attribute name="cod_sup">
                          <xsl:value-of select="//app/Cod_sup"/>
                        </xsl:attribute>
                        <i class="fa fa-file-o"></i> Consulta Planilla
                      </a>
                    </div>
                  </div>
                </div>
              </div>
              <div class="panel-group" id="accordion">
                <div class="panel panel-default">
                  <div class="panel-heading borde-heading">
                    <div class="col-lg-4 col-md-4 col-xs-4 fondo">
                      <div class="row" style="display: inline-block;">
                        <div class="col-lg-2 col-md-2 col-xs-2">
                         <i class="fa fa-users fa-4x"></i>
                        </div>
                        <div class="col-lg-10 col-md-10 col-xs-10">
                          <h4 class="panel-title text-center" style="font-size:16px">
                            <a id="" data-toggle="collapse" data-parent="#accordion" href="#Col_INICIO_PRODUCCION">Producción </a>
                          </h4>
                        </div>
                      </div>
                    </div>
                    <div id="" class="col-lg-8 col-md-8 col-xs-8">
                      <div class="table-responsive">
                        <table class="table">
                          <thead>
                            <tr>
                              <th class="chico pull-left">Env a Admin</th>
                              <th class="chico pull-left">Env a CM</th>
                              <th class="chico pull-left">Env a Reg</th>
                              <th class="chico pull-left">Aprobados</th>
                            </tr>
                          </thead>
                          <tbody id="TotalProduccion" name="TotalProduccion">
                          </tbody>
                        </table>
                      </div>
                    </div>
                  </div>
                  <div id="Col_INICIO_PRODUCCION" class="panel-collapse collapse">
                    <div class="panel-body">
                      <div role="form" id="DivFiltros_INICIO_PRODUCCION">
                        <div class="col-lg-2 col-md-2 col-xs-2 form-group">
                          <label style="margin-top: 10px;">Fecha Desde :</label>
                        </div>
                        <div class="col-lg-2 col-md-2 col-xs-2 form-group date" >
                          <input type="text" class="form-control" id="fechadesde_produccion" name="fechadesde_produccion" alias="fecha_desde">
                            <xsl:attribute name="value">
                              <xsl:value-of select="//app/fecha_desde"/>
                            </xsl:attribute>
                          </input>
                        </div>
                        <div class="col-lg-2 col-md-2 col-xs-2 form-group">
                          <label style="margin-top: 10px;">Fecha Hasta :</label>
                        </div>
                        <div class="col-lg-2 col-md-2 col-xs-2 form-group date" >
                          <input type="text" class="form-control" id="fechahasta_produccion" name="fechahasta_produccion" alias="fecha_hasta">
                            <xsl:attribute name="value">
                              <xsl:value-of select="//app/fecha_hasta"/>
                            </xsl:attribute>
                          </input>
                        </div>
                        <div class="col-lg-1 col-md-1 col-xs-2 text-center">
                          <button type="submit" style="background-color: #205081; color: #FFF;" class="btn " onclick="Buscar(1, 'INICIO_PRODUCCION');return false;">Consultar</button>
                        </div>
                        <div class="col-lg-1 col-md-1 col-xs-2 text-center">
                          <button type="reset" style="background-color: #205081; color: #FFF;" class="btn " onclick="LimpiarInicioProduccion();">Limpiar</button>
                        </div>
                      </div>
                      <div id="DivResultado_INICIO_PRODUCCION">
                        <div class="row">
                          <div class="col-lg-12 col-md-12 col-xs-12" >
                            <div class="panel panel-default">
                              <div class="panel-body">
                                <div class="dataTable_wrapper">
                                  <table width="100%" class="table table-striped table-bordered table-hover" id="dataTables-example">
                                    <thead>
                                      <tr>
                                        <th>Equipo</th>
                                        <th>Enviados a Adm</th>
                                        <th>Enviados a CM</th>
                                        <th>Enviados a Registro</th>
                                        <th>Aprobados</th>
                                        <th>Listado</th>
                                      </tr>
                                    </thead>
                                    <tbody>
                                      <tr class="odd gradeX" style="background-color: #205081; color: #FFF; ">
                                        <td class="text-center">TOTAL</td>
                                        <td class="text-center">0</td>
                                        <td class="text-center">0</td>
                                        <td class="text-center">0</td>
                                        <td class="text-center">0</td>
                                        <td class="text-center"></td>
                                      </tr>
                                    </tbody>
                                  </table>
                                </div>
                              </div>
                            </div>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
                <div class="panel panel-default">
                  <div class="panel-heading borde-heading">
                    <div class="col-lg-4 col-md-4  col-xs-4 fondo">                      
                      <h4 class="panel-title text-center">
                        <i class="fa fa-tasks fa-2x"></i>
                        <br/>
                        <br/>
                        <a data-toggle="collapse" data-parent="#accordion" href="#Col_INICIO_PENDIENTES">Pendientes </a>
                      </h4>
                    </div>
                    <div id="" class="col-lg-8 col-md-8 col-xs-8">
                      <div class="table-responsive">
                        <table class="table">
                          <thead>
                            <tr>
                              <th class="chico pull-left">Contraloria</th>
                              <th class="chico pull-left">DTA-CRM</th>
                              <th class="chico pull-left">Total</th>
                            </tr>
                          </thead>
                          <tbody id="TotalPendientes" name="TotalPendientes">
                          </tbody>
                        </table>
                      </div>
                    </div>
                  </div>
                  <div id="Col_INICIO_PENDIENTES" class="panel-collapse collapse">
                    <div class="panel-body">
                      <div role="form" id="DivFiltros_INICIO_PENDIENTES">
                        <div class="col-lg-2 col-md-2 col-xs-2  form-group">
                          <label style="margin-top: 10px;">Fecha Desde :</label>
                        </div>
                        <div class="col-lg-2 col-md-2 col-xs-2 form-group">
                          <input type="text" class="form-control" id="fechadesde_pendientes" name="fechadesde_pendientes" alias="fecha_desde">
                            <xsl:attribute name="value">
                              <xsl:value-of select="//app/fecha_desde"/>
                            </xsl:attribute>
                          </input>
                        </div>
                        <div class="col-lg-2 col-md-2 col-xs-2 form-group">
                          <label style="margin-top: 10px;">Fecha Hasta :</label>
                        </div>
                        <div class="col-lg-2 col-md-2 col-xs-2 form-group">
                          <input type="text" class="form-control" id="fechahasta_pendientes" name="fechahasta_pendientes" alias="fecha_hasta">
                            <xsl:attribute name="value">
                              <xsl:value-of select="//app/fecha_hasta"/>
                            </xsl:attribute>
                          </input>
                        </div>
                        <div class="col-lg-1 col-md-1 col-xs-2 text-center">
                          <button type="submit" style="background-color: #205081; color: #FFF;" class="btn" onclick="Buscar(1, 'INICIO_PENDIENTES');return false;">Consultar</button>
                        </div>
                        <div class="col-lg-1 col-md-1 col-xs-2 text-center">
                          <button type="reset" style="background-color: #205081; color: #FFF;" class="btn " onclick="LimpiarInicioPendiente();">Limpiar</button>
                        </div>
                      </div>
                      <br/>
                      <div id="DivResultado_INICIO_PENDIENTES">
                        <div class="row">
                          <div class="col-lg-12 col-md-12 col-xs-12">
                            <div class="panel panel-default">
                              <div class="panel-body">
                                <div class="dataTable_wrapper">
                                  <table width="100%" class="table table-striped table-bordered table-hover" id="dataTables-example">
                                    <thead>
                                      <tr>
                                        <th>Equipo</th>
                                        <th>Contraloría</th>
                                        <th>DTA-CRM</th>
                                        <th>Total</th>
                                      </tr>
                                    </thead>
                                    <tbody>
                                      <tr class="odd gradeX" style="background-color: #205081; color: #FFF; ">
                                        <td class="text-center">TOTAL</td>
                                        <td class="text-center">0</td>
                                        <td class="text-center">0</td>
                                        <td class="text-center">0</td>
                                      </tr>
                                    </tbody>
                                  </table>
                                </div>
                              </div>
                            </div>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
                <div class="panel panel-default">
                  <div class="panel-heading borde-heading">
                    <div class="col-lg-4 col-md-4 col-xs-4 fondo">                       
                      <h4 class="panel-title text-center">
                        <i class="fa fa-line-chart fa-2x"></i>
                        <br/>
                        <br/>
                        <a data-toggle="collapse" data-parent="#accordion" href="#Col_INICIO_CALIDAD">Indicadores mes actual calidad </a>
                      </h4>
                    </div>
                    <div id="" class="col-lg-8 col-md-8 col-xs-8">
                      <div class="table-responsive">
                        <table class="table">
                          <thead>
                            <tr>
                              <th class="chico pull-left">% Isapre</th>
                              <th class="chico pull-left">% CPI</th>
                              <th class="chico pull-left">% Emp. Nvas.</th>
                            </tr>
                          </thead>
                          <tbody id="TotalCalidad" name="TotalCalidad">
                          </tbody>
                        </table>
                      </div>
                    </div>
                  </div>
                  <div id="Col_INICIO_CALIDAD" class="panel-collapse collapse">
                    <div class="panel-body">
                      <div role="form" id="DivFiltros_INICIO_CALIDAD">
                        <div id="" class="col-lg-8 col-md-8 col-xs-8">
                          <div class="form-group">
                            <label>Mes :</label>
                            <input type="text" id="fecha_calidad" name="fecha_calidad" alias="fecha_calidad">
                              <xsl:attribute name="value">
                                <xsl:value-of select="//app/fecha"/>
                              </xsl:attribute>
                            </input>
                          </div>
                        </div>
                        <div class="col-lg-1 col-md-1 col-xs-2 text-center">
                          <button type="submit" style="background-color: #205081; color: #FFF;" class="btn " onclick="Buscar(1, 'INICIO_CALIDAD');return false;">Consultar</button>
                        </div>
                        <div class="col-lg-1 col-md-1 col-xs-2 text-center">
                          <button type="reset" style="background-color: #205081; color: #FFF;" class="btn " onclick="LimpiarInicioCalidad();">Limpiar</button>
                        </div>
                      </div>
                      <div  id="DivResultado_INICIO_CALIDAD">
                        <div class="row">
                          <div class="col-lg-12 col-md-12 col-xs-12">
                            <div class="panel panel-default">
                              <div class="panel-body">
                                <div class="dataTable_wrapper">
                                  <table width="100%" class="table table-striped table-bordered table-hover" id="dataTables-example">
                                    <thead>
                                      <tr>
                                        <th>Equipo</th>
                                        <th>% Isapre</th>
                                        <th>% CPI</th>
                                        <th>% Emp Nvas.</th>
                                      </tr>
                                    </thead>
                                    <tbody>
                                      <tr class="odd gradeX" style="background-color: #205081; color: #FFF; ">
                                        <td class="text-center">TOTAL</td>
                                        <td class="text-center">0%</td>
                                        <td class="text-center">0%</td>
                                        <td class="text-center">0%</td>
                                      </tr>
                                    </tbody>
                                  </table>
                                </div>
                              </div>
                            </div>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
                <div class="panel panel-default">
                  <div class="panel-heading borde-heading">
                    <div class="col-lg-4 col-md-4  col-xs-4 fondo">
                       
                      <h4 class="panel-title text-center">
                        <i class="fa fa-bar-chart fa-2x"></i>
                        <br/>
                        <br/>
                        <a data-toggle="collapse" data-parent="#accordion" href="#Col_INICIO_CANTIDAD">Indicadores mes actual cantidad </a>
                      </h4>
                    </div>
                    <div id="" class="col-lg-8 col-md-8 col-xs-8">
                      <div class="table-responsive">
                        <table class="table">
                          <thead>
                            <tr>
                              <th class="chico pull-left">Tit. Prom</th>
                              <th class="chico pull-left">Cier Emp</th>
                              <th class="chico pull-left">Cier PPE</th>
                            </tr>
                          </thead>
                          <tbody  id="TotalCantidad" name="TotalCantidad">
                          </tbody>
                        </table>
                      </div>
                    </div>
                  </div>
                  <div id="Col_INICIO_CANTIDAD" class="panel-collapse collapse">
                    <div class="panel-body">
                      <div class="panel panel-default">
                        <div class="panel-body">
                          <div class="row">
                            <div class="col-lg-12 col-md-12 col-xs-12">
                              <div role="form" id="DivFiltros_INICIO_CANTIDAD">
                                <div class="col-lg-8 col-md-8 col-xs-8">
                                  <div class="form-group">
                                    <label>Mes :</label>
                                    <input type="text" id="fecha_cantidad" name="fecha_cantidad" alias="fecha_cantidad">
                                      <xsl:attribute name="value">
                                        <xsl:value-of select="//app/fecha"/>
                                      </xsl:attribute>
                                    </input>
                                  </div>
                                </div>
                                <div class="col-lg-1 col-md-1 col-xs-2 text-center">
                                  <button type="submit" style="background-color: #205081; color: #FFF;" class="btn " onclick="Buscar(1, 'INICIO_CANTIDAD');return false;"> Consultar</button>
                                </div>
                                <div class="col-lg-1 col-md-1 col-xs-2 text-center">
                                  <button type="reset" style="background-color: #205081; color: #FFF;" class="btn " onclick="LimpiarInicioCantidad();">Limpiar</button>
                                </div>
                              </div>
                            </div>
                          </div>
                        </div>
                      </div>
                      <br/>
                      <div  id="DivResultado_INICIO_CANTIDAD">
                        <div class="row">
                          <div class="col-lg-12 col-md-12 col-xs-12">
                            <div class="panel panel-default">
                              <div class="panel-body">
                                <div class="dataTable_wrapper">
                                  <table width="100%" class="table table-striped table-bordered table-hover" id="dataTables-example">
                                    <thead>
                                      <tr>
                                        <th>Equipo</th>
                                        <th>Titulares Promedio</th>
                                        <th>Cierre Empresas</th>
                                        <th>Cierre PPE</th>
                                      </tr>
                                    </thead>
                                    <tbody>
                                      <tr class="odd gradeX" style="background-color: #205081; color: #FFF; ">
                                        <td class="text-center">TOTAL</td>
                                        <td class="text-center">0</td>
                                        <td class="text-center">0</td>
                                        <td class="text-center">0</td>
                                      </tr>
                                    </tbody>
                                  </table>
                                </div>
                              </div>
                            </div>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
              <div id="DivAccion"></div>
              <div id="DivModal1"></div>
              <div id="DivModal2"></div>
            </div>
            <div id='ajax_loader' style="z-index: 1050; position: absolute; left: 40%; top: 30%; display: none;">
              <img src="../img/loader.gif"></img>
            </div>
            
          </body>
        </html>
      </xsl:when>
    </xsl:choose>
  </xsl:template>
</xsl:stylesheet>
