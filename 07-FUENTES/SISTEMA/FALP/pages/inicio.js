﻿jQuery_1_9(document).ready(function () {
    var origen = jQuery_1_9('#origen').val();
  
    CalendarioProduccion();
    CalendarioPendientes();
    CalendarioCalidad();
    CalendarioCantidad();
    
    switch (origen) {
        case '':
            IrAPag(1, 'INICIO_PRODUCCION');
            IrAPag(1, 'INICIO_PENDIENTES');
            IrAPag(1, 'INICIO_CALIDAD');
            IrAPag(1, 'INICIO_CANTIDAD');
            break;
        case '1':
            IrAPag(1, 'INICIO_PRODUCCION');
            break;
        case '2':
            IrAPag(1, 'INICIO_PENDIENTES');
            break;
        case '3':
            IrAPag(1, 'INICIO_CALIDAD');
            break;
        case '4':
            IrAPag(1, 'INICIO_CANTIDAD');
            break;
    };

    TOT_INICIO_PROD();
    TOT_INICIO_PEND();
    TOT_INICIO_CALI();
    TOT_INICIO_CANT();
});

function Buscar(Pag, Accion) {
    IrAPag(Pag, Accion);

    switch (Accion) {
        case 'INICIO_CALIDAD':
            TOT_INICIO_CALI();
            break;
        case 'INICIO_CANTIDAD':
            TOT_INICIO_CANT();
            break;
        case 'INICIO_PRODUCCION':
            TOT_INICIO_PROD();
            break;
        case 'INICIO_PENDIENTES':
            TOT_INICIO_PEND();
            break;
    };
};
function IrAPag(Pag, Accion) {
    mostrarLoad();
    jQuery_1_9('#PagNumber' + Accion).val(Pag);
    var perfil = jQuery_1_9('#h_perfil').val();

    var Div = jQuery_1_9('#DivResultado_' + Accion);
    var f = SerializaAlias('#DivFiltros_' + Accion + ',#DivResultado_' + Accion + ',#DivHidden');
    var paginaRequest = '';

    switch (Accion) {
        case 'INICIO_CALIDAD':
        case 'INICIO_CANTIDAD':
            paginaRequest = '../PagComun/GrillaPaginada/GrillaPaginadaB.aspx';//historico
            break;
        case 'INICIO_PRODUCCION':
        case 'INICIO_PENDIENTES':
            paginaRequest = '../PagComun/GrillaPaginada/GrillaPaginada.aspx';
            break;
        default:
            paginaRequest = '../PagComun/GrillaPaginada/GrillaPaginada.aspx';
    };

    //if (perfil === 'CONV_ROL_JEFE_COORDINADOR') {
    //    Accion = Accion.replace("INICIO", "DETALLE");
    //}

    Div.load(paginaRequest, { Accion: Accion, AliasForm: f }, function () {
        Div.show();
        //jQuery_1_9('#Col_' + Accion).collapse('show');
        quitarLoad();
    });
};
function ChangeRegPag(Obj, Accion) {
    jQuery_1_9('#PagNumber' + Accion).val('1');
    var perfil = jQuery_1_9('#h_perfil').val();
    var f = SerializaAlias('#DivFiltros_' + Accion + ',#DivResultado_' + Accion);
    var Div = jQuery_1_9('#DivResultado_' + Accion);
    var paginaRequest = '';

    switch (Accion) {
        case 'INICIO_CALIDAD':
        case 'INICIO_CANTIDAD':
            paginaRequest = '../PagComun/GrillaPaginada/GrillaPaginadaB.aspx';
            break;
        case 'INICIO_PRODUCCION':
        case 'INICIO_PENDIENTES':
            paginaRequest = '../PagComun/GrillaPaginada/GrillaPaginada.aspx';
            break;
        default:
            paginaRequest = '../PagComun/GrillaPaginada/GrillaPaginada.aspx';
    }

    //if (perfil === 'CONV_ROL_JEFE_COORDINADOR') {
    //    Accion = Accion.replace("INICIO", "DETALLE");
    //}

    mostrarLoad();

    Div.load(paginaRequest, { Accion: Accion, AliasForm: f }, function () {
        quitarLoad();
    });
};
function IrDetalleProduccion(supervisor, nombre) {
    var url = 'detalle.aspx';
    var fecha_desde = jQuery_1_9('#fechadesde_produccion').val();
    var fecha_hasta = jQuery_1_9('#fechahasta_produccion').val();

    var form = jQuery_1_9('<form action="' + url + '" method="post">' +
        '<input type="hidden" name="supervisor" id="supervisor" value="' + supervisor + '" />' +
        '<input type="hidden" name="nombre" id="nombre" value="' + nombre + '" />' +
        '<input type="hidden" name="fecha_desde" id="fecha_desde" value="' + fecha_desde + '" />' +
        '<input type="hidden" name="fecha_hasta" id="fecha_hasta" value="' + fecha_hasta + '" />' +
        '<input type="hidden" name="origen_llamada" id="origen_llamada" value="1" />' +
        '</form>');

    jQuery_1_9('body').append(form);
    form.submit();
};
function IrDetallePendientes(supervisor, nombre) {
    var url = 'detalle.aspx';
    var fecha_desde = jQuery_1_9('#fechadesde_pendientes').val();
    var fecha_hasta = jQuery_1_9('#fechahasta_pendientes').val();

    var form = jQuery_1_9('<form action="' + url + '" method="post">' +
        '<input type="hidden" name="supervisor" id="supervisor" value="' + supervisor + '" />' +
        '<input type="hidden" name="nombre" id="nombre" value="' + nombre + '" />' +
        '<input type="hidden" name="fecha_desde" id="fecha_desde" value="' + fecha_desde + '" />' +
        '<input type="hidden" name="fecha_hasta" id="fecha_hasta" value="' + fecha_hasta + '" />' +
        '<input type="hidden" name="origen_llamada" id="origen_llamada" value="2" />' +
        '</form>');

    jQuery_1_9('body').append(form);
    form.submit();
};
function IrDetalleCalidad(supervisor, nombre) {
    var url = 'detalle.aspx';
    var fecha_desde = jQuery_1_9('#fechadesde_produccion').val();
    var fecha_hasta = jQuery_1_9('#fechahasta_produccion').val();
    var fecha = jQuery_1_9('#fecha_calidad').val();

    var form = jQuery_1_9('<form action="' + url + '" method="post">' +
        '<input type="hidden" name="supervisor" id="supervisor" value="' + supervisor + '" />' +
        '<input type="hidden" name="nombre" id="nombre" value="' + nombre + '" />' +
        '<input type="hidden" name="fecha_desde" id="fecha_desde" value="' + fecha_desde + '" />' +
        '<input type="hidden" name="fecha_hasta" id="fecha_hasta" value="' + fecha_hasta + '" />' +
        '<input type="hidden" name="fecha" id="fecha" value="' + fecha + '" />' +
        '<input type="hidden" name="origen_llamada" id="origen_llamada" value="3" />' +
        '</form>');

    jQuery_1_9('body').append(form);
    form.submit();
};
function IrDetalleCantidad(supervisor, nombre) {
    var url = 'detalle.aspx';
    var fecha_desde = jQuery_1_9('#fechadesde_produccion').val();
    var fecha_hasta = jQuery_1_9('#fechahasta_produccion').val();
    var fecha = jQuery_1_9('#fecha_cantidad').val();

    var form = jQuery_1_9('<form action="' + url + '" method="post">' +
        '<input type="hidden" name="supervisor" id="supervisor" value="' + supervisor + '" />' +
        '<input type="hidden" name="nombre" id="nombre" value="' + nombre + '" />' +
        '<input type="hidden" name="fecha_desde" id="fecha_desde" value="' + fecha_desde + '" />' +
        '<input type="hidden" name="fecha_hasta" id="fecha_hasta" value="' + fecha_hasta + '" />' +
        '<input type="hidden" name="fecha" id="fecha" value="' + fecha + '" />' +
        '<input type="hidden" name="origen_llamada" id="origen_llamada" value="4" />' +
        '</form>');

    jQuery_1_9('body').append(form);
    form.submit();
};


function DetalleProduccionCM(coordinador) {

    mostrarLoad();

    jQuery_1_9('#h_coordinador').val(coordinador);
    var f = SerializaAlias('#DivFiltros_INICIO_PRODUCCION,#DivHidden');

    jQuery_1_9("#DivModal1").load("detalleAcciones.aspx", { Accion: 'PRODUCCION_CM', AliasForm: f }, function () {
        jQuery_1_9('#Modal_PRODUCCION_CM').modal('show');

        quitarLoad();

    });
};
function DetalleProduccionREG(coordinador) {

    mostrarLoad();

    jQuery_1_9('#h_coordinador').val(coordinador);
    var f = SerializaAlias('#DivFiltros_INICIO_PRODUCCION,#DivHidden');

    jQuery_1_9("#DivModal1").load("detalleAcciones.aspx", { Accion: 'PRODUCCION_REG', AliasForm: f }, function () {
        jQuery_1_9('#Modal_PRODUCCION_REG').modal('show');

        quitarLoad();

    });
};
function DetallePendienteCM(coordinador) {
    mostrarLoad();
    jQuery_1_9('#h_coordinador').val(coordinador);
    var f = SerializaAlias('#DivFiltros_INICIO_PENDIENTES,#DivHidden');

    jQuery_1_9("#DivModal1").load("detalleAcciones.aspx", { Accion: 'PENDIENTES_CM', AliasForm: f }, function () {
        jQuery_1_9('#Modal_PENDIENTES_CM').modal('show');
        quitarLoad();
    });
};
function DetallePendienteDTA(coordinador) {
    mostrarLoad();
    jQuery_1_9('#h_coordinador').val(coordinador);
    var f = SerializaAlias('#DivFiltros_INICIO_PENDIENTES,#DivHidden');

    jQuery_1_9("#DivModal1").load("detalleAcciones.aspx", { Accion: 'PENDIENTES_DTA', AliasForm: f }, function () {
        jQuery_1_9('#Modal_PENDIENTE_DTA').modal('show');
        quitarLoad();
    });
};

function IrALink(obj) {

    var obj_ = jQuery_1_9(obj)[0];
    var url = obj_.getAttribute('url');
    //var url = 'detalle.aspx';
    var tipo = obj_.getAttribute('tipo');
    var cod_coo = obj_.getAttribute('cod_coo');
    var cod_sup = obj_.getAttribute('cod_sup');

    var _form = '<form action="' + url + '" method="post" target="_blank">'

    if (typeof tipo !== typeof undefined && tipo !== null) {
        _form += '<input type="hidden" name="strTipo_User" id="strTipo_User" value="' + tipo + '" />'
    };

    if (typeof cod_coo !== typeof undefined && cod_coo !== null) {
        _form += '<input type="hidden" name="COD_CORD" id="COD_CORD" value="' + cod_coo + '" />'
    };

    if (typeof cod_sup !== typeof undefined && cod_sup !== null) {
        _form += '<input type="hidden" name="COD_SUPER" id="COD_SUPER" value="' + cod_sup + '" />'
    };

    _form += '</form>';

    var form = jQuery_1_9(_form);
    jQuery_1_9('body').append(form);
    form.submit();
};

function CalendarioProduccion() {
    var fecha_desde = jQuery_1_9('#fechadesde_produccion');
    var fecha_hasta = jQuery_1_9('#fechahasta_produccion');
    fecha_desde.datetimepicker({
        locale: 'es',
        format: 'L',
        //defaultDate: moment().subtract(90, 'd')
        defaultDate: moment().date(1)
    });
    fecha_hasta.datetimepicker({
        locale: 'es',
        format: 'L',
        defaultDate: moment().endOf('month')
    });

    fecha_desde.on("dp.change", function (e) {
        fecha_hasta.data("DateTimePicker").minDate(e.date);
    });
    fecha_hasta.on("dp.change", function (e) {
        fecha_desde.data("DateTimePicker").maxDate(e.date);
    });
};
function CalendarioPendientes() {
    var fecha_desde = jQuery_1_9('#fechadesde_pendientes');
    var fecha_hasta = jQuery_1_9('#fechahasta_pendientes');
    fecha_desde.datetimepicker({
        locale: 'es',
        format: 'L',
        //defaultDate: moment().subtract(90, 'd')
        defaultDate: moment().date(1)
    });
    fecha_hasta.datetimepicker({
        locale: 'es',
        format: 'L',
        defaultDate: moment().endOf('month')
    });
    fecha_desde.on("dp.change", function (e) {
        fecha_hasta.data("DateTimePicker").minDate(e.date);
    });
    fecha_hasta.on("dp.change", function (e) {
        fecha_desde.data("DateTimePicker").maxDate(e.date);
    });
};
function CalendarioCalidad() {
    var fecha = jQuery_1_9('#fecha_calidad');

    fecha.datetimepicker({
        locale: 'es',
        format: 'MM/YYYY',
        defaultDate: moment().toDate()
    });
};
function CalendarioCantidad() {
    var fecha = jQuery_1_9('#fecha_cantidad');

    fecha.datetimepicker({
        locale: 'es',
        format: 'MM/YYYY',
        defaultDate: moment().toDate()
    });
};
function LimpiarInicioProduccion() {
    var fecha_desde = jQuery_1_9('#fechadesde_produccion');
    var fecha_hasta = jQuery_1_9('#fechahasta_produccion');

    var fecha_actual = moment().date(1);
    var fecha = moment().endOf('month')._d;//moment().subtract(90, 'd');

    jQuery_1_9(fecha_hasta).data('DateTimePicker').date(fecha_actual);
    jQuery_1_9(fecha_desde).data('DateTimePicker').date(fecha);

    jQuery_1_9("#DivResultado_INICIO_PRODUCCION").html("<div class='row'><div class='col-lg-12 col-md-12 col-xs-12'><div class='panel panel-default'><div class='panel-body'><div class='dataTable_wrapper'><table width='100%' class='table table-striped table-bordered table-hover' id='dataTables-example'><thead><tr><th>Equipo</th><th>Enviados a Adm</th><th>Enviados a CM</th><th>Enviados a Registro</th><th>Aprobados</th><th>Listado</th></tr></thead><tbody><tr class='odd gradeX' style='background-color: #205081; color: #FFF; '><td class='text-center'>TOTAL</td><td class='text-center'>0</td><td class='text-center'>0</td><td class='text-center'>0</td><td class='text-center'>0</td><td class='text-center'></td></tr></tbody></table></div></div></div></div></div>");
};
function LimpiarInicioPendiente() {
    var fecha_desde = jQuery_1_9('#fechadesde_pendientes');
    var fecha_hasta = jQuery_1_9('#fechahasta_pendientes');

    var fecha_actual = moment().date(1);
    var fecha = moment().endOf('month');//moment().subtract(90, 'd');

    jQuery_1_9(fecha_hasta).data('DateTimePicker').date(fecha_actual);
    jQuery_1_9(fecha_desde).data('DateTimePicker').date(fecha);


    jQuery_1_9("#DivResultado_INICIO_PENDIENTES").html("<div class='row'><div class='col-lg-12 col-md-12 col-xs-12'><div class='panel panel-default'><div class='panel-body'><div class='dataTable_wrapper'><table width='100%' class='table table-striped table-bordered table-hover' id='dataTables-example'><thead><tr><th>Equipo</th><th>Contraloría</th><th>DTA-CRM</th><th>Total</th></tr></thead><tbody><tr class='odd gradeX' style='background-color: #205081; color: #FFF; '><td class='text-center'>TOTAL</td><td class='text-center'>0</td><td class='text-center'>0</td><td class='text-center'>0</td></tr></tbody></table></div></div></div></div></div>");
};
function LimpiarInicioCantidad() {
    var fecha = jQuery_1_9('#fecha_cantidad');

    var fecha_actual = moment().toDate();
    jQuery_1_9(fecha).data('DateTimePicker').date(fecha_actual);

    jQuery_1_9("#DivResultado_INICIO_CANTIDAD").html("<div class='row'><div class='col-lg-12 col-md-12 col-xs-12'><div class='panel panel-default'><div class='panel-body'><div class='dataTable_wrapper'><table width='100%' class='table table-striped table-bordered table-hover' id='dataTables-example'><thead><tr><th>Equipo</th><th>% Isapre</th><th>% CPI</th><th>% Empresas Nvas.</th></tr></thead><tbody><tr class='odd gradeX' style='background-color: #205081; color: #FFF; '><td class='text-center'>TOTAL</td><td class='text-center'>0%</td><td class='text-center'>0%</td><td class='text-center'>0%</td></tr></tbody></table></div></div></div></div></div>");
};
function LimpiarInicioCalidad() {
    var fecha = jQuery_1_9('#fecha_calidad');

    var fecha_actual = moment().toDate();
    jQuery_1_9(fecha).data('DateTimePicker').date(fecha_actual);

    jQuery_1_9("#DivResultado_INICIO_CALIDAD").html("<div class='row'><div class='col-lg-12 col-md-12 col-xs-12'><div class='panel panel-default'><div class='panel-body'><div class='dataTable_wrapper'><table width='100%' class='table table-striped table-bordered table-hover' id='dataTables-example'><thead><tr><th>Equipo</th><th>% Isapre</th><th>% CPI</th><th>% Empresas Nvas.</th></tr></thead><tbody><tr class='odd gradeX' style='background-color: #205081; color: #FFF; '><td class='text-center'>TOTAL</td><td class='text-center'>0%</td><td class='text-center'>0%</td><td class='text-center'>0%</td></tr></tbody></table></div></div></div></div></div>");
};
function mostrarLoad(id) {
    jQuery_1_9("#ajax_loader").show();
};
function quitarLoad(id) {
    jQuery_1_9("#ajax_loader").hide();
};
function TOT_INICIO_PROD() {
    var f = SerializaAlias('#DivFiltros_INICIO_PRODUCCION');
    jQuery_1_9("#TotalProduccion").load("detalleAcciones.aspx", { Accion: 'TOT_INICIO_PROD', AliasForm: f }, function () {
    });
};
function TOT_INICIO_PEND() {
    var f = SerializaAlias('#DivFiltros_INICIO_PENDIENTES');
    jQuery_1_9("#TotalPendientes").load("detalleAcciones.aspx", { Accion: 'TOT_INICIO_PEND', AliasForm: f }, function () {
    });
};
function TOT_INICIO_CALI() {
    var f = SerializaAlias('#DivFiltros_INICIO_CALIDAD');
    jQuery_1_9("#TotalCalidad").load("detalleAcciones.aspx", { Accion: 'TOT_INICIO_CALI', AliasForm: f }, function () {
    });
};
function TOT_INICIO_CANT() {
    var f = SerializaAlias('#DivFiltros_INICIO_CANTIDAD');
    jQuery_1_9("#TotalCantidad").load("detalleAcciones.aspx", { Accion: 'TOT_INICIO_CANT', AliasForm: f }, function () {
    });
};
function DescargaExcel(supervisor, nombreExcel) {
    mostrarLoad();
    jQuery_1_9('#h_supervisor').val(supervisor);
    var f = SerializaAlias('#DivFiltros_INICIO_PRODUCCION,#DivHidden');
    jQuery_1_9.post('../PagComun/Excel.aspx', { Accion: 'INICIO_PRODUCCION', AliasForm: f, AliasNombreArchivo: nombreExcel },
    function (RutaArchivo) {
        if (RutaArchivo) {
            var link = document.createElement("a");
            link.href = RutaArchivo;
            link.click();
            quitarLoad();
        }
    });
};
function ExcelDetalleProduccionCM(obj, coordinador, nombreExcel) {
    mostrarLoad();
    jQuery_1_9('#h_coordinador').val(coordinador);
    var f = SerializaAlias('#DivFiltros_DETALLE_PRODUCCION,#DivHidden');
    jQuery_1_9.post('../PagComun/Excel.aspx', { Accion: 'DETALLE_PRODUCCION_CM', AliasForm: f, AliasNombreArchivo: nombreExcel },
    function (RutaArchivo) {
        if (RutaArchivo) {
            var link = document.createElement("a");
            link.href = RutaArchivo;
            link.click();
            quitarLoad();
        }
    });
};
function ExcelDetalleProduccionREG(coordinador, nombreExcel) {
    mostrarLoad();
    jQuery_1_9('#h_coordinador').val(coordinador);
    var f = SerializaAlias('#DivFiltros_DETALLE_PRODUCCION,#DivHidden');
    jQuery_1_9.post('../PagComun/Excel.aspx', { Accion: 'DETALLE_PRODUCCION_REG', AliasForm: f, AliasNombreArchivo: nombreExcel },
    function (RutaArchivo) {
        if (RutaArchivo) {
            var link = document.createElement("a");
            link.href = RutaArchivo;
            link.click();
            quitarLoad();
        }
    });
};