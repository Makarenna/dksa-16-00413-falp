﻿jQuery_1_9(document).ready(function () {
    var origen = jQuery_1_9('#origen').val();
    switch (origen) {
        case '1':
            IrAPag(1, 'DETALLE_PRODUCCION');
            break;
        case '2':
            IrAPag(1, 'DETALLE_PENDIENTES');
            break;
        case '3':
            IrAPag(1, 'DETALLE_CALIDAD');
            break;
        case '4':
            IrAPag(1, 'DETALLE_CANTIDAD');
            break;
    };

    CalendarioProduccion();
    CalendarioPendientes();
    CalendarioCalidad();
    CalendarioCantidad();

    TOT_DETALLE_PROD();
    TOT_DETALLE_PEND();
    TOT_DETALLE_CALI();
    TOT_DETALLE_CANT();
});
function Buscar(Pag, Accion) {
    IrAPag(Pag, Accion);

    switch (Accion) {
        case 'DETALLE_PRODUCCION':
            TOT_DETALLE_PROD();
            break;
        case 'DETALLE_PENDIENTES':
            TOT_DETALLE_PEND();
            break;
        case 'DETALLE_CALIDAD':
            TOT_DETALLE_CALI();
            break;
        case 'DETALLE_CANTIDAD':
            TOT_DETALLE_CANT();
            break;
    }
};
function IrAPag(Pag, Accion) {
    mostrarLoad();
    jQuery_1_9('#PagNumber' + Accion).val(Pag);
    var Div = jQuery_1_9('#DivResultado_' + Accion);
    var f = SerializaAlias('#DivFiltros_' + Accion + ',#DivResultado_' + Accion + ',#DivHidden');

    var paginaRequest = '';

    switch (Accion) {
        case 'DETALLE_CALIDAD':
        case 'DETALLE_CANTIDAD':
            paginaRequest = '../PagComun/GrillaPaginada/GrillaPaginadaB.aspx';
            break;
        case 'DETALLE_PRODUCCION':
        case 'DETALLE_PENDIENTES':
            paginaRequest = '../PagComun/GrillaPaginada/GrillaPaginada.aspx';
            break;
        default:
            paginaRequest = '../PagComun/GrillaPaginada/GrillaPaginada.aspx';
    }
    Div.load(paginaRequest, { Accion: Accion, AliasForm: f }, function () {
        Div.show();
        jQuery_1_9('#Col_' + Accion).collapse('show');
        quitarLoad();
    });
};
function ChangeRegPag(Obj, Accion) {
    jQuery_1_9('#PagNumber' + Accion).val('1');
    var f = SerializaAlias('#DivFiltros_' + Accion + ',#DivResultado_' + Accion + ',#DivHidden');
    var Div = jQuery_1_9('#DivResultado_' + Accion);

    var paginaRequest = '';

    switch (Accion) {
        case 'DETALLE_CALIDAD':
        case 'DETALLE_CANTIDAD':
            paginaRequest = '../PagComun/GrillaPaginada/GrillaPaginadaB.aspx';
            break;
        case 'DETALLE_PRODUCCION':
        case 'DETALLE_PENDIENTES':
            paginaRequest = '../PagComun/GrillaPaginada/GrillaPaginada.aspx';
            break;
        default:
            paginaRequest = '../PagComun/GrillaPaginada/GrillaPaginada.aspx';
    };

    mostrarLoad();

    Div.load(paginaRequest, { Accion: Accion, AliasForm: f }, function () {
        quitarLoad();
    });
};
function volver() {

    var origen = jQuery_1_9('#origen').val();
    var fecha_desde = jQuery_1_9('#origen_fecha_desde').val();
    var fecha_hasta = jQuery_1_9('#origen_fecha_hasta').val();

    var url = 'inicio.aspx';
    var form = jQuery_1_9('<form action="' + url + '" method="post">' +
        '<input type="hidden" name="origen" id="origen" value="' + origen + '" />' +
        '<input type="hidden" name="fecha_desde" id="fecha_desde" value="' + fecha_desde + '" />' +
        '<input type="hidden" name="fecha_hasta" id="fecha_hasta" value="' + fecha_hasta + '" />' +
        '</form>');
    jQuery_1_9('body').append(form);
    form.submit();
};
function IrALink(url) {
    var win = window.open(url, '_blank');
    if (win) {
        //Browser has allowed it to be opened
        win.focus();
    } else {
        //Browser has blocked it
        alert('Please allow popups for this website');
    }
};

function IrAPaginaExterna(id) {

    var url = 'www.google.cl'
    var tipo = 'A';
    var _form = '<form action="' + url + '" method="post" target="_blank">'

    //if (typeof tipo !== typeof undefined && tipo !== null) {
        _form += '<input type="hidden" name="strTipo_User" id="strTipo_User" value="' + tipo + '" />'
    //};

    //if (typeof cod_coo !== typeof undefined && cod_coo !== null) {
    //    _form += '<input type="hidden" name="COD_CORD" id="COD_CORD" value="' + cod_coo + '" />'
    //};

    //if (typeof cod_sup !== typeof undefined && cod_sup !== null) {
    //    _form += '<input type="hidden" name="COD_SUPER" id="COD_SUPER" value="' + cod_sup + '" />'
    //};

    _form += '</form>';

    var form = jQuery_1_9(_form);
    jQuery_1_9('body').append(form);
    form.submit();
};

function ExcelDetalleProduccion(obj, coordinador, nombreExcel) {
    jQuery_1_9('#coordinador').val(coordinador);
    var f = SerializaAlias('#DivFiltros_DETALLE_PRODUCCION,#DivHidden');

    jQuery_1_9.post('../PagComun/Excel.aspx', { Accion: 'DETALLE_PRODUCCION', AliasForm: f, AliasNombreArchivo: nombreExcel },
    function (RutaArchivo) {
        if (RutaArchivo) {
            var link = document.createElement("a");
            link.href = RutaArchivo;
            link.click();
        }
    });
};
function mostrarLoad() {
    jQuery_1_9("#ajax_loader").show();
};
function quitarLoad() {
    jQuery_1_9("#ajax_loader").hide();
};
function ExcelDetalleProduccionCM(obj, coordinador, nombreExcel) {
    mostrarLoad();
    jQuery_1_9('#coordinador').val(coordinador);
    var f = SerializaAlias('#DivFiltros_DETALLE_PRODUCCION,#DivHidden');

    jQuery_1_9.post('../PagComun/Excel.aspx', { Accion: 'DETALLE_PRODUCCION_CM', AliasForm: f, AliasNombreArchivo: nombreExcel },
    function (RutaArchivo) {
        if (RutaArchivo) {
            var link = document.createElement("a");
            link.href = RutaArchivo;
            link.click();
            quitarLoad();
        }
    });
};
function ExcelDetalleProduccionREG(coordinador, nombreExcel) {
    mostrarLoad();
    jQuery_1_9('#coordinador').val(coordinador);
    var f = SerializaAlias('#DivFiltros_DETALLE_PRODUCCION,#DivHidden');

    jQuery_1_9.post('../PagComun/Excel.aspx', { Accion: 'DETALLE_PRODUCCION_REG', AliasForm: f, AliasNombreArchivo: nombreExcel },
    function (RutaArchivo) {
        if (RutaArchivo) {
            var link = document.createElement("a");
            link.href = RutaArchivo;
            link.click();
            quitarLoad();
        }
    });
};
function DetalleProduccionCM(coordinador) {
    mostrarLoad();
    jQuery_1_9('#coordinador').val(coordinador);
    var f = SerializaAlias('#DivFiltros_DETALLE_PRODUCCION,#DivHidden');

    jQuery_1_9("#DivModal1").load("detalleAcciones.aspx", { Accion: 'PRODUCCION_CM', AliasForm: f }, function () {
        jQuery_1_9('#Modal_PRODUCCION_CM').modal('show');
        quitarLoad();
    });
};
function DetalleProduccionREG(coordinador) {
    mostrarLoad();
    jQuery_1_9('#coordinador').val(coordinador);
    var f = SerializaAlias('#DivFiltros_DETALLE_PRODUCCION,#DivHidden');

    jQuery_1_9("#DivModal1").load("detalleAcciones.aspx", { Accion: 'PRODUCCION_REG', AliasForm: f }, function () {
        jQuery_1_9('#Modal_PRODUCCION_REG').modal('show');
        quitarLoad();
    });
};
function DetallePendienteCOO(coordinador) {
    mostrarLoad();
    jQuery_1_9('#coordinador').val(coordinador);
    var f = SerializaAlias('#DivFiltros_DETALLE_PENDIENTES,#DivHidden');

    jQuery_1_9("#DivModal1").load("detalleAcciones.aspx", { Accion: 'PENDIENTES_COO', AliasForm: f }, function () {
        jQuery_1_9('#Modal_PENDIENTES_COO').modal('show');
        quitarLoad();
    });
};
function DetallePendienteCM(coordinador) {
    mostrarLoad();
    jQuery_1_9('#coordinador').val(coordinador);
    var f = SerializaAlias('#DivFiltros_DETALLE_PENDIENTES,#DivHidden');

    jQuery_1_9("#DivModal1").load("detalleAcciones.aspx", { Accion: 'PENDIENTES_CM', AliasForm: f }, function () {
        jQuery_1_9('#Modal_PENDIENTES_CM').modal('show');
        quitarLoad();
    });
};
function DetallePendienteDTA(coordinador) {
    mostrarLoad();

    jQuery_1_9('#coordinador').val(coordinador);
    var f = SerializaAlias('#DivFiltros_DETALLE_PENDIENTES,#DivHidden');

    jQuery_1_9("#DivModal1").load("detalleAcciones.aspx", { Accion: 'PENDIENTES_DTA', AliasForm: f }, function () {
        jQuery_1_9('#Modal_PENDIENTE_DTA').modal('show');
        quitarLoad();
    });
};

function IndicadorCalidad(Pag) {
    jQuery_1_9('#PagNumberDETPROD_DETALLE').val(Pag);
    var f = SerializaAlias('#DivFiltrosDetalleProduccion,#DivDetalleProduccion');
    jQuery_1_9("#DivDetalleProduccionDetalle").load("../PagComun/GrillaPaginada/GrillaPaginada.aspx", { Accion: 'DETPROD_DETALLE', AliasForm: f }, function () {
        jQuery_1_9("#DivDetalleProduccionDetalle").show();
        jQuery_1_9("#collapseTwo").collapse('show');
    });
};
function IndicadorCantidad(Pag) {
    jQuery_1_9('#PagNumberDETPROD_DETALLE').val(Pag);
    var f = SerializaAlias('#DivFiltrosDetalleProduccion,#DivDetalleProduccion');
    jQuery_1_9("#DivDetalleProduccionDetalle").load("../PagComun/GrillaPaginada/GrillaPaginada.aspx", { Accion: 'DETPROD_DETALLE', AliasForm: f }, function () {
        jQuery_1_9("#DivDetalleProduccionDetalle").show();
    });
};

function Pendientes(Pag) {
    jQuery_1_9('#PagNumberDETPROD_DETALLE').val(Pag);
    var f = SerializaAlias('#DivFiltrosDetalleProduccion,#DivDetalleProduccion');
    jQuery_1_9("#DivDetalleProduccionDetalle").load("../PagComun/GrillaPaginada/GrillaPaginada.aspx", { Accion: 'DETPROD_DETALLE', AliasForm: f }, function () {
        jQuery_1_9("#DivDetalleProduccionDetalle").show();
    });
};
function limpiarDetalleProduccion() {
    var fecha_desde = jQuery_1_9('#fechadesde_produccion');
    var fecha_hasta = jQuery_1_9('#fechahasta_produccion');

    var fecha_actual = moment().date(1);
    var fecha = moment().toDate()//moment().subtract(90, 'd');

    jQuery_1_9(fecha_desde).data('DateTimePicker').date(fecha);
    jQuery_1_9(fecha_hasta).data('DateTimePicker').date(fecha_actual);

    jQuery_1_9("#DivResultado_DETALLE_PRODUCCION").html("<div class='row'><div class='col-lg-12'><div class='panel panel-default'><div class='panel-body'><div class='dataTable_wrapper'><table width='100%' class='table table-striped table-bordered table-hover' id='dataTables-example'><thead><tr><th>Coordinador</th><th>Enviados a Adm</th><th>Enviados a CM</th><th>Enviados a Registro</th><th>Aprobados</th><th>Listado</th></tr></thead><tbody><tr class='odd gradeX' style='background-color: #205081; color: #FFF; '><td class='text-center'>TOTAL</td><td class='text-center'>0</td><td class='text-center'>0</td><td class='text-center'>0</td><td class='text-center'>0</td><td class='text-center'></td></tr></tbody></table><div class='row'><div class='col-lg-12 text-center'><div class='col-sm-2 pull-left'></div><div class='col-sm-6 text-center'><button type='button' class='btn' style='background-color: #205081; color: #FFF;' onclick='javascript:volver();'><i class='fa fa-reply' aria-hidden='true'></i> Volver</button></div><div class='col-sm-4 btn-group pull-right'></div></div></div></div></div></div></div></div>");
};
function limpiarDetallePendiente() {
    var fecha_desde = jQuery_1_9('#fechadesde_pendientes');
    var fecha_hasta = jQuery_1_9('#fechahasta_pendientes');

    var fecha_actual = moment().date(1);
    var fecha = moment().toDate()//moment().subtract(90, 'd');

    jQuery_1_9(fecha_desde).data('DateTimePicker').date(fecha);
    jQuery_1_9(fecha_hasta).data('DateTimePicker').date(fecha_actual);

    jQuery_1_9("#DivResultado_DETALLE_PENDIENTES").html("<div class='row'><div class='col-lg-12'><div class='panel panel-default'><div class='panel-body'><div class='dataTable_wrapper'><table width='100%' class='table table-striped table-bordered table-hover' id='dataTables-example'><thead><tr><th>Coordinador</th><th>Contralía</th><th>DTA-CRM</th><th>Total</th></tr></thead><tbody><tr class='odd gradeX' style='background-color: #205081; color: #FFF; '><td class='text-center'>TOTAL</td><td class='text-center'>0</td><td class='text-center'>0</td><td class='text-center'>0</td></tr></tbody></table><div class='row'><div class='col-lg-12 text-center'><div class='col-sm-2 pull-left'></div><div class='col-sm-6 text-center'><button type='button' class='btn' style='background-color: #205081; color: #FFF;' onclick='javascript:volver();'><i class='fa fa-reply' aria-hidden='true'></i> Volver</button></div><div class='col-sm-4 btn-group pull-right'></div></div></div></div></div></div></div></div>");
};
function limpiarDetalleCalidad() {
    var fecha = jQuery_1_9('#fecha_calidad');

    var fecha_actual = moment().toDate();
    jQuery_1_9(fecha).data('DateTimePicker').date(fecha_actual);

    jQuery_1_9("#DivResultado_DETALLE_CALIDAD").html("<div class='row'><div class='col-lg-12'><div class='panel panel-default'><div class='panel-body'><div class='dataTable_wrapper'><table width='100%' class='table table-striped table-bordered table-hover' id='dataTables-example'><thead><tr><th>Coordinador</th><th>% Isapre</th><th>% CPI</th><th>% Empresas Nvas.</th></tr></thead><tbody><tr class='odd gradeX' style='background-color: #205081; color: #FFF; '><td class='text-center'>TOTAL</td><td class='text-center'>0 %</td><td class='text-center'>0 %</td><td class='text-center'>0 %</td></tr></tbody></table><div class='row'><div class='col-lg-12 text-center'><div class='col-sm-2 pull-left'></div><div class='col-sm-6 text-center'><button type='button' class='btn' style='background-color: #205081; color: #FFF;' onclick='javascript:volver();'><i class='fa fa-reply' aria-hidden='true'></i> Volver</button></div><div class='col-sm-4 btn-group pull-right'></div></div></div></div></div></div></div></div>");
};
function limpiarDetalleCantidad() {
    var fecha = jQuery_1_9('#fecha_cantidad');

    var fecha_actual = moment().toDate();
    jQuery_1_9(fecha).data('DateTimePicker').date(fecha_actual);

    jQuery_1_9("#DivResultado_DETALLE_CANTIDAD").html("<div class='row'><div class='col-lg-12'><div class='panel panel-default'><div class='panel-body'><div class='dataTable_wrapper'><table width='100%' class='table table-striped table-bordered table-hover' id='dataTables-example'><thead><tr><th>Coordinador</th><th>Titulares Promedio</th><th>Cierre Empresas</th><th>Cierre PPE</th></tr></thead><tbody><tr class='odd gradeX' style='background-color: #205081; color: #FFF; '><td class='text-center'>TOTAL</td><td class='text-center'>0</td><td class='text-center'>0</td><td class='text-center'>0</td></tr></tbody></table><div class='row'><div class='col-lg-12 text-center'><div class='col-sm-2 pull-left'></div><div class='col-sm-6 text-center'><button type='button' class='btn' style='background-color: #205081; color: #FFF;' onclick='javascript:volver();'><i class='fa fa-reply' aria-hidden='true'></i> Volver</button></div><div class='col-sm-4 btn-group pull-right'></div></div></div></div></div></div></div></div>");
};
function CalendarioProduccion() {
    var fecha_desde = jQuery_1_9('#fechadesde_produccion');
    var fecha_hasta = jQuery_1_9('#fechahasta_produccion');
    fecha_desde.datetimepicker({
        locale: 'es',
        format: 'L',
        defaultDate: moment().date(1)//moment().subtract(90, 'd')
    });
    fecha_hasta.datetimepicker({
        locale: 'es',
        format: 'L',
        defaultDate: moment().endOf('month')
    });

    fecha_desde.on("dp.change", function (e) {
        fecha_hasta.data("DateTimePicker").minDate(e.date);
    });
    fecha_hasta.on("dp.change", function (e) {
        fecha_desde.data("DateTimePicker").maxDate(e.date);
    });
};
function CalendarioPendientes() {
    var fecha_desde = jQuery_1_9('#fechadesde_pendientes');
    var fecha_hasta = jQuery_1_9('#fechahasta_pendientes');
    fecha_desde.datetimepicker({
        locale: 'es',
        format: 'L',
        defaultDate: moment().date(1)//moment().subtract(90, 'd')
    });
    fecha_hasta.datetimepicker({
        locale: 'es',
        format: 'L',
        defaultDate: moment().endOf('month')
    });
    fecha_desde.on("dp.change", function (e) {
        fecha_hasta.data("DateTimePicker").minDate(e.date);
    });
    fecha_hasta.on("dp.change", function (e) {
        fecha_desde.data("DateTimePicker").maxDate(e.date);
    });
};
function CalendarioCalidad() {
    var fecha = jQuery_1_9('#fecha_calidad');

    fecha.datetimepicker({
        locale: 'es',
        format: 'MM/YYYY',
        defaultDate: moment().toDate()
    });
};
function CalendarioCantidad() {
    var fecha = jQuery_1_9('#fecha_cantidad');

    fecha.datetimepicker({
        locale: 'es',
        format: 'MM/YYYY',
        defaultDate: moment().toDate()
    });
};
function TOT_DETALLE_PROD() {
    var f = SerializaAlias('#DivFiltros_DETALLE_PRODUCCION,#DivHidden');
    jQuery_1_9("#TotalProduccion").load("detalleAcciones.aspx", { Accion: 'TOT_DETALLE_PROD', AliasForm: f }, function () {
    });
};
function TOT_DETALLE_PEND() {
    var f = SerializaAlias('#DivFiltros_DETALLE_PENDIENTES,#DivHidden');
    jQuery_1_9("#TotalPendientes").load("detalleAcciones.aspx", { Accion: 'TOT_DETALLE_PEND', AliasForm: f }, function () {
    });
};
function TOT_DETALLE_CALI() {
    var f = SerializaAlias('#DivFiltros_DETALLE_CALIDAD,#DivHidden');
    jQuery_1_9("#TotalCalidad").load("detalleAcciones.aspx", { Accion: 'TOT_DETALLE_CALI', AliasForm: f }, function () {
    });
};
function TOT_DETALLE_CANT() {
    var f = SerializaAlias('#DivFiltros_DETALLE_CANTIDAD,#DivHidden');
    jQuery_1_9("#TotalCantidad").load("detalleAcciones.aspx", { Accion: 'TOT_DETALLE_CANT', AliasForm: f }, function () {
    });
};