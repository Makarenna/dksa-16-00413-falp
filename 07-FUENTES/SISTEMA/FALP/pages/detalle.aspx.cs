﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

using System.Xml;
using System.Xml.Xsl;
using System.IO;
using System.Data.SqlClient;

using library.xml;
using library.common;
using library.Security;


/* SIEMPRE DEBBO REMPLAZAR EL CODIGO DE LA CLASE */
public partial class detalle : PaginaBase
{
    protected void Page_Load(object sender, EventArgs e)
    {
        string supervisor = XmlHelper.strGetNodoTexto(XmlBase, "//requestFormPost/supervisor");
        string sup_nombre = XmlHelper.strGetNodoTexto(XmlBase, "//requestFormPost/nombre");
        string Origen = XmlHelper.strGetNodoTexto(XmlBase, "//requestFormPost/origen_llamada");

        string fecha_desde = XmlHelper.strGetNodoTexto(XmlBase, "//requestFormPost/fecha_desde");
        string fecha_hasta = XmlHelper.strGetNodoTexto(XmlBase, "//requestFormPost/fecha_hasta");
        string fecha = XmlHelper.strGetNodoTexto(XmlBase, "//requestFormPost/fecha");

        //if (!fecha_desde.Equals("") && fecha_desde.Length > 8)
        //{
        //    fecha = string.Concat(fecha.Split('/')[1], "/", fecha.Split('/')[2]);
        //}

        string origen = XmlHelper.strGetNodoTexto(XmlBase, "//requestFormPost/origen");
        string LinkContratos = cnfHelper.GetConfigAppVariable("LinkContratos");
        string LinkEmpresas = cnfHelper.GetConfigAppVariable("LinkEmpresas");
        string LinkPlantillas = cnfHelper.GetConfigAppVariable("LinkPlantillas");
        string LinkContraloria = cnfHelper.GetConfigAppVariable("LinkContraloria");

        XmlHelper.AgregaNodoConTexto(XmlBase, "//app", "Origen_llamada", Origen);
        
        XmlHelper.AgregaNodoConTexto(XmlBase, "//app", "supervisor", supervisor);
        XmlHelper.AgregaNodoConTexto(XmlBase, "//app", "sup_nombre", sup_nombre);
        
        XmlHelper.AgregaNodoConTexto(XmlBase, "//app", "fecha_desde", fecha_desde);
        XmlHelper.AgregaNodoConTexto(XmlBase, "//app", "fecha_hasta", fecha_hasta);
        XmlHelper.AgregaNodoConTexto(XmlBase, "//app", "fecha", fecha);

        XmlHelper.AgregaNodoConTexto(XmlBase, "//app", "LinkContratos", LinkContratos);
        XmlHelper.AgregaNodoConTexto(XmlBase, "//app", "LinkEmpresas", LinkEmpresas);
        XmlHelper.AgregaNodoConTexto(XmlBase, "//app", "LinkPlantillas", LinkPlantillas);
        XmlHelper.AgregaNodoConTexto(XmlBase, "//app", "LinkContraloria", LinkContraloria);

        XmlHelper.AgregaNodoConTexto(XmlBase, "//app", "Accion", "HTML");
        XmlHelper.appDibujarHTML(XmlBase, Request.MapPath("detalle.xsl"), this);
        XmlBase = null;
    }
}