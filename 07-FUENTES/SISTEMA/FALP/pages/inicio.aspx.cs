﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

using System.Xml;
using System.Xml.Xsl;
using System.IO;
using System.Data.SqlClient;

using library.xml;
using library.common;
using library.Security;

/* SIEMPRE DEBBO REMPLAZAR EL CODIGO DE LA CLASE */
public partial class inicio : PaginaBase
{
    protected void Page_Load(object sender, EventArgs e)
    {

        string origen = XmlHelper.strGetNodoTexto(XmlBase, "//requestFormPost/origen");
        string fecha_desde = XmlHelper.strGetNodoTexto(XmlBase, "//requestFormPost/fecha_desde");
        string fecha_hasta = XmlHelper.strGetNodoTexto(XmlBase, "//requestFormPost/fecha_hasta");
        string fecha = XmlHelper.strGetNodoTexto(XmlBase, "//requestFormPost/fecha");

        string LinkContratos = cnfHelper.GetConfigAppVariable("LinkContratos");
        string LinkEmpresas = cnfHelper.GetConfigAppVariable("LinkEmpresas");
        string LinkPlantillas = cnfHelper.GetConfigAppVariable("LinkPlantillas");

        XmlDocument XmlParametrosSql = null, XmlResultadoSql = null;
        XmlParametrosSql = XmlHelper.StpCrearParametros();
        XmlHelper.StpAgregarNodoParametros(XmlParametrosSql, "sesID", sesID);
        XmlHelper.StpAgregarNodoParametros(XmlParametrosSql, "usuID", usuID);
        XmlHelper.StpAgregarNodoParametros(XmlParametrosSql, "perID", perID);

        XmlHelper.AgregaNodoConTexto(XmlBase, "//app", "origen", origen);
        XmlHelper.AgregaNodoConTexto(XmlBase, "//app", "fecha_desde", fecha_desde);
        XmlHelper.AgregaNodoConTexto(XmlBase, "//app", "fecha_hasta", fecha_hasta);
        XmlHelper.AgregaNodoConTexto(XmlBase, "//app", "fecha", fecha);

        XmlHelper.AgregaNodoConTexto(XmlBase, "//app", "LinkContratos", LinkContratos);
        XmlHelper.AgregaNodoConTexto(XmlBase, "//app", "LinkEmpresas", LinkEmpresas);
        XmlHelper.AgregaNodoConTexto(XmlBase, "//app", "LinkPlantillas", LinkPlantillas);

        XmlHelper.AgregaNodoConTexto(XmlBase, "//app", "usuario_nombre", usuNombre);
        XmlHelper.AgregaNodoConTexto(XmlBase, "//app", "usuario_perfil", perID);
        XmlHelper.AgregaNodoConTexto(XmlBase, "//app", "Tipo", tipoID);
        XmlHelper.AgregaNodoConTexto(XmlBase, "//app", "Cod_coo", codCoo);
        XmlHelper.AgregaNodoConTexto(XmlBase, "//app", "Cod_sup", codSup);

        XmlHelper.AgregaNodoConTexto(XmlBase, "//app", "Accion", "HTML");
        XmlHelper.appDibujarHTML(XmlBase, Request.MapPath("inicio.xsl"), this);
        XmlBase = null;
    }
}