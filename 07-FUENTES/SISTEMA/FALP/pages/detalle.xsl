<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
  <xsl:output method="html" version="1.0" encoding="ISO-8859-1" indent="no" omit-xml-declaration="yes" standalone="yes" media-type="text/html" />
  <xsl:include href="../XslComun/Encabezado.xsl"/>
  <xsl:include href="../XslComun/JavaScript.xsl"/>
  <xsl:template match="/">
    <xsl:choose>
      <xsl:when test="//app/Accion = 'HTML'">
        <xsl:text disable-output-escaping="yes">&lt;!DOCTYPE html&gt;</xsl:text>
        <html>
          <head>
            <script src="http://ajax.googleapis.com/ajax/libs/jquery/1.10.2/jquery.min.js"></script>
            <script src="../bower_components/bootstrap/dist/js/bootstrap.min.js"></script>
            <script src="../js/moment-with-locales.js"></script>
            <script src="../js/bootstrap-datetimepicker.js"></script>
            
            <!--<xsl:call-template name="DibujaJavaScript" />-->
            <script type="text/javascript">
              var jQuery_1_9 = $.noConflict(true);
            </script>
            
            <script src="../dist/js/sb-admin-2.js"></script>            
            <script src="../js/Comunes.js"></script>            
            
            <!--<xsl:call-template name="DibujaJS_header" />-->            
            <xsl:call-template name="DibujaEncabezado" />
            <script src="detalle.js"></script>
          </head>
          <body class="" style="zoom: 0.9;">
            <div id="DivHidden">
              <input  type="hidden" value="" id="supervisor" name="supervisor" alias ="supervisor" >
                <xsl:attribute name="value">
                  <xsl:value-of select="//app/supervisor" />
                </xsl:attribute>
              </input>
              <input  type="hidden" value="" id="coordinador" name="coordinador" alias ="coordinador" >
                <xsl:attribute name="value">
                  <xsl:value-of select="//app/coordinador" />
                </xsl:attribute>
              </input>
              <input id="origen" name="origen" alias="origen" type="hidden" value="">
                <xsl:attribute name="value">
                  <xsl:value-of select="//app/Origen_llamada" />
                </xsl:attribute>
              </input>
              <input id="origen_fecha_desde" type="hidden" value="">
                <xsl:attribute name="value">
                  <xsl:value-of select="//app/fecha_desde" />
                </xsl:attribute>
              </input>
              <input id="origen_fecha_hasta" type="hidden" value="">
                <xsl:attribute name="value">
                  <xsl:value-of select="//app/fecha_hasta" />
                </xsl:attribute>
              </input>
            </div>
            <div id="page-wrapper" style="min-height: 626px;">
              <div class="row">
                <div class="col-lg-12 col-md-12 col-xs-12">
                  <h1 class="page-header"></h1>
                </div>
                <!-- /.col-lg-12 -->
              </div>
              <!-- /.row -->
              <div class="row">
                <div class="col-lg-4 col-md-4 col-xs-4">
                  <div class="panel panel-default">
                    <div class="panel-body">
                      <a class="btn btn-block btn-social btn-bitbucket">
                        <xsl:attribute name="onclick">
                          IrALink('<xsl:value-of select="//app/LinkContratos" />');
                        </xsl:attribute>
                        <i class="fa fa-suitcase"></i> Consulta Contrato
                      </a>
                    </div>
                  </div>
                </div>
                <div class="col-lg-4 col-md-4 col-xs-4">
                  <div class="panel panel-default">
                    <div class="panel-body">
                      <a class="btn btn-block btn-social btn-bitbucket" onclick="">
                        <xsl:attribute name="onclick">
                          IrALink('<xsl:value-of select="//app/LinkEmpresas"/>');
                        </xsl:attribute>
                        <i class="fa fa-institution"></i> Consulta Empresa
                      </a>
                    </div>
                  </div>
                </div>
                <div class="col-lg-4 col-md-4 col-xs-4">
                  <div class="panel panel-default">
                    <div class="panel-body">
                      <a class="btn btn-block btn-social btn-bitbucket">
                        <xsl:attribute name="onclick">
                          IrALink('<xsl:value-of select="//app/LinkPlantillas" />');
                        </xsl:attribute>
                        <i class="fa fa-file-o"></i> Consulta Planilla
                      </a>
                    </div>
                  </div>
                </div>
              </div>
              <div class="panel-group" id="accordion">
                <div class="panel panel-default">
                  <div class="panel-heading borde-heading">
                    <div class="col-lg-4 col-md-4 col-xs-4 fondo">
                      <h4 class="panel-title">
                        <i class="fa fa-users fa-2x"></i>
                        <br/>
                        <br/>
                        <br/>
                        <a data-toggle="collapse" data-parent="#accordion" href="#Col_DETALLE_PRODUCCION" aria-expanded="false" class="collapsed">
                           Producción | <xsl:value-of select="//app/sup_nombre"/>
                        </a>
                      </h4>
                    </div>
                    <div class="col-lg-8 col-md-8 col-xs-8">
                      <div class="table-responsive">
                        <table class="table">
                          <thead>
                            <tr>
                              <th>Env. a Admin</th>
                              <th>Env. a CM</th>
                              <th>Env. a registro</th>
                              <th>Aprobados</th>
                            </tr>
                          </thead>
                          <tbody id="TotalProduccion" name="TotalProduccion">
                          </tbody>
                        </table>
                      </div>
                    </div>
                  </div>

                  <div id="Col_DETALLE_PRODUCCION" class="collapse" aria-expanded="false" style="height: 0px;">
                    <div class="panel-body">
                      <div role="form" id="DivFiltros_DETALLE_PRODUCCION">
                        <div class="col-lg-2 col-md-2 col-xs-2 form-group">
                          <label style="margin-top: 10px;">Fecha Desde :</label>
                        </div>
                        <div class="col-lg-2 col-md-2 col-xs-2 form-group">
                          <input type="text" class="form-control" id="fechadesde_produccion" name="fechadesde_produccion" alias="fecha_desde">
                            <xsl:attribute name="value">
                              <xsl:value-of select="//app/fecha_desde"/>
                            </xsl:attribute>
                          </input>
                        </div>
                        <div class="col-lg-2 col-md-2 col-xs-2 form-group">
                          <label style="margin-top: 10px;">Fecha Hasta :</label>
                        </div>
                        <div class="col-lg-2 col-md-2 col-xs-2 form-group">
                          <input type="text" class="form-control" id="fechahasta_produccion" name="fechahasta_produccion" alias="fecha_hasta">
                            <xsl:attribute name="value">
                              <xsl:value-of select="//app/fecha_hasta"/>
                            </xsl:attribute>
                          </input>
                        </div>
                        <div class="col-lg-1 col-md-1 col-xs-2 text-center">
                          <button type="submit" style="background-color: #205081; color: #FFF;" class="btn " onclick="Buscar(1, 'DETALLE_PRODUCCION')">Consultar</button>
                        </div>
                        <div class="col-lg-1 col-md-1 col-xs-2 text-center">
                          <button type="reset" style="background-color: #205081; color: #FFF;" class="btn " onclick="limpiarDetalleProduccion();">Limpiar</button>
                        </div>
                      </div>
                      <div id="DivResultado_DETALLE_PRODUCCION">
                        <div class="row">
                          <div class="col-lg-12 col-md-12 col-xs-12">
                            <div class="panel panel-default">
                              <div class="panel-body">
                                <div class="dataTable_wrapper">
                                  <table width="100%" class="table table-striped table-bordered table-hover" id="dataTables-example">
                                    <thead>
                                      <tr>
                                        <th>Coordinador</th>
                                        <th>Enviados a Adm</th>
                                        <th>Enviados a CM</th>
                                        <th>Enviados a Registro</th>
                                        <th>Aprobados</th>
                                        <th>Listado</th>
                                      </tr>
                                    </thead>
                                    <tbody>
                                      <tr class="odd gradeX" style="background-color: #205081; color: #FFF; ">
                                        <td class="text-center">TOTAL</td>
                                        <td class="text-center">0</td>
                                        <td class="text-center">0</td>
                                        <td class="text-center">0</td>
                                        <td class="text-center">0</td>
                                        <td class="text-center"></td>
                                      </tr>
                                    </tbody>
                                  </table>
                                  <div class="row">
                                    <div class="col-lg-12 col-md-12 col-xs-12 text-center">
                                      <div class="col-sm-2 pull-left">
                                      </div>
                                      <div class="col-sm-6 text-center">
                                        <button type="button" class="btn" style="background-color: #205081; color: #FFF;" onclick="volver();">
                                          <i class="fa fa-reply" aria-hidden="true"></i> Volver
                                        </button>
                                      </div>
                                      <div class="col-sm-4 btn-group pull-right">
                                      </div>
                                    </div>
                                  </div>
                                </div>
                              </div>
                            </div>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
                <div class="panel panel-default">
                  <div class="panel-heading borde-heading">
                    <div class="col-lg-4 col-md-4 col-xs-4 fondo">
                      <h4 class="panel-title">
                        <i class="fa fa-tasks fa-2x"></i>
                        <br/>
                        <br/>
                        <a data-toggle="collapse" data-parent="#accordion" href="#Col_DETALLE_PENDIENTES" aria-expanded="false" class="collapsed">
                          Pendientes | <xsl:value-of select="//app/sup_nombre"/>
                        </a>
                      </h4>
                    </div>
                    <div class="col-lg-8 col-md-8 col-xs-8">
                      <div class="table-responsive">
                        <table class="table">
                          <thead>
                            <tr>
                              <th>Contraloria</th>
                              <th>DTA-CRM</th>
                              <th>Total</th>
                            </tr>
                          </thead>
                          <tbody id="TotalPendientes" name="TotalPendientes">
                          </tbody>
                        </table>
                      </div>
                    </div>
                  </div>
                  <div id="Col_DETALLE_PENDIENTES" class="panel-collapse collapse" aria-expanded="false" style="height: 0px;">
                    <div class="panel-body">
                      <div role="form">
                        <div class="col-lg-2 col-md-2 col-xs-2 form-group">
                          <label style="margin-top: 10px;">Fecha Desde :</label>
                        </div>
                        <div class="col-lg-2 col-md-2 col-xs-2 form-group">
                          <input type="text" class="form-control" id="fechadesde_pendientes" name="fechadesde_pendientes" alias="fecha_desde">
                            <xsl:attribute name="value">
                              <xsl:value-of select="//app/fecha_desde"/>
                            </xsl:attribute>
                          </input>
                        </div>

                        <div class="col-lg-2 col-md-2 col-xs-2 form-group">
                          <label style="margin-top: 10px;">Fecha Hasta :</label>
                        </div>
                        <div class="col-lg-2 col-md-2 col-xs-2 form-group">
                          <input type="text" class="form-control" id="fechahasta_pendientes" name="fechahasta_pendientes" alias="fecha_hasta">
                            <xsl:attribute name="value">
                              <xsl:value-of select="//app/fecha_hasta"/>
                            </xsl:attribute>
                          </input>
                        </div>
                        <div class="col-lg-1 col-md-1 col-xs-2 text-center">
                          <button type="submit" style="background-color: #205081; color: #FFF;" class="btn " onclick="Buscar(1, 'DETALLE_PENDIENTES')">Consultar</button>
                        </div>
                        <div class="col-lg-1 col-md-1 col-xs-2 text-center">
                          <button type="reset" style="background-color: #205081; color: #FFF;" class="btn " onclick="limpiarDetallePendiente();">Limpiar</button>
                        </div>
                      </div>
                      <div id="DivResultado_DETALLE_PENDIENTES">
                        <div class="row">
                          <div class="col-lg-12 col-md-12 col-xs-12">
                            <div class="panel panel-default">
                              <div class="panel-body">
                                <div class="dataTable_wrapper">
                                  <table width="100%" class="table table-striped table-bordered table-hover" id="dataTables-example">
                                    <thead>
                                      <tr>
                                        <th>Coordinador</th>
                                        <th>Contralía</th>
                                        <th>DTA-CRM</th>
                                        <th>Total</th>
                                      </tr>
                                    </thead>
                                    <tbody>
                                      <tr class="odd gradeX" style="background-color: #205081; color: #FFF; ">
                                        <td class="text-center">TOTAL</td>
                                        <td class="text-center">0</td>
                                        <td class="text-center">0</td>
                                        <td class="text-center">0</td>
                                      </tr>
                                    </tbody>
                                  </table>
                                  <div class="row">
                                    <div class="col-lg-12 col-md-12 col-xs-12 text-center">
                                      <div class="col-sm-2 pull-left">
                                      </div>
                                      <div class="col-sm-6 text-center">
                                        <button type="button" class="btn" style="background-color: #205081; color: #FFF;" onclick="volver();">
                                          <i class="fa fa-reply" aria-hidden="true"></i> Volver
                                        </button>
                                      </div>
                                      <div class="col-sm-4 btn-group pull-right">
                                      </div>
                                    </div>
                                  </div>
                                </div>
                              </div>
                            </div>
                          </div>
                        </div>
                      </div>
                      <div id="DivDETPROD_MODAL"></div>
                    </div>
                  </div>
                </div>
                <div class="panel panel-default">
                  <div class="panel-heading borde-heading">
                    <div class="col-lg-4 col-md-4 col-xs-4 fondo">
                      <h4 class="panel-title">
                        <i class="fa fa-line-chart fa-2x"></i>
                        <br/>
                        <br/>
                        <a data-toggle="collapse" data-parent="#accordion" href="#Col_DETALLE_CALIDAD">
                         Calidad | <xsl:value-of select="//app/sup_nombre"/>
                        </a>
                      </h4>
                    </div>
                    <div class="col-lg-8 col-md-8 col-xs-8">
                      <div class="table-responsive">
                        <table class="table">
                          <thead>
                            <tr>
                              <th>% Isapre</th>
                              <th>% CPI</th>
                              <th>Empresas Nvas.</th>
                            </tr>
                          </thead>
                          <tbody id="TotalCalidad" name="TotalCalidad">
                          </tbody>
                        </table>
                      </div>
                    </div>
                  </div>
                  <div id="Col_DETALLE_CALIDAD" class="panel-collapse collapse">
                    <div class="panel-body">
                      <div role="form">
                        <div class="col-lg-8 col-md-8 col-xs-8">
                          <div class="form-group" id="DivFiltros_DETALLE_CALIDAD">
                            <label>Mes :</label>
                            <input type="text" id="fecha_calidad" name="fecha_calidad" alias="fecha_calidad">
                              <xsl:attribute name="value">
                                <xsl:value-of select="//app/fecha"/>
                              </xsl:attribute>
                            </input>
                          </div>
                        </div>
                        <div class="col-lg-1 col-md-1 col-xs-2 text-center">
                          <button type="submit" style="background-color: #205081; color: #FFF;" class="btn " onclick="Buscar(1, 'DETALLE_CALIDAD')">Consultar</button>
                        </div>
                        <div class="col-lg-1 col-md-1 col-xs-2 text-center">
                          <button type="reset" style="background-color: #205081; color: #FFF;" class="btn " onclick="limpiarDetalleCalidad();">Limpiar</button>
                        </div>
                      </div>
                      <div id="DivResultado_DETALLE_CALIDAD">
                        <div class="row">
                          <div class="col-lg-12 col-md-12 col-xs-12">
                            <div class="panel panel-default">
                              <div class="panel-body">
                                <div class="dataTable_wrapper">
                                  <table width="100%" class="table table-striped table-bordered table-hover" id="dataTables-example">
                                    <thead>
                                      <tr>
                                        <th>Coordinador</th>
                                        <th>% Isapre</th>
                                        <th>% CPI</th>
                                        <th>% Empresas Nvas.</th>
                                      </tr>
                                    </thead>
                                    <tbody>
                                      <tr class="odd gradeX" style="background-color: #205081; color: #FFF; ">
                                        <td class="text-center">TOTAL</td>
                                        <td class="text-center">0 %</td>
                                        <td class="text-center">0 %</td>
                                        <td class="text-center">0 %</td>
                                      </tr>
                                    </tbody>
                                  </table>
                                  <div class="row">
                                    <div class="col-lg-12 col-md-12 col-xs-12 text-center">
                                      <div class="col-sm-2 pull-left">
                                      </div>
                                      <div class="col-sm-6 text-center">
                                        <button type="button" class="btn" style="background-color: #205081; color: #FFF;" onclick="javascript:volver();">
                                          <i class="fa fa-reply" aria-hidden="true"></i> Volver
                                        </button>
                                      </div>
                                      <div class="col-sm-4 btn-group pull-right">
                                      </div>
                                    </div>
                                  </div>
                                </div>
                              </div>
                            </div>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
                <div class="panel panel-default">
                  <div class="panel-heading borde-heading">
                    <div class="col-lg-4 col-md-4 col-xs-4 fondo">
                      <h4 class="panel-title">
                        <i class="fa fa-bar-chart fa-2x"></i>
                        <br/>
                        <br/>
                        <a data-toggle="collapse" data-parent="#accordion" href="#Col_DETALLE_CANTIDAD">
                          Cantidad | <xsl:value-of select="//app/sup_nombre"/>
                        </a>
                      </h4>
                    </div>
                    <div class="col-lg-8 col-md-8 col-xs-8">
                      <div class="table-responsive">
                        <table class="table">
                          <thead>
                            <tr>
                              <th>Titulares Promedio</th>
                              <th>Cierre Empresas</th>
                              <th>Cierre PPE</th>
                            </tr>
                          </thead>
                          <tbody id="TotalCantidad" name="TotalCantidad">
                          </tbody>
                        </table>
                      </div>
                    </div>
                  </div>
                  <div id="Col_DETALLE_CANTIDAD" class="panel-collapse collapse">
                    <div class="panel-body">
                      <div role="form" id="DivFiltros_DETALLE_CANTIDAD">
                        <div class="col-lg-8 col-md-8 col-xs-8">
                          <div class="form-group">
                            <label>Mes :</label>
                            <input type="text" id="fecha_cantidad" name="fecha_cantidad" alias="fecha_cantidad">
                              <xsl:attribute name="value">
                                <xsl:value-of select="//app/fecha"/>
                              </xsl:attribute>
                            </input>
                          </div>
                        </div>
                        <div class="col-lg-1 col-md-1 col-xs-2 text-center">
                          <button type="submit" style="background-color: #205081; color: #FFF;" class="btn " onclick="Buscar(1, 'DETALLE_CANTIDAD')">Consultar</button>
                        </div>
                        <div class="col-lg-1 col-md-1 col-xs-2 text-center">
                          <button type="reset" style="background-color: #205081; color: #FFF;" class="btn " onclick="limpiarDetalleCantidad();">Limpiar</button>
                        </div>
                      </div>
                      <div id="DivResultado_DETALLE_CANTIDAD">
                        <div class="row">
                          <div class="col-lg-12 col-md-12 col-xs-12">
                            <div class="panel panel-default">
                              <div class="panel-body">
                                <div class="dataTable_wrapper">
                                  <table width="100%" class="table table-striped table-bordered table-hover" id="dataTables-example">
                                    <thead>
                                      <tr>
                                        <th>Coordinador</th>
                                        <th>Titulares Promedio</th>
                                        <th>Cierre Empresas</th>
                                        <th>Cierre PPE</th>
                                      </tr>
                                    </thead>
                                    <tbody>
                                      <tr class="odd gradeX" style="background-color: #205081; color: #FFF; ">
                                        <td class="text-center">TOTAL</td>
                                        <td class="text-center">0</td>
                                        <td class="text-center">0</td>
                                        <td class="text-center">0</td>
                                      </tr>
                                    </tbody>
                                  </table>
                                  <div class="row">
                                    <div class="col-lg-12 col-md-12 col-xs-12 text-center">
                                      <div class="col-sm-2 pull-left">
                                      </div>
                                      <div class="col-sm-6 text-center">
                                        <button type="button" class="btn" style="background-color: #205081; color: #FFF;" onclick="volver();">
                                          <i class="fa fa-reply" aria-hidden="true"></i> Volver
                                        </button>
                                      </div>
                                      <div class="col-sm-4 btn-group pull-right">
                                      </div>
                                    </div>
                                  </div>
                                </div>
                              </div>
                            </div>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
              <div id="DivModal1"></div>
              <div id="DivModal2"></div>
            </div>
            <div id='ajax_loader' style="position: fixed; left: 40%; top: 30%; display: none;">
              <img src="../img/loader.gif"></img>
            </div>
            <!--<xsl:call-template name="DibujaJavaScript" />-->
          </body>
        </html>
      </xsl:when>
      <xsl:when test="//app/Accion = 'PRODUCCION_CM'">
        <div id="Modal_PRODUCCION_CM" class="modal fade in" role="dialog" style="display: block; padding-left: 10px;">
          <div class="modal-dialog" style="width: 900px;">
            <div class="modal-content">
              <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">×</button>
                <h4 class="modal-title">
                  Contraloría Médica | Equipo <xsl:value-of select="//app/PRODUCCION_CM/Datos/row/@GRUPO"/>
                </h4>
              </div>
              <div class="modal-body">
                <div class="row">
                  <div class="col-lg-12 col-md-12 col-xs-12">
                    <div class="panel panel-default">
                      <div class="panel-heading" style="padding:10px 15px">
                        Detalle Enviados a Contraloría Médica | Equipo <xsl:value-of select="//app/PRODUCCION_CM/Datos/row/@GRUPO"/>
                      </div>
                      <div class="panel-body">
                        <div class="dataTable_wrapper">
                          <table width="100%" class="table table-striped table-bordered table-hover" id="dataTables-example">
                            <thead>
                              <tr>
                                <th>COORDINADOR</th>
                                <th>SIN RESO</th>
                                <th>PEN</th>
                                <th>APRO</th>
                                <th>RECH</th>
                                <th>TOTAL</th>
                                <th>LISTADO</th>
                              </tr>
                            </thead>
                            <tbody>
                              <xsl:for-each select="//app/PRODUCCION_CM/Datos/row">
                                <tr class="odd gradeX">
                                  <td class="text-center">
                                    <xsl:value-of select="@NOMBRE_COOR"/>
                                  </td>
                                  <td class="text-center">
                                    <xsl:value-of select="@SINRESO"/>
                                  </td>
                                  <td class="text-center">
                                    <xsl:value-of select="@PEN"/>
                                  </td>
                                  <td class="text-center">
                                    <xsl:value-of select="@APRO"/>
                                  </td>
                                  <td class="text-center">
                                    <xsl:value-of select="@RECH"/>
                                  </td>
                                  <td class="text-center">
                                    <xsl:value-of select="@SINRESO + @PEN + @APRO + @RECH"/>
                                  </td>
                                  <td class="text-center">
                                    <a>
                                      <xsl:attribute name="onclick">
                                        ExcelDetalleProduccionCM(this,'<xsl:value-of select="@COOR" />','<xsl:value-of select="@NOMBREEXCEL" />');
                                      </xsl:attribute>
                                      <p class="fa fa-download"></p>
                                    </a>
                                  </td>
                                </tr>
                              </xsl:for-each>
                              <xsl:for-each select="//app/PRODUCCION_CM/Datos/row1">
                                <tr class="odd gradeX" style="background-color: #205081; color: #FFF;">
                                  <td class="text-center">TOTAL</td>
                                  <td class="text-center">
                                    <xsl:value-of select="@T_SINRESO"/>
                                  </td>
                                  <td class="text-center">
                                    <xsl:value-of select="@T_PEN"/>
                                  </td>
                                  <td class="text-center">
                                    <xsl:value-of select="@T_APRO"/>
                                  </td>
                                  <td class="text-center">
                                    <xsl:value-of select="@T_RECH"/>
                                  </td>
                                  <td class="text-center">
                                    <xsl:value-of select="@T_SINRESO + @T_PEN + @T_APRO + @T_RECH"/>
                                  </td>
                                  <td class="text-center">
                                    <a href="listado.xlsx" target="blank"></a>
                                  </td>
                                </tr>
                              </xsl:for-each>
                            </tbody>
                          </table>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
              <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
              </div>
            </div>
          </div>
        </div>
      </xsl:when>
      <xsl:when test="//app/Accion = 'PRODUCCION_REG'">
        <div id="Modal_PRODUCCION_REG" class="modal fade in" role="dialog" style="display: block; padding-left: 19px;">
          <div class="modal-dialog" style="width: 900px;">
            <div class="modal-content">
              <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">×</button>
                <h4 class="modal-title">
                  Registro | Equipo <xsl:value-of select="//app/PRODUCCION_REG/Datos/row/@GRUPO"/>
                </h4>
              </div>
              <div class="modal-body">
                <div class="row">
                  <div class="col-lg-12 col-md-12 col-xs-12">
                    <div class="panel panel-default">
                      <div class="panel-heading" style="padding:10px 15px">
                        Detalle Enviados a Registro | Equipo <xsl:value-of select="//app/PRODUCCION_REG/Datos/row/@GRUPO"/>
                      </div>
                      <div class="panel-body">
                        <div class="dataTable_wrapper">
                          <table width="100%" class="table table-striped table-bordered table-hover" id="dataTables-example">
                            <thead>
                              <tr>
                                <th>COORDINADOR</th>
                                <th>NORECEP</th>
                                <th>PEN</th>
                                <th>APRO</th>
                                <th>RECH</th>
                                <th>TOTAL</th>
                                <th>LISTADO</th>
                              </tr>
                            </thead>
                            <tbody>
                              <xsl:for-each select="//app/PRODUCCION_REG/Datos/row">
                                <tr class="odd gradeX">
                                  <td class="text-center">
                                      <xsl:value-of select="@NOMBRE_COOR"/>
                                  </td>
                                  <td class="text-center">
                                    <xsl:value-of select="@NORECEP"/>
                                  </td>
                                  <td class="text-center">
                                    <xsl:value-of select="@PEN"/>
                                  </td>
                                  <td class="text-center">
                                    <xsl:value-of select="@APRO"/>
                                  </td>
                                  <td class="text-center">
                                    <xsl:value-of select="@RECH"/>
                                  </td>
                                  <td class="text-center">
                                    <xsl:value-of select="@NORECEP + @PEN +@APRO + @RECH"/>
                                  </td>
                                  <td class="text-center">
                                    <a>
                                      <xsl:attribute name="onclick">
                                        ExcelDetalleProduccionREG(this,'<xsl:value-of select="@COOR" />','<xsl:value-of select="@NOMBREEXCEL" />');
                                      </xsl:attribute>
                                      <p class="fa fa-download"></p>
                                    </a>
                                  </td>
                                </tr>
                              </xsl:for-each>
                              <xsl:for-each select="//app/PRODUCCION_REG/Datos/row1">
                                <tr class="odd gradeX" style="background-color: #205081; color: #FFF;">
                                  <td class="text-center">TOTAL</td>
                                  <td class="text-center">
                                    <xsl:value-of select="@T_NORECEP"/>
                                  </td>
                                  <td class="text-center">
                                    <xsl:value-of select="@T_PEN"/>
                                  </td>
                                  <td class="text-center">
                                    <xsl:value-of select="@T_APRO"/>
                                  </td>
                                  <td class="text-center">
                                    <xsl:value-of select="@T_RECH"/>
                                  </td>
                                  <td class="text-center">
                                    <xsl:value-of select="@T_PEN +@T_PEN + @T_APRO + @T_RECH"/>
                                  </td>
                                  <td class="text-center"></td>
                                </tr>
                              </xsl:for-each>
                            </tbody>
                          </table>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
              <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
              </div>
            </div>
          </div>
        </div>
      </xsl:when>
      
      <xsl:when test="//app/Accion = 'PENDIENTES_COO'">
        <div id="Modal_PENDIENTES_COO" class="modal fade in" role="dialog" style="display: block; padding-left: 19px;">
          <div class="modal-dialog" style="width: 900px;">
            <div class="modal-content">
              <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">×</button>
                <h4 class="modal-title">
                  Pendientes CM | Equipo <xsl:value-of select="//app/PENDIENTES_COO/Datos/row/@GRUPO"/>
                </h4>
              </div>
              <div class="modal-body">
                <div class="row">
                  <div class="col-lg-12 col-md-12 col-xs-12">
                    <div class="panel panel-default">
                      <div class="panel-heading" style="padding:10px 15px">
                        Detalle Pendientes Contraloría Médica | Equipo <xsl:value-of select="//app/PENDIENTES_COO/Datos/row/@GRUPO"/>
                      </div>
                      <div class="panel-body">
                        <div class="dataTable_wrapper">
                          <table width="100%" class="table table-striped table-bordered table-hover" id="dataTables-example">
                            <thead>
                              <tr>
                                <th>Folio</th>
                                <th>Resuelve</th>
                                <th>Fecha</th>
                                <th>Rut</th>
                                <th>Nombre Titular</th>
                                <th>Nro. DS</th>
                                <th>Restan</th>
                              </tr>
                            </thead>
                            <tbody>
                              <xsl:for-each select="//app/PENDIENTES_COO/Datos/row">
                                <tr class="odd gradeX">
                                  <td class="text-center">
                                    <xsl:value-of select="@FOLIO"/>
                                  </td>
                                  <td class="text-center">
                                    <xsl:value-of select="@RESUELVE"/>
                                  </td>
                                  <td class="text-center">
                                    <xsl:value-of select="@FECHA"/>
                                  </td>
                                  <td class="text-center">
                                    <xsl:value-of select="@RUT"/>
                                  </td>
                                  <td class="text-center">
                                    <xsl:value-of select="@TITULAR"/>
                                  </td>
                                  <td class="text-center">
                                    <xsl:value-of select="@DS"/>
                                  </td>
                                  <td class="text-center">
                                    <xsl:value-of select="@RESTAN"/>
                                  </td>
                                </tr>
                              </xsl:for-each>
                            </tbody>
                          </table>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
              <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
              </div>
            </div>
          </div>
        </div>
      </xsl:when>      
      <xsl:when test="//app/Accion ='PENDIENTES_CM'">
        <div id="Modal_PENDIENTES_CM" class="modal fade in" role="dialog" style="display: block; padding-right: 17px;">
          <div class="modal-dialog" style="width: 900px;">
            <div class="modal-content">
              <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">×</button>
                <h4 class="modal-title">
                  Pendientes CM | Equipo <xsl:value-of select="//app/PENDIENTES_CM/Datos/row/@GRUPO"/>
                </h4>
              </div>
              <div class="modal-body">
                <div class="row">
                  <div class="col-lg-12 col-md-12 col-xs-12">
                    <div class="panel panel-default">
                      <div class="panel-heading" style="padding:10px 15px">
                        Detalle Pendientes Contraloría Médica | Equipo <xsl:value-of select="//app/PENDIENTES_CM/Datos/row/@GRUPO"/>
                      </div>
                      <div class="panel-body">
                        <div class="dataTable_wrapper">
                          <table width="100%" class="table table-striped table-bordered table-hover" id="dataTables-example">
                            <thead>
                              <tr>
                                <th>Folio</th>
                                <th>Resuelve</th>
                                <th>Fecha</th>
                                <th>Rut</th>
                                <th>Nombre Titular</th>
                                <th>Nro. DS</th>
                                <th>Restan</th>
                              </tr>
                            </thead>
                            <tbody>
                              <xsl:for-each select="//app/PENDIENTES_CM/Datos/row">
                                 <tr class="odd gradeX">
                                  <td class="text-center">
                                    <a>
                                     <xsl:attribute name="onclick">
                                        IrAPaginaExterna('<xsl:value-of select="@FOLIO" />');
                                      </xsl:attribute>
                                     <xsl:value-of select="@FOLIO"/>
                                    </a>
                                  </td>
                                  <td class="text-center">
                                    <xsl:value-of select="@RESUELVE"/>
                                  </td>
                                  <td class="text-center">
                                    <xsl:value-of select="@FECHA"/>
                                  </td>
                                  <td class="text-center">
                                    <xsl:value-of select="@RUT"/>
                                  </td>
                                  <td class="text-center">
                                    <xsl:value-of select="@TITULAR"/>
                                  </td>
                                  <td class="text-center">
                                    <xsl:value-of select="@DS"/>
                                  </td>
                                  <td class="text-center">
                                    <xsl:value-of select="@RESTAN"/>
                                  </td>
                                </tr>
                              </xsl:for-each>
                            </tbody>
                          </table>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
              <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
              </div>
            </div>
          </div>
        </div>
      </xsl:when>
      <xsl:when test="//app/Accion ='PENDIENTES_DTA'">
        <div id="Modal_PENDIENTE_DTA" class="modal fade in" role="dialog" style="display: block;">
          <div class="modal-dialog" style="width: 900px;">
            <div class="modal-content">
              <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">×</button>
                <h4 class="modal-title">
                  Listado de Requerimientos Asignados | Equipo <xsl:value-of select="//app/PRODUCCION_REG/Datos/row/@GRUPO"/>
                </h4>
              </div>
              <div class="modal-body">
                <div class="row">
                  <div class="col-lg-12 col-md-12 col-xs-12">
                    <div class="panel panel-default">
                      <div class="panel-heading" style="padding:10px 15px">
                        Detalle Pendientes Contraloría Médica | Equipo <xsl:value-of select="//app/PRODUCCION_REG/Datos/row/@GRUPO"/>
                      </div>
                      <div class="panel-body">
                        <div class="dataTable_wrapper">
                          <table width="100%" class="table table-striped table-bordered table-hover" id="dataTables-example">
                            <thead>
                              <tr>
                                <th>Nro.</th>
                                <th>Nro. por caja</th>
                                <th>Tipo de Requerimiento</th>
                                <th>Responsable</th>
                                <th>Responsable</th>
                                <th>Días </th>
                                <th>Días Req.</th>
                                <th>Folio</th>
                                <th>Afiliado</th>
                                <th>Fec. Apertura</th>
                                <th>Fec. Cierre</th>
                                <th>Hospitalización</th>
                              </tr>
                            </thead>
                            <tbody>
                              <xsl:for-each select="//app/PENDIENTES_DTA/Datos/row">
                                <tr class="odd gradeX">
                                  <td class="text-center"><xsl:value-of select="@NUM"/></td>
                                  <td class="text-center"><xsl:value-of select="@CAJA"/></td>
                                  <td class="text-center"><xsl:value-of select="@REQ"/></td>
                                  <td class="text-center"><xsl:value-of select="@RESP"/></td>
                                  <td class="text-center"><xsl:value-of select="@RESP"/></td>
                                  <td class="text-center"><xsl:value-of select="@DIAS"/></td>
                                  <td class="text-center"><xsl:value-of select="@DIASREQ"/></td>
                                  <td class="text-center"><xsl:value-of select="@FOLIO"/></td>
                                  <td class="text-center"><xsl:value-of select="@AFILIADO"/></td>
                                  <td class="text-center"><xsl:value-of select="@FAPERTURA"/></td>
                                  <td class="text-center"><xsl:value-of select="@FCIERRE"/></td>
                                  <td class="text-center"><xsl:value-of select="@HOSP"/></td>
                                </tr>
                              </xsl:for-each>
                            </tbody>
                          </table>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
              <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
              </div>
            </div>
          </div>
        </div>
      </xsl:when>
      
      <xsl:when test="//app/Accion ='TOT_INICIO_PROD'">
        <tr>
          <td class="chico pull-left">
            <span class="label label-default label-success">
              <xsl:value-of select="//app/Tproduccion/Datos/row/@ADMIN"/>
            </span>
          </td>
          <td class="chico pull-left">
            <span class="label label-default label-success">
              <xsl:value-of select="//app/Tproduccion/Datos/row/@CM"/>
            </span>
          </td>
          <td class="chico pull-left">
            <span class="label label-default label-success">
              <xsl:value-of select="//app/Tproduccion/Datos/row/@REGISTRO"/>
            </span>
          </td>
          <td class="chico pull-left">
            <span class="label label-default label-success">
              <xsl:value-of select="//app/Tproduccion/Datos/row/@APROBADOS"/>
            </span>
          </td>
        </tr>
      </xsl:when>
      <xsl:when test="//app/Accion = 'TOT_INICIO_PEND'">
        <tr>
          <td class="chico pull-left">
            <span class="label label-default label-success">
              <xsl:value-of select="//app/TPendientes/Datos/row/@CONTRALORIA"/>
            </span>
          </td>
          <td class="chico pull-left">
            <span class="label label-default label-success">
              <xsl:value-of select="//app/TPendientes/Datos/row/@DTACRM"/>
            </span>
          </td>
          <td class="chico pull-left">
            <span class="label label-default label-success">
              <xsl:value-of select="//app/TPendientes/Datos/row/@TOTAL"/>
            </span>
          </td>
        </tr>
      </xsl:when>      
      <xsl:when test="//app/Accion = 'TOT_INICIO_CALI'">
        <tr>
          <td class="chico pull-left">
            <span class="label label-default label-success">
              <xsl:value-of select="//app/TCalidad/Datos/row/@ISAPRE"/>%
            </span>
          </td>
          <td class="chico pull-left">
            <span class="label label-default label-warning">
              <xsl:value-of select="//app/TCalidad/Datos/row/@CPI"/>%
            </span>
          </td>
          <td class="chico pull-left">
            <span class="label label-default label-danger">
              <xsl:value-of select="//app/TCalidad/Datos/row/@EMP"/>%
            </span>
          </td>
        </tr>
      </xsl:when>      
      <xsl:when test="//app/Accion = 'TOT_INICIO_CANT'">
        <tr>
          <td class="chico pull-left">
            <span class="label label-default label-success">
              <xsl:value-of select="//app/TCantidad/Datos/row/@PROMEDIO"/>
            </span>
          </td>
          <td class="chico pull-left">
            <span class="label label-default label-success">
              <xsl:value-of select="//app/TCantidad/Datos/row/@EMPRESAS"/>
            </span>
          </td>
          <td class="chico pull-left">
            <span class="label label-default label-success">
              <xsl:value-of select="//app/TCantidad/Datos/row/@PPE"/>
            </span>
          </td>
        </tr>
      </xsl:when>
      <xsl:when test="//app/Accion = 'TOT_DETALLE_PROD'">
        <tr>
          <td>
            <span class="label label-default label-success">
              <xsl:value-of select="//app/Tproduccion/Datos/row/@ADMIN"/>
            </span>
          </td>
          <td >
            <span class="label label-default label-success">
              <xsl:value-of select="//app/Tproduccion/Datos/row/@CM"/>
            </span>
          </td>
          <td >
            <span class="label label-default label-success">
              <xsl:value-of select="//app/Tproduccion/Datos/row/@REGISTRO"/>
            </span>
          </td>
          <td >
            <span class="label label-default label-success">
              <xsl:value-of select="//app/Tproduccion/Datos/row/@APROBADOS"/>
            </span>
          </td>
        </tr>
      </xsl:when>
      <xsl:when test="//app/Accion = 'TOT_DETALLE_PEND'">
        <tr>
          <td >
            <span class="label label-default label-success">
              <xsl:value-of select="//app/TPendientes/Datos/row/@CONTRALORIA"/>
            </span>
          </td>
          <td >
            <span class="label label-default label-success">
              <xsl:value-of select="//app/TPendientes/Datos/row/@DTACRM"/>
            </span>
          </td>
          <td >
            <span class="label label-default label-success">
              <xsl:value-of select="//app/TPendientes/Datos/row/@TOTAL"/>
            </span>
          </td>
        </tr>
      </xsl:when>
      <xsl:when test="//app/Accion = 'TOT_DETALLE_CALI'">
        <tr>
          <td >
            <span class="label label-default label-success">
              <xsl:value-of select="//app/TCalidad/Datos/row/@ISAPRE"/>%
            </span>
          </td>
          <td >
            <span class="label label-default label-warning">
              <xsl:value-of select="//app/TCalidad/Datos/row/@CPI"/>%
            </span>
          </td>
          <td >
            <span class="label label-default label-danger">
              <xsl:value-of select="//app/TCalidad/Datos/row/@EMP"/>%
            </span>
          </td>
        </tr>
      </xsl:when>
      <xsl:when test="//app/Accion = 'TOT_DETALLE_CANT'">
        <tr>
          <td >
            <span class="label label-default label-success">
              <xsl:value-of select="//app/TCantidad/Datos/row/@PROMEDIO"/>
            </span>
          </td>
          <td >
            <span class="label label-default label-success">
              <xsl:value-of select="//app/TCantidad/Datos/row/@EMPRESAS"/>
            </span>
          </td>
          <td >
            <span class="label label-default label-success">
              <xsl:value-of select="//app/TCantidad/Datos/row/@PPE"/>
            </span>
          </td>
        </tr>
      </xsl:when>
    </xsl:choose>
  </xsl:template>
</xsl:stylesheet>
