﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;



using System.Xml;
using System.Xml.Xsl;
using System.IO;
using System.Data.SqlClient;

using library.xml;
using library.common;
using library.Security;

public partial class detalleAcciones : PaginaBase
{
    protected void Page_Load(object sender, EventArgs e)
    {
        XmlDocument XmlFinal = null, XmlParametrosSql = null, XmlResultadoSql = null;

        string Resultado = string.Empty, MensajeResultado = string.Empty;
        try
        {
            #region Parametros
            XmlFinal = XmlHelper.CrearPaginaEsquemaAll(XmlFinal, this);
            string Accion = XmlHelper.strGetNodoTexto(XmlFinal, "//requestFormPost/Accion");
            string AliasForm = XmlHelper.strGetNodoTexto(XmlFinal, "//requestFormPost/AliasForm");
            string AliasTabla = XmlHelper.strGetNodoTexto(XmlFinal, "//requestFormPost/AliasTabla");

            XmlParametrosSql = XmlHelper.StpCrearParametros();
            XmlHelper.StpAgregarNodoParametros(XmlParametrosSql, "sesID", sesID);
            XmlHelper.StpAgregarNodoParametros(XmlParametrosSql, "usuID", usuID);
            XmlHelper.StpAgregarNodoParametros(XmlParametrosSql, "perID", perID);
            new PaginaBase().SerializaJson(ref XmlParametrosSql, AliasForm);
            new PaginaBase().SerializaDiccionarioJson(ref XmlParametrosSql, AliasTabla);
            #endregion Parametros

            switch (Accion)
            {
                case "PRODUCCION_CM":
                    XmlResultadoSql = XmlHelper.CrearXmlDocumentConGetRow(Accion, new cBD().Ejecuta_PA_Oracle_Historico(XmlParametrosSql.OuterXml, "PCK_CONV_DKERNEL.DK_PRODUCCION_CM"));
                    XmlHelper.UnirArbol(XmlFinal, XmlResultadoSql, "//app");
                    break;
                case "PRODUCCION_REG":
                    XmlResultadoSql = XmlHelper.CrearXmlDocumentConGetRow(Accion, new cBD().Ejecuta_PA_Oracle_Online(XmlParametrosSql.OuterXml, "PCK_CONV_DKERNEL.DK_PRODUCCION_REG"));
                    XmlHelper.UnirArbol(XmlFinal, XmlResultadoSql, "//app");
                    break;

                case "PENDIENTES_COO":
                    XmlResultadoSql = XmlHelper.CrearXmlDocumentConGetRow(Accion, new cBD().Ejecuta_PA_Oracle_Online(XmlParametrosSql.OuterXml, "PCK_CONV_DKERNEL.DK_PENDIENTES_COO"));
                    XmlHelper.UnirArbol(XmlFinal, XmlResultadoSql, "//app");
                    break;
                case "PENDIENTES_CM":
                    XmlResultadoSql = XmlHelper.CrearXmlDocumentConGetRow(Accion, new cBD().Ejecuta_PA_Oracle_Online(XmlParametrosSql.OuterXml, "PCK_CONV_DKERNEL.DK_PENDIENTES_CM"));
                    XmlHelper.UnirArbol(XmlFinal, XmlResultadoSql, "//app");
                    break;
                case "PENDIENTES_DTA":
                    XmlResultadoSql = XmlHelper.CrearXmlDocumentConGetRow(Accion, new cBD().Ejecuta_PA_Oracle_Online(XmlParametrosSql.OuterXml, "PCK_CONV_DKERNEL.DK_PENDIENTES_DTA"));
                    XmlHelper.UnirArbol(XmlFinal, XmlResultadoSql, "//app");
                    break;


                case "TOT_INICIO_PROD":
                    XmlResultadoSql = XmlHelper.CrearXmlDocumentConGetRow("Tproduccion", new cBD().Ejecuta_PA_Oracle_Online(XmlParametrosSql.OuterXml, "PCK_CONV_DKERNEL.DK_PANEL_INICIO_PRODUCCION"));
                    XmlHelper.UnirArbol(XmlFinal, XmlResultadoSql, "//app");
                    break;
                case "TOT_INICIO_PEND":
                    XmlResultadoSql = XmlHelper.CrearXmlDocumentConGetRow("TPendientes", new cBD().Ejecuta_PA_Oracle_Online(XmlParametrosSql.OuterXml, "PCK_CONV_DKERNEL.DK_PANEL_INICIO_PENDIENTES"));
                    XmlHelper.UnirArbol(XmlFinal, XmlResultadoSql, "//app");
                    break;
                case "TOT_DETALLE_PROD":
                    XmlResultadoSql = XmlHelper.CrearXmlDocumentConGetRow("Tproduccion", new cBD().Ejecuta_PA_Oracle_Online(XmlParametrosSql.OuterXml, "PCK_CONV_DKERNEL.DK_PANEL_DETALLE_PRODUCCION"));
                    XmlHelper.UnirArbol(XmlFinal, XmlResultadoSql, "//app");
                    break;
                case "TOT_DETALLE_PEND":
                    XmlResultadoSql = XmlHelper.CrearXmlDocumentConGetRow("TPendientes", new cBD().Ejecuta_PA_Oracle_Online(XmlParametrosSql.OuterXml, "PCK_CONV_DKERNEL.DK_PANEL_DETALLE_PENDIENTES"));
                    XmlHelper.UnirArbol(XmlFinal, XmlResultadoSql, "//app");
                    break;
                case "TOT_INICIO_CALI":
                    XmlResultadoSql = XmlHelper.CrearXmlDocumentConGetRow("TCalidad", new cBD().Ejecuta_PA_Oracle_Historico(XmlParametrosSql.OuterXml, "PCK_CONV_DKERNEL.DK_PANEL_INICIO_CALIDAD"));
                    XmlHelper.UnirArbol(XmlFinal, XmlResultadoSql, "//app");
                    break;
                case "TOT_INICIO_CANT":
                    XmlResultadoSql = XmlHelper.CrearXmlDocumentConGetRow("TCantidad", new cBD().Ejecuta_PA_Oracle_Historico(XmlParametrosSql.OuterXml, "PCK_CONV_DKERNEL.DK_PANEL_INICIO_CANTIDAD"));
                    XmlHelper.UnirArbol(XmlFinal, XmlResultadoSql, "//app");
                    break;
                case "TOT_DETALLE_CALI":
                    XmlResultadoSql = XmlHelper.CrearXmlDocumentConGetRow("TCalidad", new cBD().Ejecuta_PA_Oracle_Historico(XmlParametrosSql.OuterXml, "PCK_CONV_DKERNEL.DK_PANEL_DETALLE_CALIDAD"));
                    XmlHelper.UnirArbol(XmlFinal, XmlResultadoSql, "//app");
                    break;
                case "TOT_DETALLE_CANT":
                    XmlResultadoSql = XmlHelper.CrearXmlDocumentConGetRow("TCantidad", new cBD().Ejecuta_PA_Oracle_Historico(XmlParametrosSql.OuterXml, "PCK_CONV_DKERNEL.DK_PANEL_DETALLE_CANTIDAD"));
                    XmlHelper.UnirArbol(XmlFinal, XmlResultadoSql, "//app");
                    break;
            }

            XmlHelper.AgregaNodoConTexto(XmlFinal, "//app", "Accion", Accion);
            XmlHelper.appDibujarHTML(XmlFinal, Request.MapPath("detalle.xsl"), this);
        }
        catch (Exception ex)
        {
            new Log().GuardaLog("detalleAcciones:Page_Load", ex.Source, ex.Message, ex.StackTrace);
        }
        finally
        {
            XmlFinal = null;
            XmlParametrosSql = null;
            XmlResultadoSql = null;
        }
    }
}