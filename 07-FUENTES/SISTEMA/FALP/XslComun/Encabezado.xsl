<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
<xsl:output method="xml" version="1.0" encoding="utf-8" indent="yes" omit-xml-declaration="yes"	standalone="yes" />
<xsl:template name="DibujaEncabezado">
  <meta charset="utf-8" />
  <meta name="viewport" content="width=device-width, initial-scale=1.0" />
  <meta name="description" content="" />
  <meta name="author" content="" />

  <title>..: FALP :..</title>
  
  <meta http-equiv="Cache-Control" content="no-cache, no-store, must-revalidate" />
  <meta http-equiv="Pragma" content="no-cache" />
  <meta http-equiv="Expires" content="0" />
  <meta http-equiv="X-UA-Compatible" content="IE=Edge" />
  

    <link href="../bower_components/bootstrap/dist/css/bootstrap.min.css" rel="stylesheet"/>
  
	  <link href="../dist/css/sb-admin-2.css" rel="stylesheet"/>
	  <link href="../bower_components/font-awesome/css/font-awesome.min.css" rel="stylesheet"/>
    <link href="../bower_components/bootstrap-social/bootstrap-social.css" rel="stylesheet"/>
  
    <link href="../css/bootstrap-datetimepicker.min.css" rel="stylesheet" />
  
</xsl:template>  
</xsl:stylesheet>
