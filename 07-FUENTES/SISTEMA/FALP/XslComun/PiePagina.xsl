<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
  <xsl:output method="xml" version="1.0" encoding="utf-8" indent="yes" omit-xml-declaration="yes"	standalone="yes" />
  <xsl:template name="DibujaPie">
    <div class="footer">
	    <div class="pull-right">
		    Sistema desarrollado para: <strong>PINTO-PIGA</strong> Agrícola.
	    </div>
	    <div>
		    <strong>Copyright</strong> Todos los derechos reservados 2016 Power By <a href="http:\\www.mysmart.cl">MySmart Solutions Limitada</a>
	    </div>
    </div>
  </xsl:template>
</xsl:stylesheet>
