 <xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
  <xsl:output method="xml" version="1.0" encoding="utf-8" indent="yes" omit-xml-declaration="yes"	standalone="yes" />
  <xsl:template name="DibujaLogo">
    <div class="navbar-header">
      <a class="navbar-toggle pull-right" data-toggle="collapse" data-target=".sidebar-collapse">
        <i class="fa fa-bars"></i> Menu
      </a>
      <div class="navbar-brand" style="padding:0px;">
        <a href="../Admin/frm_inicio.aspx">
          <img src="../img/logo_top.png" class="" alt=""></img>
        </a>
      </div>
    </div>
  </xsl:template>  
  <xsl:template name="DibujaMensajes">
    <ul class="nav navbar-top-links navbar-right">
        <li>
            <span class="m-r-sm text-muted welcome-message">Pinto Piga PLUS</span>
        </li>
        <li class="dropdown">
        <a class="dropdown-toggle count-info" data-toggle="dropdown" href="#">
            <i class="fa fa-envelope"></i>  <span class="label label-warning">16</span>
        </a>
    
        <ul class="dropdown-menu dropdown-messages">
            <li>
                <div class="dropdown-messages-box">
                    <a href="profile.php" class="pull-left">
                        <img alt="image" class="img-circle" src="img/a7.jpg"/>
                    </a>
                    <div class="media-body">
                        <small class="pull-right">hace 46 Minutos</small>
                        <strong>Juan Jose Araya</strong> started following <strong>Monica Smith</strong>. <br/>
                        <small class="text-muted">3 days ago at 7:58 pm - 10.06.2014</small>
                    </div>
                </div>
            </li>
            <li class="divider"></li>
            <li>
                <div class="dropdown-messages-box">
                    <a href="profile.php" class="pull-left">
                        <img alt="image" class="img-circle" src="img/a4.jpg"/>
                    </a>
                    <div class="media-body ">
                        <small class="pull-right text-navy">Hace 5 Horas</small>
                        <strong>Chris Johnatan Overtunk</strong> started following <strong>Monica Smith</strong>. <br/>
                        <small class="text-muted">Yesterday 1:21 pm - 11.06.2014</small>
                    </div>
                </div>
            </li>
            <li class="divider"></li>
            <li>
                <div class="dropdown-messages-box">
                    <a href="profile.php" class="pull-left">
                        <img alt="image" class="img-circle" src="img/profile.jpg"/>
                    </a>
                    <div class="media-body ">
                        <small class="pull-right">Hace 23 Horas</small>
                        <strong>Monica Smith</strong> love <strong>Kim Smith</strong>. <br/>
                        <small class="text-muted">2 days ago at 2:30 am - 11.06.2014</small>
                    </div>
                </div>
            </li>
            <li class="divider"></li>
            <li>
                <div class="text-center link-block">
                    <a href="mailbox.php">
                        <i class="fa fa-envelope"></i> <strong>Leer todos los mensajes</strong>
                    </a>
                </div>
            </li>
        </ul>
    </li>
    
        <li class="dropdown">
        <a class="dropdown-toggle count-info" data-toggle="dropdown" href="#">
            <i class="fa fa-bell"></i>  <span class="label label-primary">8</span>
        </a>
        <ul class="dropdown-menu dropdown-alerts">
            <li>
                <a href="mailbox.php">
                    <div>
                        <i class="fa fa-warning fa-fw"></i> You have 16 messages
                        <span class="pull-right text-muted small">4 minutes ago</span>
                    </div>
                </a>
            </li>
            <li class="divider"></li>
            <li>
                <a href="profile.php">
                    <div>
                        <i class="fa fa-warning fa-fw"></i> 3 New Followers
                        <span class="pull-right text-muted small">12 minutes ago</span>
                    </div>
                </a>
            </li>
            <li class="divider"></li>
            <li>
                <a href="grid_options.php">
                    <div>
                        <i class="fa fa-warning fa-fw"></i> Server Rebooted
                        <span class="pull-right text-muted small">4 minutes ago</span>
                    </div>
                </a>
            </li>
            <li class="divider"></li>
            <li>
                <div class="text-center link-block">
                    <a href="notifications.php">
                        <strong>Ver todas las alertas</strong>
                        <i class="fa fa-angle-right"></i>
                    </a>
                </div>
            </li>
        </ul>
    </li>
    
        <li class="dropdown">
        <a class="dropdown-toggle count-info" data-toggle="dropdown" href="#">
            <i class="fa fa-flag"></i>
        </a>
        <ul class="dropdown-menu dropdown-alerts">
            <li>
                <a href="mailbox.php">
                    <div>
                        <img src="img/flags/16/Spain.png"/> Español
                    </div>
                </a>
            </li>
            <li class="divider"></li>
            <li>
                <a href="profile.php">
                    <div>
                        <img src="img/flags/16/United-States.png"/> Ingles
                    </div>
                </a>
            </li>
            <li class="divider"></li>
        </ul>
    </li>
      
        <li>
            <a href="login.php">
                <i class="fa fa-sign-out"></i> Cerrar Sesion
            </a>
        </li>
    </ul>
 </xsl:template>
</xsl:stylesheet>