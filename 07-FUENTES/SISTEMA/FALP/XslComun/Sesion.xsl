<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
  <xsl:output method="xml" version="1.0" encoding="utf-8" indent="yes" omit-xml-declaration="yes"	standalone="yes" />
 <xsl:template name="DibujaSesion">
   <!-- begin SIDE NAV USER PANEL -->
   <li class="side-user hidden-xs">
     <img class="img-circle" src="../img/_profile-pic.jpg" alt=""></img>
       <p class="name tooltip-sidebar-logout">
         <xsl:value-of select="//LaSesion/Datos/row/@PrimerNombre" />
         <span class="last-name">
           <xsl:text> </xsl:text><xsl:value-of select="//LaSesion/Datos/row/@ApellidoPat" />
         </span>
       </p>
       <div class="clearfix"></div>
     </li>
   <!-- end SIDE NAV USER PANEL -->
</xsl:template>
</xsl:stylesheet>
