<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
  <xsl:output method="xml" version="1.0" encoding="utf-8" indent="yes" omit-xml-declaration="yes"	standalone="yes" />
<xsl:template name="DibujaMenu">
  <xsl:for-each select="//Menus/Datos/row[@ID_PADRE ='-1']">
    <xsl:variable name="ID" select="@ID"></xsl:variable>
    <li>
      <a href="javascript:;">
        <i class="fa fa-plus-square-o">
          <xsl:attribute name="class"><xsl:value-of select="@ICONO" /></xsl:attribute>
        </i>
        <span class="nav-label">
          <xsl:value-of select="@NOMBRE" />
        </span>
        <span class="fa arrow"></span>
      </a>
      <ul class="nav nav-second-level collapse">
        <xsl:attribute name="id">
          <xsl:value-of select="@ID" />
        </xsl:attribute>
        <xsl:for-each select="//Menus/Datos/row[@ID_PADRE=$ID]">
          <xsl:variable name="ID2" select="@ID"></xsl:variable>
          <li id="">
            <xsl:attribute name="id"><xsl:value-of select="@ID" /></xsl:attribute>
            <a href="javascript:">
              <xsl:attribute name="onclick">javascript:IrAUrl('<xsl:value-of select="@URL"/>','<xsl:value-of select="@ID"/>');</xsl:attribute>
              <xsl:value-of select="@NOMBRE" /><span class="fa arrow"></span>
            </a>
            <ul class="nav nav-third-level">
              <xsl:attribute name="id"><xsl:value-of select="@ID" /></xsl:attribute>
              <xsl:for-each select="//Menus/Datos/row[@ID_PADRE=$ID2]">
                <li id="">
                  <xsl:attribute name="id"><xsl:value-of select="@ID"/></xsl:attribute>
                  <a href="javascript:">
                    <xsl:attribute name="onclick">javascript:IrAUrl('<xsl:value-of select="@URL"/>','<xsl:value-of select="@ID"/>');</xsl:attribute>
                    <xsl:value-of select="@NOMBRE" />
                  </a>
                </li>
               </xsl:for-each>
            </ul>
          </li>
         </xsl:for-each>
      </ul>
    </li>
  </xsl:for-each>
  <li>
      <a href="alertas_wf.php"><i class="fa fa-bell"></i> <span class="nav-label">Alertas</span></a>
  </li>
  <input id="idMenu" type="hidden">
      <xsl:attribute name="value">
        <xsl:value-of select="//app/requestFormApp/IdUrlRequest"/>
      </xsl:attribute>
    </input>
</xsl:template>
</xsl:stylesheet>
