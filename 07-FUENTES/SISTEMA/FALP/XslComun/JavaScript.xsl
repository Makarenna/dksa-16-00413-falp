﻿<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
  <xsl:output method="xml" version="1.0" encoding="utf-8" indent="yes" omit-xml-declaration="yes"	standalone="yes" />
  <xsl:template name="DibujaJavaScript">    
    <script src="../bower_components/bootstrap/dist/js/bootstrap.min.js"></script>
    <!--<script src="../bower_components/metisMenu/dist/metisMenu.min.js"></script>-->
    <script src="../dist/js/sb-admin-2.js"></script>
    <script src="../js/moment-with-locales.js"></script>
    <script src="../js/bootstrap-datetimepicker.js"></script>
  </xsl:template>
  <xsl:template name="DibujaJS_header">
    <!--<script src="../bower_components/jquery/dist/jquery.min.js"></script>-->
    <!--<script src="http://ajax.googleapis.com/ajax/libs/jquery/1.10.2/jquery.min.js"></script>
    <script type="text/javascript">
    var jQuery_1_9 = $.noConflict(true);
    </script>-->
    <!--<script src="../js/jquery-1.7.1.min.js"></script>-->
    
    <script src="../js/Comunes.js"></script>
  </xsl:template>
</xsl:stylesheet>
