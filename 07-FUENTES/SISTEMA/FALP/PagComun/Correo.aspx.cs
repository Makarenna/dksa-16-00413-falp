﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Xml;
using library.xml;


using library.common;
using System.IO;

public partial class Correo : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        #region  Parametros

        XmlDocument XmlFinal = null, XmlAuxA = null, XmlAuxB = null, XmlAuxC = null, XmlAuxD = null, XmlAuxE = null;

        //Crear XML Schema
        XmlFinal = XmlHelper.CrearPaginaEsquemaAll(XmlFinal, this);
        string Accion = XmlHelper.strGetNodoTexto(XmlFinal, "//requestFormPost/Accion");
        string Accion2 = XmlHelper.strGetNodoTexto(XmlFinal, "//requestFormPost/Accion2");
        string Alias = XmlHelper.strGetNodoTexto(XmlFinal, "//requestFormPost/alias");

        XmlAuxA = XmlHelper.StpCrearParametros();
        string[] LosAlias = Alias.Split('~');

        //for de split Personas
        for (int j = 0; j < LosAlias.Length; j++)
        {
            if (!LosAlias[j].Equals(""))
            {
                string[] MyFiltro = LosAlias[j].Split('|');
                XmlHelper.StpAgregarNodoParametros(XmlAuxA, MyFiltro[0], MyFiltro[1]);
            }
        }//FIN_FOR

        #endregion Parametros

        try
        {
            switch (Accion)
            {
                case "EnviaMail":

                    XmlAuxC = XmlHelper.CrearXmlDocumentConGetRow("ParametroCorreo", new cBD().EjecutaPA(XmlAuxA.OuterXml, "MSS_B_ParametrosCorreo"));
                    cCorreo ServicioCorreo = new cCorreo();
                    ServicioCorreo.EnviaCorreo(XmlAuxC.InnerXml);

                    break;
            }

            if (XmlAuxC != null)
            {
                XmlHelper.UnirArbol(XmlFinal, XmlAuxC, "//app");
                XmlHelper.AgregaNodoConTexto(XmlFinal, "//app", "Accion", Accion);
                XmlHelper.AgregaNodoConTexto(XmlFinal, "//app", "Accion2", Accion2);

                string contenidoArchivo = XmlHelper.appDibujarXSLT(XmlFinal, Request.MapPath("Correo.xsl"));
            }
        }
        catch (Exception ex)
        {
            string msg = ex.Message;
        }

        Response.Write("");
        Response.End();

        //Limpiar Todo
        XmlFinal = null;
        XmlAuxA = XmlFinal;
        XmlAuxB = XmlFinal;
        XmlAuxC = XmlFinal;
        XmlAuxD = XmlFinal;
        XmlAuxE = XmlFinal;
    }
}
