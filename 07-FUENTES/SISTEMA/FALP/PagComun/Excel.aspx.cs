﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Xml;
using library.xml;


using library.common;
using System.IO;

public partial class Excel : System.Web.UI.Page
{
    public delegate void LimpiarCarpeta(string RutaCarpeta, int Antiguedad);
    protected void Page_Load(object sender, EventArgs e)
    {
        XmlDocument XmlFinal = null, XmlParametrosSql = null, XmlResultadoSql = null;
        #region Parametros
        XmlFinal = XmlHelper.CrearPaginaEsquemaAll(XmlFinal, this);
        string Accion = XmlHelper.strGetNodoTexto(XmlFinal, "//requestFormPost/Accion");
        string AliasForm = XmlHelper.strGetNodoTexto(XmlFinal, "//requestFormPost/AliasForm");
        string AliasNombreArchivo = XmlHelper.strGetNodoTexto(XmlFinal, "//requestFormPost/AliasNombreArchivo");

        XmlParametrosSql = XmlHelper.StpCrearParametros();
        new PaginaBase().SerializaJson(ref XmlParametrosSql, AliasForm);
        #endregion Parametros

        string NombreArchivo = "";
        string RutaArchivo = string.Empty;

        try
        {
            switch (Accion)
            {
                case "INICIO_PRODUCCION":
                    NombreArchivo =  string.Concat(AliasNombreArchivo, ".xls");
                    XmlResultadoSql = XmlHelper.CrearXmlDocumentConGetRow(Accion, new cBD().Ejecuta_PA_Oracle_Historico(XmlParametrosSql.OuterXml, "PCK_CONV_DKERNEL.DK_INICIO_PRODUCCION_EXCEL"));
                    break;
                 case "DETALLE_PRODUCCION":
                    NombreArchivo =  string.Concat(AliasNombreArchivo, ".xls");
                    XmlResultadoSql = XmlHelper.CrearXmlDocumentConGetRow(Accion, new cBD().Ejecuta_PA_Oracle_Historico(XmlParametrosSql.OuterXml, "PCK_CONV_DKERNEL.DK_DETALLE_PRODUCCION_EXCEL"));
                    break;
                case "DETALLE_PRODUCCION_CM":
                    NombreArchivo =  string.Concat(AliasNombreArchivo, ".xls");
                    XmlResultadoSql = XmlHelper.CrearXmlDocumentConGetRow(Accion, new cBD().Ejecuta_PA_Oracle_Historico(XmlParametrosSql.OuterXml, "PCK_CONV_DKERNEL.DK_DETALLE_PRODUCCION_CM_EXCEL"));
                    break;
                case "DETALLE_PRODUCCION_REG":
                    NombreArchivo =  string.Concat(AliasNombreArchivo, ".xls");
                    XmlResultadoSql = XmlHelper.CrearXmlDocumentConGetRow(Accion, new cBD().Ejecuta_PA_Oracle_Historico(XmlParametrosSql.OuterXml, "PCK_CONV_DKERNEL.DK_DETALLE_PRODUCCION_RE_EXCEL"));
                    break;
            }
            if (XmlResultadoSql != null)
            {
                RutaArchivo = string.Concat("../temp/", NombreArchivo);
                XmlHelper.UnirArbol(XmlFinal, XmlResultadoSql, "//app");
                XmlHelper.AgregaNodoConTexto(XmlFinal, "//app", "Accion", Accion);

                string contenidoArchivo = XmlHelper.appDibujarXSLT(XmlFinal, Request.MapPath(string.Format("Excel_{0}.xsl", Accion)));

                new Log().GuardaLog("RUTA TEMP EXCEL", Server.MapPath(RutaArchivo),"","");

                if (System.IO.File.Exists(Server.MapPath(RutaArchivo)))
                {
                    System.IO.File.Delete(Server.MapPath(RutaArchivo));
                }

                using (StreamWriter _Writer = new StreamWriter(Server.MapPath(RutaArchivo), true, System.Text.Encoding.UTF8))
                {
                    _Writer.Write(contenidoArchivo);
                }
            }
        }
        catch (Exception ex)
        {
            new Log().GuardaLog("Excel:Page_Load", ex.Source, ex.Message, ex.StackTrace);
        }
        finally
        {
            XmlFinal = null;
            XmlParametrosSql = null;
            XmlResultadoSql = null;
            LimpiarCarpeta LimpiaDelegate = new LimpiarCarpeta(LimpiarLocal);
            LimpiaDelegate(Server.MapPath("../temp/"), 5);
        }

        Response.Write(RutaArchivo);
        Response.End();
    }
    public void LimpiarLocal(string RutaCarpeta, int Antiguedad)
    {
        Mantencion ServicioMantencion = new Mantencion();
        ServicioMantencion.LimpiarCarpeta(RutaCarpeta, Antiguedad);
    }
}
