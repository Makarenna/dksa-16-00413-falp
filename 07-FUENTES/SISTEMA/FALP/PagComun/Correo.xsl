<xsl:stylesheet version="1.0"  xmlns="urn:schemas-microsoft-com:office:spreadsheet"
    xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
 xmlns:msxsl="urn:schemas-microsoft-com:xslt"
 xmlns:user="urn:my-scripts"
 xmlns:o="urn:schemas-microsoft-com:office:office"
 xmlns:x="urn:schemas-microsoft-com:office:excel"
 xmlns:ss="urn:schemas-microsoft-com:office:spreadsheet" >
  <xsl:output encoding="ISO-8859-1" />
  <xsl:template match="/">
            <Workbook xmlns="urn:schemas-microsoft-com:office:spreadsheet"
      xmlns:o="urn:schemas-microsoft-com:office:office"
      xmlns:x="urn:schemas-microsoft-com:office:excel"
      xmlns:ss="urn:schemas-microsoft-com:office:spreadsheet"
      xmlns:html="http://www.w3.org/TR/REC-html40">
              <Styles>
                <Style ss:ID="Default" ss:Name="Normal">
                  <Alignment ss:Vertical="Bottom" />
                  <Borders />
                  <Font />
                  <Interior />
                  <NumberFormat />
                  <Protection />
                </Style>
                <Style ss:ID="LetraCursiva">
                  <Font ss:FontName="Calibri" x:Family="Swiss" ss:Size="7" ss:Color="#000000" ss:Italic="1"/>
                </Style>
                <Style ss:ID="Titulo">
                  <Alignment ss:Horizontal="Center" ss:Vertical="Bottom"/>
                  <Borders>
                    <Border ss:Position="Bottom" ss:LineStyle="Continuous" ss:Weight="1"/>
                    <Border ss:Position="Left" ss:LineStyle="Continuous" ss:Weight="1"/>
                    <Border ss:Position="Right" ss:LineStyle="Continuous" ss:Weight="1"/>
                    <Border ss:Position="Top" ss:LineStyle="Continuous" ss:Weight="1"/>
                  </Borders>
                  <Font ss:FontName="Calibri" x:Family="Swiss" ss:Size="12" ss:Color="#000000" ss:Bold="1"/>
                </Style>
                <Style ss:ID="BordeTitulo">
                  <Alignment ss:Horizontal="Center" ss:Vertical="Bottom" ss:WrapText="1"/>
                  <Borders>
                    <Border ss:Position="Bottom" ss:LineStyle="Continuous" ss:Weight="1"/>
                    <Border ss:Position="Left" ss:LineStyle="Continuous" ss:Weight="1"/>
                    <Border ss:Position="Right" ss:LineStyle="Continuous" ss:Weight="1"/>
                    <Border ss:Position="Top" ss:LineStyle="Continuous" ss:Weight="1"/>
                  </Borders>
                  <Font ss:FontName="Calibri" x:Family="Swiss" ss:Size="10" ss:Color="#FFFFFF" ss:Bold="1"/>
                  <Interior ss:Color="#005548" ss:Pattern="Solid"/>
                </Style>
                <Style ss:ID="BordeNormal">
                  <Alignment ss:Horizontal="Left" ss:Vertical="Center" ss:WrapText="1"/>
                  <Borders>
                    <Border ss:Position="Bottom" ss:LineStyle="Continuous" ss:Weight="1"/>
                    <Border ss:Position="Left" ss:LineStyle="Continuous" ss:Weight="1"/>
                    <Border ss:Position="Right" ss:LineStyle="Continuous" ss:Weight="1"/>
                    <Border ss:Position="Top" ss:LineStyle="Continuous" ss:Weight="1"/>
                  </Borders>
                  <Font ss:FontName="Calibri" x:Family="Swiss" ss:Size="10" ss:Color="#000000"/>
                </Style>
              </Styles>
              <xsl:choose>
                <xsl:when test="//app/Accion='base'">
                  <Worksheet ss:Name="base">
                    <Table x:FullColumns="1" x:FullRows="1">
                      <Column ss:AutoFitWidth="0" ss:Width="20"/>
                      <Row ss:Index="1">
                        <Cell ss:StyleID="BordeTitulo">
                          <Data ss:Type="String">Fila</Data>
                        </Cell>
                      </Row>
                      <xsl:for-each select="//app/base/Datos/row">
                        <Row>
                          <Cell ss:StyleID="BordeNormal">
                            <Data ss:Type="String">
                              <xsl:value-of select="@Fila"/>
                            </Data>
                          </Cell>
                        </Row>
                      </xsl:for-each>
                    </Table>
                  </Worksheet>
                </xsl:when>
                
                <xsl:when test="//app/Accion='UsuariosPerfil'">
                  <Worksheet ss:Name="Usuario">
                    <Table x:FullColumns="1" x:FullRows="1">
                      <Column ss:AutoFitWidth="0" ss:Width="20"/>
                      <Row ss:Index="1">
                        <Cell ss:StyleID="BordeTitulo">
                          <Data ss:Type="String">Fila</Data>
                        </Cell>
                        <Cell ss:StyleID="BordeTitulo">
                          <Data ss:Type="String">Rut</Data>
                        </Cell>
                        <Cell ss:StyleID="BordeTitulo">
                          <Data ss:Type="String">Nombre</Data>
                        </Cell>
                        <Cell ss:StyleID="BordeTitulo">
                          <Data ss:Type="String">Institucion</Data>
                        </Cell>
                        <Cell ss:StyleID="BordeTitulo">
                          <Data ss:Type="String">Sede</Data>
                        </Cell>
                        <Cell ss:StyleID="BordeTitulo">
                          <Data ss:Type="String">Perfil</Data>
                        </Cell>

                      </Row>
                      <xsl:for-each select="//app/UsuariosPerfil/Datos/row">
                        <Row>
                          <Cell ss:StyleID="BordeNormal">
                            <Data ss:Type="String">
                              <xsl:value-of select="@Fila"/>
                            </Data>
                          </Cell>
                          <Cell ss:StyleID="BordeNormal">
                            <Data ss:Type="String">
                              <xsl:value-of select="@Rut"/>
                            </Data>
                          </Cell>
                          <Cell ss:StyleID="BordeNormal">
                            <Data ss:Type="String">
                              <xsl:value-of select="@Nombre"/>
                            </Data>
                          </Cell>
                          <Cell ss:StyleID="BordeNormal">
                            <Data ss:Type="String">
                              <xsl:value-of select="@Institucion"/>
                            </Data>
                          </Cell>
                          <Cell ss:StyleID="BordeNormal">
                            <Data ss:Type="String">
                              <xsl:value-of select="@Sede"/>
                            </Data>
                          </Cell>
                          <Cell ss:StyleID="BordeNormal">
                            <Data ss:Type="String">
                              <xsl:value-of select="@Perfil"/>
                            </Data>
                          </Cell>

                        </Row>
                      </xsl:for-each>
                    </Table>
                  </Worksheet>
                </xsl:when>
                
                <xsl:when test="//app/Accion='GestionIntervencion'">
                  <Worksheet ss:Name="GestionIntervencion">
                    <Table x:FullColumns="1" x:FullRows="1">
                  <Column ss:AutoFitWidth="0" ss:Width="20"/>
                  <Row ss:Index="1">
                    <Cell ss:StyleID="BordeTitulo">
                      <Data ss:Type="String">Fila</Data>
                    </Cell>
                    <Cell ss:StyleID="BordeTitulo">
                      <Data ss:Type="String">Institucion</Data>
                    </Cell>
                    <Cell ss:StyleID="BordeTitulo">
                      <Data ss:Type="String">Segmentado</Data>
                    </Cell>
                    <Cell ss:StyleID="BordeTitulo">
                      <Data ss:Type="String">Sede</Data>
                    </Cell>
                    <Cell ss:StyleID="BordeTitulo">
                      <Data ss:Type="String">Estado</Data>
                    </Cell>
                    <Cell ss:StyleID="BordeTitulo">
                      <Data ss:Type="String">Rut</Data>
                    </Cell>
                    <Cell ss:StyleID="BordeTitulo">
                      <Data ss:Type="String">ApellidoPaterno</Data>
                    </Cell>
                    <Cell ss:StyleID="BordeTitulo">
                      <Data ss:Type="String">ApellidoMaterno</Data>
                    </Cell>
                    <Cell ss:StyleID="BordeTitulo">
                      <Data ss:Type="String">Nombre</Data>
                    </Cell>
                    <Cell ss:StyleID="BordeTitulo">
                      <Data ss:Type="String">NombreCarrera</Data>
                    </Cell>
                    <Cell ss:StyleID="BordeTitulo">
                      <Data ss:Type="String">NombreJornada</Data>
                    </Cell>
                    <Cell ss:StyleID="BordeTitulo">
                      <Data ss:Type="String">NombreIntervencion</Data>
                    </Cell>
                    <Cell ss:StyleID="BordeTitulo">
                      <Data ss:Type="String">Sección</Data>
                    </Cell>
                    <Cell ss:StyleID="BordeTitulo">
                      <Data ss:Type="String">Fecha Inicio</Data>
                    </Cell>
                    <Cell ss:StyleID="BordeTitulo">
                      <Data ss:Type="String">Fecha Fin</Data>
                    </Cell>
                    <Cell ss:StyleID="BordeTitulo">
                      <Data ss:Type="String">Descripción</Data>
                    </Cell>
                    <Cell ss:StyleID="BordeTitulo">
                      <Data ss:Type="String">Rut Encargado</Data>
                    </Cell>
                    <Cell ss:StyleID="BordeTitulo">
                      <Data ss:Type="String">Nombre Encargado</Data>
                    </Cell>
                  </Row>
                  <xsl:for-each select="//app/GestionIntervencion/Datos/row">
                    <Row>
                      <Cell ss:StyleID="BordeNormal">
                        <Data ss:Type="String">
                          <xsl:value-of select="@Fila"/>
                        </Data>
                      </Cell>
                      <Cell ss:StyleID="BordeNormal">
                        <Data ss:Type="String">
                          <xsl:value-of select="@NombreInstitucion"/>
                        </Data>
                      </Cell>
                      <Cell ss:StyleID="BordeNormal">
                        <Data ss:Type="String">
                          <xsl:value-of select="@NombreSegmentador"/>
                        </Data>
                      </Cell>
                      <Cell ss:StyleID="BordeNormal">
                        <Data ss:Type="String">
                          <xsl:value-of select="@NombreSede"/>
                        </Data>
                      </Cell>
                      <Cell ss:StyleID="BordeNormal">
                        <Data ss:Type="String">
                          <xsl:value-of select="@NombreEstado"/>
                        </Data>
                      </Cell>
                      <Cell ss:StyleID="BordeNormal">
                        <Data ss:Type="String">
                          <xsl:value-of select="@Rut"/>
                        </Data>
                      </Cell>
                      <Cell ss:StyleID="BordeNormal">
                        <Data ss:Type="String">
                          <xsl:value-of select="@ApellidoPaterno"/>
                        </Data>
                      </Cell>
                      <Cell ss:StyleID="BordeNormal">
                        <Data ss:Type="String">
                          <xsl:value-of select="@ApellidoMaterno"/>
                        </Data>
                      </Cell>
                      <Cell ss:StyleID="BordeNormal">
                        <Data ss:Type="String">
                          <xsl:value-of select="@Nombre"/>
                        </Data>
                      </Cell>
                      <Cell ss:StyleID="BordeNormal">
                        <Data ss:Type="String">
                          <xsl:value-of select="@NombreCarrera"/>
                        </Data>
                      </Cell>
                      <Cell ss:StyleID="BordeNormal">
                        <Data ss:Type="String">
                          <xsl:value-of select="@NombreJornada"/>
                        </Data>
                      </Cell>
                      <Cell ss:StyleID="BordeNormal">
                        <Data ss:Type="String">
                          <xsl:value-of select="@NombreIntervencion"/>
                        </Data>
                      </Cell>
                      <Cell ss:StyleID="BordeNormal">
                        <Data ss:Type="String">
                          <xsl:value-of select="@Seccion"/>
                        </Data>
                      </Cell>
                      <Cell ss:StyleID="BordeNormal">
                        <Data ss:Type="String">
                          <xsl:value-of select="@FechaInicio"/>
                        </Data>
                      </Cell>
                      <Cell ss:StyleID="BordeNormal">
                        <Data ss:Type="String">
                          <xsl:value-of select="@FechaFin"/>
                        </Data>
                      </Cell>
                      <Cell ss:StyleID="BordeNormal">
                        <Data ss:Type="String">
                          <xsl:value-of select="@Descripcion"/>
                        </Data>
                      </Cell>
                      <Cell ss:StyleID="BordeNormal">
                        <Data ss:Type="String">
                          <xsl:value-of select="@RutEncargado"/>
                        </Data>
                      </Cell>
                      <Cell ss:StyleID="BordeNormal">
                        <Data ss:Type="String">
                          <xsl:value-of select="@NombreEncargado"/>
                        </Data>
                      </Cell>
                    </Row>
                  </xsl:for-each>
                </Table>
                  </Worksheet>
                </xsl:when>
                
                <xsl:when test="//app/Accion='Gest_Estadistica'">
                  <Worksheet ss:Name="Estadistica">
                    <Table x:FullColumns="1" x:FullRows="1">
                      <Column ss:AutoFitWidth="0" ss:Width="20"/>
                      <Row ss:Index="1">
                        <Cell ss:StyleID="BordeTitulo">
                          <Data ss:Type="String">#</Data>
                        </Cell>
                        <Cell ss:StyleID="BordeTitulo">
                          <Data ss:Type="String">Institución</Data>
                        </Cell>
                          <Cell ss:StyleID="BordeTitulo">
                          <Data ss:Type="String">Sede</Data>
                        </Cell>
                          <Cell ss:StyleID="BordeTitulo">
                          <Data ss:Type="String">Segmentador</Data>
                        </Cell>
                        <Cell ss:StyleID="BordeTitulo">
                          <Data ss:Type="String">Fecha</Data>
                        </Cell>
                        <Cell ss:StyleID="BordeTitulo">
                          <Data ss:Type="String">Matriculados</Data>
                        </Cell>
                        <Cell ss:StyleID="BordeTitulo">
                          <Data ss:Type="String">Retirados</Data>
                        </Cell>
                        <Cell ss:StyleID="BordeTitulo">
                          <Data ss:Type="String">Clasificados para Intervención</Data>
                        </Cell>
                        <Cell ss:StyleID="BordeTitulo">
                          <Data ss:Type="String">Una o más Intervenciones</Data>
                        </Cell>
                        <Cell ss:StyleID="BordeTitulo">
                          <Data ss:Type="String">Retirados con Intervención</Data>
                        </Cell>
                        <Cell ss:StyleID="BordeTitulo">
                          <Data ss:Type="String">Retirados sin Intervencion</Data>
                        </Cell>
                      </Row>
                      <xsl:for-each select="//app/Gest_Estadistica/Datos/row">
                        <Row>
                          <Cell ss:StyleID="BordeNormal">
                            <Data ss:Type="String">
                              <xsl:value-of select="@Fila"/>
                            </Data>
                          </Cell>
                          <Cell ss:StyleID="BordeNormal">
                            <Data ss:Type="String">
                              <xsl:value-of select="@NombreInstitucion"/>
                            </Data>
                          </Cell>
                          <Cell ss:StyleID="BordeNormal">
                            <Data ss:Type="String">
                              <xsl:value-of select="@NombreSegmentador"/>
                            </Data>
                          </Cell>
                          <Cell ss:StyleID="BordeNormal">
                            <Data ss:Type="String">
                              <xsl:value-of select="@NombreSede"/>
                            </Data>
                          </Cell>
                          <Cell ss:StyleID="BordeNormal">
                            <Data ss:Type="String">
                              <xsl:value-of select="@EFECHA_EJEC"/>
                            </Data>
                          </Cell>
                          <Cell ss:StyleID="BordeNormal">
                            <Data ss:Type="String">
                              <xsl:value-of select="@EMASTRICULADOS"/>
                            </Data>
                          </Cell>
                          <Cell ss:StyleID="BordeNormal">
                            <Data ss:Type="String">
                              <xsl:value-of select="@ERETIRADOS"/>
                            </Data>
                          </Cell>
                          <Cell ss:StyleID="BordeNormal">
                            <Data ss:Type="String">
                              <xsl:value-of select="@ECLASIFICADOS"/>
                            </Data>
                          </Cell>
                          <Cell ss:StyleID="BordeNormal">
                            <Data ss:Type="String">
                              <xsl:value-of select="@EINTERVENCION"/>
                            </Data>
                          </Cell>
                          <Cell ss:StyleID="BordeNormal">
                            <Data ss:Type="String">
                              <xsl:value-of select="@ERETIRADOS1"/>
                            </Data>
                          </Cell>
                          <Cell ss:StyleID="BordeNormal">
                            <Data ss:Type="String">
                              <xsl:value-of select="@ERETIRADOS_SIN"/>
                            </Data>
                          </Cell>
                        </Row>
                      </xsl:for-each>
                    </Table>
                  </Worksheet>
                </xsl:when>
                
                <xsl:when test="//app/Accion='GestionAvance'">
                  <Worksheet ss:Name="Estadistica">
                    <Table x:FullColumns="1" x:FullRows="1">
                      <Column ss:AutoFitWidth="0" ss:Width="20"/>
                      
                      <Row ss:Index="1">
                        <Cell ss:StyleID="BordeTitulo">
                          <Data ss:Type="String">#</Data>
                        </Cell>
                        <Cell ss:StyleID="BordeTitulo"><Data ss:Type="String">Institucion</Data></Cell>      
                        <Cell ss:StyleID="BordeTitulo"><Data ss:Type="String">Segmentador</Data></Cell>      
                       <Cell ss:StyleID="BordeTitulo"><Data ss:Type="String">Sede</Data></Cell>      
                      <Cell ss:StyleID="BordeTitulo"><Data ss:Type="String">Carrera</Data></Cell>      
                      <Cell ss:StyleID="BordeTitulo"><Data ss:Type="String">Jornada</Data></Cell>      
                      <Cell ss:StyleID="BordeTitulo"><Data ss:Type="String">Clasificacion Realizada</Data></Cell>      
                      <Cell ss:StyleID="BordeTitulo"><Data ss:Type="String">Pendiente Clasificar</Data></Cell>      
                      <Cell ss:StyleID="BordeTitulo"><Data ss:Type="String">Intervencion Asignada</Data></Cell>      
                      <Cell ss:StyleID="BordeTitulo"><Data ss:Type="String">Pendiente Asignar</Data></Cell>      
                      <Cell ss:StyleID="BordeTitulo"><Data ss:Type="String">Intervencion Finalizada</Data></Cell>      
                      <Cell ss:StyleID="BordeTitulo"><Data ss:Type="String">Intervencion Problema</Data></Cell>
                      </Row>
                      <xsl:for-each select="//app/GestionAvance/Datos/row">
                        <Row>
                          <Cell ss:StyleID="BordeNormal">
                            <Data ss:Type="String">
                              <xsl:value-of select="@Fila"/>
                            </Data>
                          </Cell>
                          <Cell ss:StyleID="BordeNormal"><Data ss:Type="String"><xsl:value-of select="@NombreInstitucion"/></Data></Cell>
                          <Cell ss:StyleID="BordeNormal"><Data ss:Type="String"><xsl:value-of select="@NombreSegmentador"/></Data></Cell>
                          <Cell ss:StyleID="BordeNormal"><Data ss:Type="String"><xsl:value-of select="@NombreSede"/></Data></Cell>
                          <Cell ss:StyleID="BordeNormal"><Data ss:Type="String"><xsl:value-of select="@NombreCarrera"/></Data></Cell>
                          <Cell ss:StyleID="BordeNormal"><Data ss:Type="String"><xsl:value-of select="@NombreJornada"/></Data></Cell>
                          <Cell ss:StyleID="BordeNormal"><Data ss:Type="String"><xsl:value-of select="@ClasificacionRealizada"/></Data></Cell>
                          <Cell ss:StyleID="BordeNormal"><Data ss:Type="String"><xsl:value-of select="@PendienteClasificar"/></Data></Cell>
                          <Cell ss:StyleID="BordeNormal"><Data ss:Type="String"><xsl:value-of select="@IntervencionAsignada"/></Data></Cell>
                          <Cell ss:StyleID="BordeNormal"><Data ss:Type="String"><xsl:value-of select="@PendienteAsignar"/></Data></Cell>
                          <Cell ss:StyleID="BordeNormal"><Data ss:Type="String"><xsl:value-of select="@IntervencionFinalizada"/></Data></Cell>
                          <Cell ss:StyleID="BordeNormal"><Data ss:Type="String"><xsl:value-of select="@IntervencionProblema"/></Data></Cell>
                        </Row>
                      </xsl:for-each>
                    </Table>
                  </Worksheet>
                </xsl:when>

                <xsl:when test="//app/Accion='DetalleRut'">
                  <Worksheet ss:Name="DetalleRutExcel">
                    <Table x:FullColumns="1" x:FullRows="1">
                      <Column ss:AutoFitWidth="0" ss:Width="20"/>
                      <Row ss:Index="1">
                        <Cell ss:StyleID="BordeTitulo">
                          <Data ss:Type="String">NOMBREAREA</Data>
                        </Cell>
                        <Cell ss:StyleID="BordeTitulo">
                          <Data ss:Type="String">CARRERAS</Data>
                        </Cell>
                        <Cell ss:StyleID="BordeTitulo">
                          <Data ss:Type="String">RESPONSABLE</Data>
                        </Cell>
                        <Cell ss:StyleID="BordeTitulo">
                          <Data ss:Type="String">MATRICULADOS</Data>
                        </Cell>
                        <Cell ss:StyleID="BordeTitulo">
                          <Data ss:Type="String">PENDIENTECLASIFICAR</Data>
                        </Cell>
                        <Cell ss:StyleID="BordeTitulo">
                          <Data ss:Type="String">PENDIENTEINTERVENCION</Data>
                        </Cell>
                        <Cell ss:StyleID="BordeTitulo">
                          <Data ss:Type="String">AVANCEINTERVENCION</Data>
                        </Cell>
                        <Cell ss:StyleID="BordeTitulo">
                          <Data ss:Type="String">INSTITUCION</Data>
                        </Cell>
                        <Cell ss:StyleID="BordeTitulo">
                          <Data ss:Type="String">NOMBRECARRERA</Data>
                        </Cell>
                        <Cell ss:StyleID="BordeTitulo">
                          <Data ss:Type="String">JORNADA</Data>
                        </Cell>
                        <Cell ss:StyleID="BordeTitulo">
                          <Data ss:Type="String">MATRICULADOS</Data>
                        </Cell>
                        <Cell ss:StyleID="BordeTitulo">
                          <Data ss:Type="String">NOMBRESEGMENTADOR</Data>
                        </Cell>
                        <Cell ss:StyleID="BordeTitulo">
                          <Data ss:Type="String">PENDIENTECLASIFICAR</Data>
                        </Cell>
                      </Row>
                      <Row>
                        <Cell ss:StyleID="BordeNormal">
                          <Data ss:Type="String">
                            <xsl:value-of select="//app/DetalleRutExcel/Datos/row/@NOMBREAREA"/>
                          </Data>
                        </Cell>
                        <Cell ss:StyleID="BordeNormal">
                          <Data ss:Type="String">
                            <xsl:value-of select="//app/DetalleRutExcel/Datos/row/@CARRERAS"/>
                          </Data>
                        </Cell>
                        <Cell ss:StyleID="BordeNormal">
                          <Data ss:Type="String">
                            <xsl:value-of select="//app/DetalleRutExcel/Datos/row/@RESPONSABLE"/>
                          </Data>
                        </Cell>
                        <Cell ss:StyleID="BordeNormal">
                          <Data ss:Type="String">
                            <xsl:value-of select="//app/DetalleRutExcel/Datos/row/@MATRICULADOS"/>
                          </Data>
                        </Cell>
                        <Cell ss:StyleID="BordeNormal">
                          <Data ss:Type="String">
                            <xsl:value-of select="//app/DetalleRutExcel/Datos/row/@PENDIENTECLASIFICAR"/>
                          </Data>
                        </Cell>
                        <Cell ss:StyleID="BordeNormal">
                          <Data ss:Type="String">
                            <xsl:value-of select="//app/DetalleRutExcel/Datos/row/@PENDIENTEINTERVENCION"/>
                          </Data>
                        </Cell>
                        <Cell ss:StyleID="BordeNormal">
                          <Data ss:Type="String">
                            <xsl:value-of select="//app/DetalleRutExcel/Datos/row/@AVANCEINTERVENCION"/>
                          </Data>
                        </Cell>
                        <Cell ss:StyleID="BordeNormal">
                          <Data ss:Type="String">
                            <xsl:value-of select="//app/DetalleRutExcel/Datos/row1/@INSTITUCION"/>
                          </Data>
                        </Cell>
                        <Cell ss:StyleID="BordeNormal">
                          <Data ss:Type="String">
                            <xsl:value-of select="//app/DetalleRutExcel/Datos/row1/@NOMBRECARRERA"/>
                          </Data>
                        </Cell>
                        <Cell ss:StyleID="BordeNormal">
                          <Data ss:Type="String">
                            <xsl:value-of select="//app/DetalleRutExcel/Datos/row1/@JORNADA"/>
                          </Data>
                        </Cell>
                        <Cell ss:StyleID="BordeNormal">
                          <Data ss:Type="String">
                            <xsl:value-of select="//app/DetalleRutExcel/Datos/row1/@MATRICULADOS"/>
                          </Data>
                        </Cell>
                        <Cell ss:StyleID="BordeNormal">
                          <Data ss:Type="String">
                            <xsl:value-of select="//app/DetalleRutExcel/Datos/row2/@NOMBRESEGMENTADOR"/>
                          </Data>
                        </Cell>
                        <Cell ss:StyleID="BordeNormal">
                          <Data ss:Type="String">
                            <xsl:value-of select="//app/DetalleRutExcel/Datos/row1/@PENDIENTECLASIFICAR"/>
                          </Data>
                        </Cell>
                      </Row>
                      <Row ss:Index="5">
                        <Cell ss:StyleID="BordeTitulo">
                          <Data ss:Type="String">#</Data>
                        </Cell>
                        <Cell ss:StyleID="BordeTitulo">
                          <Data ss:Type="String">RUTALUMNO</Data>
                        </Cell>
                        <Cell ss:StyleID="BordeTitulo">
                          <Data ss:Type="String">APPPATALUMNO</Data>
                        </Cell>
                        <Cell ss:StyleID="BordeTitulo">
                          <Data ss:Type="String">APPMATALUMNO</Data>
                        </Cell>
                        <Cell ss:StyleID="BordeTitulo">
                          <Data ss:Type="String">NOMBREALUMNO</Data>
                        </Cell>
                        <Cell ss:StyleID="BordeTitulo">
                          <Data ss:Type="String">TIPOALUMNO</Data>
                        </Cell>
                        <Cell ss:StyleID="BordeTitulo">
                          <Data ss:Type="String">INDICADORANTERIOR</Data>
                        </Cell>
                        <Cell ss:StyleID="BordeTitulo">
                          <Data ss:Type="String">INDICADORACTUAL</Data>
                        </Cell>
                        <Cell ss:StyleID="BordeTitulo">
                          <Data ss:Type="String">SITUACIONACTUAL</Data>
                        </Cell>
                        <Cell ss:StyleID="BordeTitulo">
                          <Data ss:Type="String">NUEVASITUACION</Data>
                        </Cell>
                      </Row>
                      <xsl:for-each select="//app/DetalleRutExcel/Datos/row3">
                        <Row>
                          <Cell ss:StyleID="BordeNormal">
                            <Data ss:Type="String">
                              <xsl:value-of select="position()" />
                            </Data>
                          </Cell>
                          <Cell ss:StyleID="BordeNormal">
                            <Data ss:Type="String">
                              <xsl:value-of select="@RUTALUMNO"/>
                            </Data>
                          </Cell>
                          <Cell ss:StyleID="BordeNormal">
                            <Data ss:Type="String">
                              <xsl:value-of select="@APPPATALUMNO"/>
                            </Data>
                          </Cell>
                          <Cell ss:StyleID="BordeNormal">
                            <Data ss:Type="String">
                              <xsl:value-of select="@APPMATALUMNO"/>
                            </Data>
                          </Cell>
                          <Cell ss:StyleID="BordeNormal">
                            <Data ss:Type="String">
                              <xsl:value-of select="@NOMBREALUMNO"/>
                            </Data>
                          </Cell>
                          <Cell ss:StyleID="BordeNormal">
                            <Data ss:Type="String">
                              <xsl:value-of select="@TIPOALUMNO"/>
                            </Data>
                          </Cell>
                          <Cell ss:StyleID="BordeNormal">
                            <Data ss:Type="String">
                              <xsl:value-of select="@INDICADORANTERIOR"/>
                            </Data>
                          </Cell>
                          <Cell ss:StyleID="BordeNormal">
                            <Data ss:Type="String">
                              <xsl:value-of select="@INDICADORACTUAL"/>
                            </Data>
                          </Cell>
                          <Cell ss:StyleID="BordeNormal">
                            <Data ss:Type="String">
                              <xsl:value-of select="@SITUACIONACTUAL"/>
                            </Data>
                          </Cell>
                          <Cell ss:StyleID="BordeNormal">
                            <Data ss:Type="String">
                              <xsl:value-of select="@NUEVASITUACION"/>
                            </Data>
                          </Cell>
                        </Row>
                      </xsl:for-each>
                    </Table>
                  </Worksheet>
                </xsl:when>

                <xsl:when test="//app/Accion='DetalleRutInterExcel'">
                  <Worksheet ss:Name="base">
                    <Table x:FullColumns="1" x:FullRows="1">
                      <Column ss:AutoFitWidth="0" ss:Width="20"/>
                      
                      <Row ss:Index="1">
                        <Cell ss:StyleID="BordeTitulo">
                          <Data ss:Type="String">NOMBREAREA</Data>
                        </Cell>
                        <Cell ss:StyleID="BordeTitulo">
                          <Data ss:Type="String">CARRERAS</Data>
                        </Cell>
                        <Cell ss:StyleID="BordeTitulo">
                          <Data ss:Type="String">RESPONSABLE</Data>
                        </Cell>
                        <Cell ss:StyleID="BordeTitulo">
                          <Data ss:Type="String">NOMBRE</Data>
                        </Cell>
                        <Cell ss:StyleID="BordeTitulo">
                          <Data ss:Type="String">MATRICULADOS</Data>
                        </Cell>
                        <Cell ss:StyleID="BordeTitulo">
                          <Data ss:Type="String">PENDIENTECLASIFICAR</Data>
                        </Cell>
                        <Cell ss:StyleID="BordeTitulo">
                          <Data ss:Type="String">PENDIENTEINTERVENCION</Data>
                        </Cell>
                        <Cell ss:StyleID="BordeTitulo">
                          <Data ss:Type="String">AVANCEINTERVENCION</Data>
                        </Cell>
                        <Cell ss:StyleID="BordeTitulo">
                          <Data ss:Type="String">INSTITUCION</Data>
                        </Cell>
                        <Cell ss:StyleID="BordeTitulo">
                          <Data ss:Type="String">NOMBRECARRERA</Data>
                        </Cell>
                        <Cell ss:StyleID="BordeTitulo">
                          <Data ss:Type="String">JORNADA</Data>
                        </Cell>
                        <Cell ss:StyleID="BordeTitulo">
                          <Data ss:Type="String">MATRICULADOS</Data>
                        </Cell>
                        <Cell ss:StyleID="BordeTitulo">
                          <Data ss:Type="String">NOMBRESEGMENTADOR</Data>
                        </Cell>
                        <Cell ss:StyleID="BordeTitulo">
                          <Data ss:Type="String">PENDIENTEINTERVENCION</Data>
                        </Cell>
                      </Row>
                      <Row>
                        <Cell ss:StyleID="BordeNormal">
                          <Data ss:Type="String">
                            <xsl:value-of select="//app/DetalleRutInterExcel/Datos/row/@NOMBREAREA"/>
                          </Data>
                        </Cell>
                        <Cell ss:StyleID="BordeNormal">
                          <Data ss:Type="String">
                            <xsl:value-of select="//app/DetalleRutInterExcel/Datos/row/@CARRERAS"/>
                          </Data>
                        </Cell>
                        <Cell ss:StyleID="BordeNormal">
                          <Data ss:Type="String">
                            <xsl:value-of select="//app/DetalleRutInterExcel/Datos/row/@RESPONSABLE"/>
                          </Data>
                        </Cell>
                        <Cell ss:StyleID="BordeNormal">
                          <Data ss:Type="String">
                            <xsl:value-of select="//app/DetalleRutInterExcel/Datos/row/@NOMBRE"/>
                          </Data>
                        </Cell>
                        <Cell ss:StyleID="BordeNormal">
                          <Data ss:Type="String">
                            <xsl:value-of select="//app/DetalleRutInterExcel/Datos/row/@MATRICULADOS"/>
                          </Data>
                        </Cell>
                        <Cell ss:StyleID="BordeNormal">
                          <Data ss:Type="String">
                            <xsl:value-of select="//app/DetalleRutInterExcel/Datos/row/@PENDIENTECLASIFICAR"/>
                          </Data>
                        </Cell>
                        <Cell ss:StyleID="BordeNormal">
                          <Data ss:Type="String">
                            <xsl:value-of select="//app/DetalleRutInterExcel/Datos/row/@PENDIENTEINTERVENCION"/>
                          </Data>
                        </Cell>
                        <Cell ss:StyleID="BordeNormal">
                          <Data ss:Type="String">
                            <xsl:value-of select="//app/DetalleRutInterExcel/Datos/row/@AVANCEINTERVENCION"/>
                          </Data>
                        </Cell>
                        <Cell ss:StyleID="BordeNormal">
                          <Data ss:Type="String">
                            <xsl:value-of select="//app/DetalleRutInterExcel/Datos/row1/@INSTITUCION"/>
                          </Data>
                        </Cell>
                        <Cell ss:StyleID="BordeNormal">
                          <Data ss:Type="String">
                            <xsl:value-of select="//app/DetalleRutInterExcel/Datos/row1/@NOMBRECARRERA"/>
                          </Data>
                        </Cell>
                        <Cell ss:StyleID="BordeNormal">
                          <Data ss:Type="String">
                            <xsl:value-of select="//app/DetalleRutInterExcel/Datos/row1/@JORNADA"/>
                          </Data>
                        </Cell>
                        <Cell ss:StyleID="BordeNormal">
                          <Data ss:Type="String">
                            <xsl:value-of select="//app/DetalleRutInterExcel/Datos/row1/@MATRICULADOS"/>
                          </Data>
                        </Cell>
                        <Cell ss:StyleID="BordeNormal">
                          <Data ss:Type="String">
                            <xsl:value-of select="//app/DetalleRutInterExcel/Datos/row2/@NOMBRESEGMENTADOR"/>
                          </Data>
                        </Cell>
                        <Cell ss:StyleID="BordeNormal">
                          <Data ss:Type="String">
                            <xsl:value-of select="//app/DetalleRutInterExcel/Datos/row1/@PENDIENTEINTERVENCION"/>
                          </Data>
                        </Cell>
                      </Row>
                      <Row ss:Index="5">
                        <Cell ss:StyleID="BordeTitulo">
                          <Data ss:Type="String">#</Data>
                        </Cell>
                        <Cell ss:StyleID="BordeTitulo">
                          <Data ss:Type="String">RUTALUMNO</Data>
                        </Cell>
                        <Cell ss:StyleID="BordeTitulo">
                          <Data ss:Type="String">APPPATALUMNO</Data>
                        </Cell>
                        <Cell ss:StyleID="BordeTitulo">
                          <Data ss:Type="String">APPMATALUMNO</Data>
                        </Cell>
                        <Cell ss:StyleID="BordeTitulo">
                          <Data ss:Type="String">NOMBREALUMNO</Data>
                        </Cell>
                        <Cell ss:StyleID="BordeTitulo">
                          <Data ss:Type="String">TIPOALUMNO</Data>
                        </Cell>
                        <xsl:for-each select="//DetalleRutInterExcel/Datos/row5">
                          <Cell ss:StyleID="BordeTitulo">
                            <Data ss:Type="String">
                              <xsl:value-of select="@NOMBREINTERVENCION" />
                            </Data>
                          </Cell>
                        </xsl:for-each>
                      </Row>
                      <xsl:for-each select="//app/DetalleRutInterExcel/Datos/row3">
                        <xsl:variable name="SEG_ALUMNO" select="@SEG_ALUMNO" />
                        <Row>
                          <Cell ss:StyleID="BordeNormal">
                            <Data ss:Type="String">
                              <xsl:value-of select="position()" />
                            </Data>
                          </Cell>
                          <Cell ss:StyleID="BordeNormal">
                            <Data ss:Type="String">
                              <xsl:value-of select="@RUTALUMNO"/>
                            </Data>
                          </Cell>
                          <Cell ss:StyleID="BordeNormal">
                            <Data ss:Type="String">
                              <xsl:value-of select="@APPPATALUMNO"/>
                            </Data>
                          </Cell>
                          <Cell ss:StyleID="BordeNormal">
                            <Data ss:Type="String">
                              <xsl:value-of select="@APPMATALUMNO"/>
                            </Data>
                          </Cell>
                          <Cell ss:StyleID="BordeNormal">
                            <Data ss:Type="String">
                              <xsl:value-of select="@NOMBREALUMNO"/>
                            </Data>
                          </Cell>
                          <Cell ss:StyleID="BordeNormal">
                            <Data ss:Type="String">
                              <xsl:value-of select="@TIPOALUMNO"/>
                            </Data>
                          </Cell>
                          <xsl:for-each select="//DetalleRutInterExcel/Datos/row5">
                            <xsl:variable name="IDINTER" select="@INTERVENCION" />
                            <Cell ss:StyleID="BordeNormal">
                              <Data ss:Type="String">
                                <xsl:choose>
                                  <xsl:when test="//DetalleRutInterExcel/Datos/row4[@CI_SEG_ALUMNO=$SEG_ALUMNO and @CI_INTERVENCION=$IDINTER]/@TIPO_ESTADO='INICIAL'">
                                    <xsl:value-of select="//DetalleRutInterExcel/Datos/row4[@CI_SEG_ALUMNO=$SEG_ALUMNO and @CI_INTERVENCION=$IDINTER]/@ESTADO"/>
                                  </xsl:when>
                                  <!--<xsl:otherwise>Asignado</xsl:otherwise>-->
                                </xsl:choose>
                              </Data>
                            </Cell>
                          </xsl:for-each>
                        </Row>
                      </xsl:for-each>
                    </Table>
                  </Worksheet>
                </xsl:when>

                <xsl:when test="//app/Accion='DetalleRutAvanceExcel'">
                  <Worksheet ss:Name="base">
                    <Table x:FullColumns="1" x:FullRows="1">
                      <Column ss:AutoFitWidth="0" ss:Width="20"/>
                      <Row ss:Index="1">
                        <Cell ss:StyleID="BordeTitulo">
                          <Data ss:Type="String">NOMBREAREA</Data>
                        </Cell>
                        <Cell ss:StyleID="BordeTitulo">
                          <Data ss:Type="String">CARRERAS</Data>
                        </Cell>
                        <Cell ss:StyleID="BordeTitulo">
                          <Data ss:Type="String">RESPONSABLE</Data>
                        </Cell>
                        <Cell ss:StyleID="BordeTitulo">
                          <Data ss:Type="String">NOMBRE</Data>
                        </Cell>
                        <Cell ss:StyleID="BordeTitulo">
                          <Data ss:Type="String">MATRICULADOS</Data>
                        </Cell>
                        <Cell ss:StyleID="BordeTitulo">
                          <Data ss:Type="String">PENDIENTECLASIFICAR</Data>
                        </Cell>
                        <Cell ss:StyleID="BordeTitulo">
                          <Data ss:Type="String">PENDIENTEINTERVENCION</Data>
                        </Cell>
                        <Cell ss:StyleID="BordeTitulo">
                          <Data ss:Type="String">AVANCEINTERVENCION</Data>
                        </Cell>
                        <Cell ss:StyleID="BordeTitulo">
                          <Data ss:Type="String">INSTITUCION</Data>
                        </Cell>
                        <Cell ss:StyleID="BordeTitulo">
                          <Data ss:Type="String">NOMBRECARRERA</Data>
                        </Cell>
                        <Cell ss:StyleID="BordeTitulo">
                          <Data ss:Type="String">JORNADA</Data>
                        </Cell>
                        <Cell ss:StyleID="BordeTitulo">
                          <Data ss:Type="String">MATRICULADOS</Data>
                        </Cell>
                        <Cell ss:StyleID="BordeTitulo">
                          <Data ss:Type="String">NOMBRESEGMENTADOR</Data>
                        </Cell>
                        <Cell ss:StyleID="BordeTitulo">
                          <Data ss:Type="String">PENDIENTEINTERVENCION</Data>
                        </Cell>
                      </Row>
                      <Row>
                        <Cell ss:StyleID="BordeNormal">
                          <Data ss:Type="String">
                            <xsl:value-of select="//app/DetalleRutAvanceExcel/Datos/row/@NOMBREAREA"/>
                          </Data>
                        </Cell>
                        <Cell ss:StyleID="BordeNormal">
                          <Data ss:Type="String">
                            <xsl:value-of select="//app/DetalleRutAvanceExcel/Datos/row/@CARRERAS"/>
                          </Data>
                        </Cell>
                        <Cell ss:StyleID="BordeNormal">
                          <Data ss:Type="String">
                            <xsl:value-of select="//app/DetalleRutAvanceExcel/Datos/row/@RESPONSABLE"/>
                          </Data>
                        </Cell>
                        <Cell ss:StyleID="BordeNormal">
                          <Data ss:Type="String">
                            <xsl:value-of select="//app/DetalleRutAvanceExcel/Datos/row/@NOMBRE"/>
                          </Data>
                        </Cell>
                        <Cell ss:StyleID="BordeNormal">
                          <Data ss:Type="String">
                            <xsl:value-of select="//app/DetalleRutAvanceExcel/Datos/row/@MATRICULADOS"/>
                          </Data>
                        </Cell>
                        <Cell ss:StyleID="BordeNormal">
                          <Data ss:Type="String">
                            <xsl:value-of select="//app/DetalleRutAvanceExcel/Datos/row/@PENDIENTECLASIFICAR"/>
                          </Data>
                        </Cell>
                        <Cell ss:StyleID="BordeNormal">
                          <Data ss:Type="String">
                            <xsl:value-of select="//app/DetalleRutAvanceExcel/Datos/row/@PENDIENTEINTERVENCION"/>
                          </Data>
                        </Cell>
                        <Cell ss:StyleID="BordeNormal">
                          <Data ss:Type="String">
                            <xsl:value-of select="//app/DetalleRutAvanceExcel/Datos/row/@AVANCEINTERVENCION"/>
                          </Data>
                        </Cell>
                        <Cell ss:StyleID="BordeNormal">
                          <Data ss:Type="String">
                            <xsl:value-of select="//app/DetalleRutAvanceExcel/Datos/row1/@INSTITUCION"/>
                          </Data>
                        </Cell>
                        <Cell ss:StyleID="BordeNormal">
                          <Data ss:Type="String">
                            <xsl:value-of select="//app/DetalleRutAvanceExcel/Datos/row1/@NOMBRECARRERA"/>
                          </Data>
                        </Cell>
                        <Cell ss:StyleID="BordeNormal">
                          <Data ss:Type="String">
                            <xsl:value-of select="//app/DetalleRutAvanceExcel/Datos/row1/@JORNADA"/>
                          </Data>
                        </Cell>
                        <Cell ss:StyleID="BordeNormal">
                          <Data ss:Type="String">
                            <xsl:value-of select="//app/DetalleRutAvanceExcel/Datos/row1/@MATRICULADOS"/>
                          </Data>
                        </Cell>
                        <Cell ss:StyleID="BordeNormal">
                          <Data ss:Type="String">
                            <xsl:value-of select="//app/DetalleRutAvanceExcel/Datos/row2/@NOMBRESEGMENTADOR"/>
                          </Data>
                        </Cell>
                        <Cell ss:StyleID="BordeNormal">
                          <Data ss:Type="String">
                            <xsl:value-of select="//app/DetalleRutAvanceExcel/Datos/row1/@PENDIENTEINTERVENCION"/>
                          </Data>
                        </Cell>
                      </Row>
                      <Row ss:Index="5">
                        <Cell ss:StyleID="BordeTitulo">
                          <Data ss:Type="String">#</Data>
                        </Cell>
                        <Cell ss:StyleID="BordeTitulo">
                          <Data ss:Type="String">RUTALUMNO</Data>
                        </Cell>
                        <Cell ss:StyleID="BordeTitulo">
                          <Data ss:Type="String">APPPATALUMNO</Data>
                        </Cell>
                        <Cell ss:StyleID="BordeTitulo">
                          <Data ss:Type="String">APPMATALUMNO</Data>
                        </Cell>
                        <Cell ss:StyleID="BordeTitulo">
                          <Data ss:Type="String">NOMBREALUMNO</Data>
                        </Cell>
                        <Cell ss:StyleID="BordeTitulo">
                          <Data ss:Type="String">TIPOALUMNO</Data>
                        </Cell>
                        <xsl:for-each select="//DetalleRutAvanceExcel/Datos/row5">
                          <Cell ss:StyleID="BordeTitulo">
                            <Data ss:Type="String">
                              <xsl:value-of select="@NOMBREINTERVENCION" />
                            </Data>
                          </Cell>
                        </xsl:for-each>
                      </Row>
                      <xsl:for-each select="//app/DetalleRutAvanceExcel/Datos/row3">
                        <xsl:variable name="SEG_ALUMNO" select="@SEG_ALUMNO" />
                        <Row>
                          <Cell ss:StyleID="BordeNormal">
                            <Data ss:Type="String">
                              <xsl:value-of select="position()" />
                            </Data>
                          </Cell>
                          <Cell ss:StyleID="BordeNormal">
                            <Data ss:Type="String">
                              <xsl:value-of select="@RUTALUMNO"/>
                            </Data>
                          </Cell>
                          <Cell ss:StyleID="BordeNormal">
                            <Data ss:Type="String">
                              <xsl:value-of select="@APPPATALUMNO"/>
                            </Data>
                          </Cell>
                          <Cell ss:StyleID="BordeNormal">
                            <Data ss:Type="String">
                              <xsl:value-of select="@APPMATALUMNO"/>
                            </Data>
                          </Cell>
                          <Cell ss:StyleID="BordeNormal">
                            <Data ss:Type="String">
                              <xsl:value-of select="@NOMBREALUMNO"/>
                            </Data>
                          </Cell>
                          <Cell ss:StyleID="BordeNormal">
                            <Data ss:Type="String">
                              <xsl:value-of select="@TIPOALUMNO"/>
                            </Data>
                          </Cell>
                          <xsl:for-each select="//DetalleRutAvanceExcel/Datos/row5">
                            <xsl:variable name="IDINTER" select="@INTERVENCION" />
                            <Cell ss:StyleID="BordeNormal">
                              <Data ss:Type="String">
                                <xsl:choose>
                                  <xsl:when test="//DetalleRutAvanceExcel/Datos/row4[@CI_SEG_ALUMNO=$SEG_ALUMNO and @CI_INTERVENCION=$IDINTER]">
                                    <xsl:value-of select="//DetalleRutAvanceExcel/Datos/row4[@CI_SEG_ALUMNO=$SEG_ALUMNO and @CI_INTERVENCION=$IDINTER]/@ESTADO"/>
                                  </xsl:when>
                                  <!--<xsl:otherwise>Asignado</xsl:otherwise>-->
                                </xsl:choose>
                              </Data>
                            </Cell>
                          </xsl:for-each>
                        </Row>
                      </xsl:for-each>
                    </Table>
                  </Worksheet>
                </xsl:when>
              </xsl:choose>
            </Workbook>
  </xsl:template>
</xsl:stylesheet>
