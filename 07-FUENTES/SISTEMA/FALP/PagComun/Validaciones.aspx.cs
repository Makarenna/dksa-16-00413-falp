﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Xml;
using library.xml;
using library.common;

public partial class Validaciones : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        XmlDocument XmlFinal = null, XmlAuxA = null, XmlAuxB = null, XmlAuxC = null, XmlAuxD = null, XmlAuxE = null;

        try
        {
            //Crear XML Schema
            XmlFinal = XmlHelper.CrearPaginaEsquemaAll(XmlFinal, this);
            string OK = "";

            //DE ACA SACAS LAS  DEL POST, O GET
            string Accion = XmlHelper.strGetNodoTexto(XmlFinal, "//requestFormPost/Accion");

            string Filtros = XmlHelper.strGetNodoTexto(XmlFinal, "//requestFormPost/filtro");

            XmlAuxA = XmlHelper.StpCrearParametros();

            string[] LosFiltros = Filtros.Split('~');

            //for de split Personas
            for (int j = 0; j < LosFiltros.Length; j++)
            {
                if (!LosFiltros[j].Equals(""))
                {
                    string[] MyFiltro = LosFiltros[j].Split('|');
                    XmlHelper.StpAgregarNodoParametros(XmlAuxA, MyFiltro[0], MyFiltro[1]);
                }
            }//FIN_FOR

            switch (Accion)
            {
                case "ValidaLogin":

                    XmlAuxB = XmlHelper.StpCrearParametros();
                    XmlAuxC = XmlHelper.CrearXmlDocumentConGetRow("ValidaLogin", new cBD().EjecutaPA(XmlAuxA.OuterXml, "MSS_S_ValidaLogin"));

                    XmlHelper.UnirArbol(XmlFinal, XmlAuxC, "//app");
                    break;

                case "ValidaInstSede":
                    XmlAuxC = XmlHelper.CrearXmlDocumentConGetRow("ValidaInstSede", new cBD().EjecutaPA(XmlAuxA.OuterXml, "MSS_S_ValidaInstSede"));
                    XmlHelper.UnirArbol(XmlFinal, XmlAuxC, "//app");
                    break;

            }//fin_switch

            XmlHelper.AgregaNodoConTexto(XmlFinal, "//app", "Accion", Accion);
            XmlHelper.appDibujarHTML(XmlFinal, Request.MapPath("Validaciones.xsl"), this);
           
        }
        catch (Exception ex)
        {
            new Log().GuardaLog("Page_Load", ex.Source, ex.Message, ex.StackTrace);
        }
        finally
        {
             //Limpiar Todo
            XmlFinal = null;
            XmlAuxA = XmlFinal;
            XmlAuxB = XmlFinal;
            XmlAuxC = XmlFinal;
            XmlAuxD = XmlFinal;
            XmlAuxE = XmlFinal;
        }
    }
}
