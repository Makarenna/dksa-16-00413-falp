<xsl:stylesheet version="1.0"  xmlns="urn:schemas-microsoft-com:office:spreadsheet"
    xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
 xmlns:msxsl="urn:schemas-microsoft-com:xslt"
 xmlns:user="urn:my-scripts"
 xmlns:o="urn:schemas-microsoft-com:office:office"
 xmlns:x="urn:schemas-microsoft-com:office:excel"
 xmlns:ss="urn:schemas-microsoft-com:office:spreadsheet" >
  <xsl:output encoding="ISO-8859-1" />
  <xsl:template match="/">
    <Workbook xmlns="urn:schemas-microsoft-com:office:spreadsheet"
      xmlns:o="urn:schemas-microsoft-com:office:office"
      xmlns:x="urn:schemas-microsoft-com:office:excel"
      xmlns:ss="urn:schemas-microsoft-com:office:spreadsheet"
      xmlns:html="http://www.w3.org/TR/REC-html40">
      <Styles>
        <Style ss:ID="Default" ss:Name="Normal">
          <Alignment ss:Vertical="Bottom" />
          <Borders />
          <Font />
          <Interior />
          <NumberFormat />
          <Protection />
        </Style>
        <Style ss:ID="LetraCursiva">
          <Font ss:FontName="Calibri" x:Family="Swiss" ss:Size="7" ss:Color="#000000" ss:Italic="1"/>
        </Style>
        <Style ss:ID="Titulo">
          <Alignment ss:Horizontal="Center" ss:Vertical="Bottom"/>
          <Borders>
            <Border ss:Position="Bottom" ss:LineStyle="Continuous" ss:Weight="1"/>
            <Border ss:Position="Left" ss:LineStyle="Continuous" ss:Weight="1"/>
            <Border ss:Position="Right" ss:LineStyle="Continuous" ss:Weight="1"/>
            <Border ss:Position="Top" ss:LineStyle="Continuous" ss:Weight="1"/>
          </Borders>
          <Font ss:FontName="Calibri" x:Family="Swiss" ss:Size="12" ss:Color="#000000" ss:Bold="1"/>
        </Style>
        <Style ss:ID="BordeTitulo">
          <Alignment ss:Horizontal="Center" ss:Vertical="Bottom" ss:WrapText="1"/>
          <Borders>
            <Border ss:Position="Bottom" ss:LineStyle="Continuous" ss:Weight="1"/>
            <Border ss:Position="Left" ss:LineStyle="Continuous" ss:Weight="1"/>
            <Border ss:Position="Right" ss:LineStyle="Continuous" ss:Weight="1"/>
            <Border ss:Position="Top" ss:LineStyle="Continuous" ss:Weight="1"/>
          </Borders>
          <Font ss:FontName="Calibri" x:Family="Swiss" ss:Size="10" ss:Color="#FFFFFF" ss:Bold="1"/>
          <Interior ss:Color="#005548" ss:Pattern="Solid"/>
        </Style>
        <Style ss:ID="BordeNormal">
          <Alignment ss:Horizontal="Left" ss:Vertical="Center" ss:WrapText="1"/>
          <Borders>
            <Border ss:Position="Bottom" ss:LineStyle="Continuous" ss:Weight="1"/>
            <Border ss:Position="Left" ss:LineStyle="Continuous" ss:Weight="1"/>
            <Border ss:Position="Right" ss:LineStyle="Continuous" ss:Weight="1"/>
            <Border ss:Position="Top" ss:LineStyle="Continuous" ss:Weight="1"/>
          </Borders>
          <Font ss:FontName="Calibri" x:Family="Swiss" ss:Size="10" ss:Color="#000000"/>
        </Style>
      </Styles>
      <Worksheet ss:Name="base">
        <Table x:FullColumns="1" x:FullRows="1">
          <Column ss:AutoFitWidth="0" ss:Width="100"/>
          <Row ss:Index="1">
            <Cell ss:StyleID="BordeTitulo">
              <Data ss:Type="String">#</Data>
            </Cell>
            <Cell ss:StyleID="BordeTitulo">
              <Data ss:Type="String">Folio</Data>
            </Cell>
            <Cell ss:StyleID="BordeTitulo">
              <Data ss:Type="String">Etapa Actual</Data>
            </Cell>
            <Cell ss:StyleID="BordeTitulo">
              <Data ss:Type="String">Fecha Envío Adm</Data>
            </Cell>
            <Cell ss:StyleID="BordeTitulo">
              <Data ss:Type="String">Fecha Envío a C.M</Data>
            </Cell>
            <Cell ss:StyleID="BordeTitulo">
              <Data ss:Type="String">Fecha Resolución C.M</Data>
            </Cell>
            <Cell ss:StyleID="BordeTitulo">
              <Data ss:Type="String">Fecha Recepción a Registro</Data>
            </Cell>
            <Cell ss:StyleID="BordeTitulo">
              <Data ss:Type="String">Fecha Resolución Registro</Data>
            </Cell>
          </Row>
          <xsl:for-each select="//app/DETALLE_PRODUCCION_REG/Datos/row">
            <Row>
              <Cell ss:StyleID="BordeNormal">
                <Data ss:Type="String">
                  <xsl:value-of select="@FILA"/>
                </Data>
              </Cell>
              <Cell ss:StyleID="BordeNormal">
                <Data ss:Type="String">
                  <xsl:value-of select="@FOLIO"/>
                </Data>
              </Cell>
              <Cell ss:StyleID="BordeNormal">
                <Data ss:Type="String">
                  <xsl:value-of select="@ETAPA_ACTUAL"/>
                </Data>
              </Cell>
              <Cell ss:StyleID="BordeNormal">
                <Data ss:Type="String">
                  <xsl:value-of select="@FECHA_ENVIO_ADM"/>
                </Data>
              </Cell>
              <Cell ss:StyleID="BordeNormal">
                <Data ss:Type="String">
                  <xsl:value-of select="@FECHA_ENVIO_CM"/>
                </Data>
              </Cell>
              <Cell ss:StyleID="BordeNormal">
                <Data ss:Type="String">
                  <xsl:value-of select="@FECHA_ENVIO_REG"/>
                </Data>
              </Cell>
              <Cell ss:StyleID="BordeNormal">
                <Data ss:Type="String">
                  <xsl:value-of select="@FECHA_RECEPCION_REG"/>
                </Data>
              </Cell>
              <Cell ss:StyleID="BordeNormal">
                <Data ss:Type="String">
                  <xsl:value-of select="@FECHA_RESOLUCION_REG"/>
                </Data>
              </Cell>
            </Row>
          </xsl:for-each>
        </Table>
      </Worksheet>
    </Workbook>
  </xsl:template>
</xsl:stylesheet>
