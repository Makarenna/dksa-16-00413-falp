﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Xml;
using library.xml;
using library.common;

public partial class GrillaPaginada : PaginaBase//System.Web.UI.Page
{
    public string ConfiguracionPaginacion { get; set; }
    protected void Page_Load(object sender, EventArgs e)
    {
        XmlDocument XmlFinal = null, XmlParametrosSql = null;
        try
        {
            XmlFinal = XmlHelper.CrearPaginaEsquemaAll(XmlFinal, this);

            string Accion = XmlHelper.strGetNodoTexto(XmlFinal, "//requestFormPost/Accion");
            string AliasForm = XmlHelper.strGetNodoTexto(XmlFinal, "//requestFormPost/AliasForm");

            string TotalRegistros = "0";
            string RegPag = "";
            string Pag = "";

            if (Accion.Equals(string.Empty))
                return;

            XmlParametrosSql = XmlHelper.StpCrearParametros();
            XmlHelper.StpAgregarNodoParametros(XmlParametrosSql, "sesID", sesID);
            XmlHelper.StpAgregarNodoParametros(XmlParametrosSql, "usuID", usuID);
            XmlHelper.StpAgregarNodoParametros(XmlParametrosSql, "perID", perID);
            new PaginaBase().SerializaJson(ref XmlParametrosSql, AliasForm);

            string SP = "PCK_CONV_DKERNEL.DK_GRILLA_" + Accion;
            string Datos = "//" + Accion + "/Datos/row";
            string Paginacion = "Paginacion" + Accion;

            string RegPagIniConf = "";

            ConfiguracionPaginacion = System.IO.File.ReadAllText(Request.MapPath("Paginacion.xml"));
            XmlDocument _XmlPaginacion = XmlHelper.StpCrearParametros();
            _XmlPaginacion.LoadXml(ConfiguracionPaginacion);
            XmlHelper.UnirArbol(XmlFinal, _XmlPaginacion, "//app");
            RegPagIniConf = XmlHelper.strGetNodoGetAttributo(XmlFinal, "//app/parametros/Paginacion/RegPag", "valor");

            Pag = XmlHelper.strGetNodoTexto(XmlParametrosSql, "//parametros/PageNumber");
            RegPag = XmlHelper.strGetNodoTexto(XmlParametrosSql, "//parametros/PageSize");

            if (Pag.Equals(""))
            {
                Pag = "1";
                XmlHelper.StpAgregarNodoParametros(XmlParametrosSql, "PageNumber", Pag);
            }

            if (RegPag.Equals(""))
            {
                RegPag = RegPagIniConf;
                XmlHelper.StpAgregarNodoParametros(XmlParametrosSql, "PageSize", RegPag);
            }

            XmlDocument _DatosGrilla = XmlHelper.CrearXmlDocumentConGetRow(Accion, new cBD().Ejecuta_PA_Oracle_Online(XmlParametrosSql.OuterXml, SP));
            XmlHelper.UnirArbol(XmlFinal, _DatosGrilla, "//app");

            TotalRegistros = XmlHelper.strGetNodoGetAttributo(_DatosGrilla, Datos, "TOTALREGISTROS");

            GeneraMultiplePaginacion(Pag, TotalRegistros, XmlFinal, RegPag, Paginacion);

            XmlHelper.AgregaNodoConTexto(XmlFinal, "//app", "RegPag", RegPag);
            XmlHelper.AgregaNodoConTexto(XmlFinal, "//app", "perID", perID);
            XmlHelper.AgregaNodoConTexto(XmlFinal, "//app", "Accion", Accion);
            XmlHelper.appDibujarHTML(XmlFinal, Request.MapPath(string.Format("GrillaPaginada_{0}.xsl",Accion)), this);

        }
        catch (Exception ex)
        {
            new Log().GuardaLog("GrillaPaginada:Page_Load", ex.Source, ex.Message, ex.StackTrace);
        }
        finally
        {
            XmlFinal = null;
        }
    }
    public static XmlDocument GeneraMultiplePaginacion(string Pagina, string TotalDeRegistros, XmlDocument XmlFinal, string RegPag, string Multiple)
    {

        int Pag, TotalRegs = 0, Resto, RegFirst = 0, RegLast = 0, i = 0, PagSgte = 0, PagAnt = 0, NumPaginas = 0, PagSgteSgte = 0, PagAntAnt = 0;

        int CTE_REGS_POR_PAG = int.Parse(RegPag);


        if (Pagina == string.Empty)
        {
            Pag = 1;
        }
        else
        {
            try
            {
                Pag = int.Parse(Pagina);
            }
            catch
            {
                Pag = 1;
            }

            if (Pag == 0) Pag = 1;
        }

        //si es negativo traerlo al primero.
        if (Pag < 0) Pag = 1;

        if (TotalDeRegistros == string.Empty)
        {
            TotalRegs = 0;
        }
        else
        {
            TotalRegs = int.Parse(TotalDeRegistros);
        }

        if (TotalRegs != 0)
        {
            Resto = TotalRegs % CTE_REGS_POR_PAG;

            if (Resto == 0)
            {
                NumPaginas = TotalRegs / CTE_REGS_POR_PAG;
            }
            else
            {
                NumPaginas = TotalRegs / CTE_REGS_POR_PAG + 1;
            }
        }

        //Pagina actual mayor que el total. mandarlo al ultimo
        if (Pag > NumPaginas)
        {
            Pag = NumPaginas;
        }

        RegFirst = Pag * CTE_REGS_POR_PAG - CTE_REGS_POR_PAG + 1;
        RegLast = Pag * CTE_REGS_POR_PAG;

        if (RegLast > TotalRegs)
        {
            RegLast = TotalRegs;
        }

        if (Pag < NumPaginas)
        {
            PagSgte = Pag + 1;
        }
        else
        {
            PagSgte = 0;
        }

        if (PagSgte < NumPaginas && PagSgte != 0)
        {
            PagSgteSgte = PagSgte + 1;
        }
        else
        {
            PagSgteSgte = 0;
        }


        if (Pag > 1)
        {
            PagAnt = Pag - 1;
        }
        else
        {
            PagAnt = 0;
        }

        if (PagAnt > 1 && PagAnt != 0)
        {
            PagAntAnt = PagAnt - 1;
        }
        else
        {
            PagAntAnt = 0;
        }


        string NmRoot, NmBase;

        NmRoot = "M_" + Multiple;
        NmBase = "raiz/" + NmRoot;

        XmlFinal = XmlHelper.AgregaNodo(XmlFinal, "raiz", NmRoot);
        XmlFinal = XmlHelper.AgregaNodoConTexto(XmlFinal, NmBase, "NumPaginas", NumPaginas.ToString());
        XmlFinal = XmlHelper.AgregaNodoConTexto(XmlFinal, NmBase, "PagSgte", PagSgte.ToString());
        XmlFinal = XmlHelper.AgregaNodoConTexto(XmlFinal, NmBase, "PagAnt", PagAnt.ToString());
        XmlFinal = XmlHelper.AgregaNodoConTexto(XmlFinal, NmBase, "PagAct", Pag.ToString());
        XmlFinal = XmlHelper.AgregaNodoConTexto(XmlFinal, NmBase, "PagSgteSgte", PagSgteSgte.ToString());
        XmlFinal = XmlHelper.AgregaNodoConTexto(XmlFinal, NmBase, "PagAntAnt", PagAntAnt.ToString());
        XmlFinal = XmlHelper.AgregaNodoConTexto(XmlFinal, NmBase, "RegFirst", RegFirst.ToString());
        XmlFinal = XmlHelper.AgregaNodoConTexto(XmlFinal, NmBase, "RegLast", RegLast.ToString());
        XmlFinal = XmlHelper.AgregaNodoConTexto(XmlFinal, NmBase, "Total", TotalRegs.ToString());

        //if (cnfHelper.GetConfigAppVariable("PagTxt") != null)
        //{
        //    string PagTxt = cnfHelper.GetConfigAppVariable("PagTxt").Replace("{Total}", TotalRegs.ToString()).Replace("{PagAct}", Pag.ToString()).Replace("{NumPaginas}", NumPaginas.ToString());
        //    XmlFinal = AgregaNodoConTexto(XmlFinal, NmBase, "PagTxt", PagTxt);
        //}

        XmlFinal = XmlHelper.AgregaNodo(XmlFinal, NmBase, "Paginas");

        for (i = 1; i <= NumPaginas; i++)
        {
            XmlFinal = XmlHelper.AgregaNodoConTexto(XmlFinal, NmBase + "/Paginas", "NumPag", i.ToString());
        }

        return XmlFinal;

    }
}
