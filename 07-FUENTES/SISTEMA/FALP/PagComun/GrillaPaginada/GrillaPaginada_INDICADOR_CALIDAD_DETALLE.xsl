<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform"  >
  <xsl:output method="html" version="1.0" encoding="utf-8" indent="yes" omit-xml-declaration="yes" standalone="yes" doctype-public="-//W3C//DTD XHTML 1.1//EN" doctype-system="http://www.w3.org/TR/xhtml11/DTD/xhtml11.dtd" ></xsl:output>
  <xsl:include href="GrillaPaginada.xsl"/>
  <xsl:template match="/">
    <div class="row">
      <div class="col-lg-12">
        <div class="panel panel-default">
            <div class="panel-body">
              <div class="dataTable_wrapper">
                  <table width="100%" class="table table-striped table-bordered table-hover" id="dataTables-example">
                    <thead>
                        <tr>
                          <th>Equipo</th>
                          <th>Enviados a Adm</th>
                          <th>Enviados a CM</th>
                          <th>Enviados a Registro</th>
                          <th>Aprobados</th>
                          <th>Listado</th>
                        </tr>
                    </thead>
                    <tbody>
                        <tr class="odd gradeX">
                          <td class="text-center"><a id="collapseOne" onclick="ir(this,'collapseOne');">Equipo 1</a></td>
                          <td class="text-center">220</td>
                          <td class="text-center">205</td>
                          <td class="text-center">160</td>
                          <td class="text-center">135</td>
                          <td class="text-center"><a href="listado.xlsx" target="blank"><p class="fa fa-download"></p></a></td>
                        </tr>
                        <tr class="odd gradeX">
                          <td class="text-center"><a id="collapseOne" onclick="ir(this,'collapseOne');">Equipo 2</a></td>
                          <td class="text-center">220</td>
                          <td class="text-center">205</td>
                          <td class="text-center">160</td>
                          <td class="text-center">135</td>
                          <td class="text-center"><a href="listado.xlsx" target="blank"><p class="fa fa-download"></p></a></td>
                        </tr>
                        <tr class="odd gradeX">
                          <td class="text-center"><a id="collapseOne" onclick="ir(this,'collapseOne');">Equipo 3</a></td>
                          <td class="text-center">220</td>
                          <td class="text-center">205</td>
                          <td class="text-center">160</td>
                          <td class="text-center">135</td>
                          <td class="text-center"><a href="listado.xlsx" target="blank"><p class="fa fa-download"></p></a></td>
                        </tr>
                        <tr class="odd gradeX">
                          <td class="text-center"><a id="collapseOne" onclick="ir(this,'collapseOne');">Equipo 4</a></td>
                          <td class="text-center">220</td>
                          <td class="text-center">205</td>
                          <td class="text-center">160</td>
                          <td class="text-center">135</td>
                          <td class="text-center"><a href="listado.xlsx" target="blank"><p class="fa fa-download"></p></a></td>
                        </tr>
                        <tr class="odd gradeX">
                          <td class="text-center"><a id="collapseOne" onclick="ir(this,'collapseOne');">Equipo 5</a></td>
                          <td class="text-center">220</td>
                          <td class="text-center">205</td>
                          <td class="text-center">160</td>
                          <td class="text-center">135</td>
                          <td class="text-center"><a href="listado.xlsx" target="blank"><p class="fa fa-download"></p></a></td>
                        </tr>
                        <tr class="odd gradeX">
                          <td class="text-center"><a id="collapseOne" onclick="ir(this,'collapseOne');">Equipo 6</a></td>
                          <td class="text-center">220</td>
                          <td class="text-center">205</td>
                          <td class="text-center">160</td>
                          <td class="text-center">135</td>
                          <td class="text-center"><a href="listado.xlsx" target="blank"><p class="fa fa-download"></p></a></td>
                        </tr>
                        <tr class="odd gradeX">
                          <td class="text-center"><a id="collapseOne" onclick="ir(this,'collapseOne');">Equipo 7</a></td>
                          <td class="text-center">220</td>
                          <td class="text-center">205</td>
                          <td class="text-center">160</td>
                          <td class="text-center">135</td>
                          <td class="text-center"><a href="listado.xlsx" target="blank"><p class="fa fa-download"></p></a></td>
                        </tr>
                        <tr class="odd gradeX">
                          <td class="text-center"><a id="collapseOne" onclick="ir(this,'collapseOne');">Equipo 8</a></td>
                          <td class="text-center">220</td>
                          <td class="text-center">205</td>
                          <td class="text-center">160</td>
                          <td class="text-center">135</td>
                          <td class="text-center"><a href="listado.xlsx" target="blank"><p class="fa fa-download"></p></a></td>
                        </tr>
                        <tr class="odd gradeX">
                          <td class="text-center"><a id="collapseOne" onclick="ir(this,'collapseOne');">Equipo 9</a></td>
                          <td class="text-center">220</td>
                          <td class="text-center">205</td>
                          <td class="text-center">160</td>
                          <td class="text-center">135</td>
                          <td class="text-center"><a href="listado.xlsx" target="blank"><p class="fa fa-download"></p></a></td>
                        </tr>
                        <tr class="odd gradeX">
                          <td class="text-center"><a id="collapseOne" onclick="ir(this,'collapseOne');">Equipo 10</a></td>
                          <td class="text-center">220</td>
                          <td class="text-center">205</td>
                          <td class="text-center">160</td>
                          <td class="text-center">135</td>
                          <td class="text-center"><a href="listado.xlsx" target="blank"><p class="fa fa-download"></p></a></td>
                        </tr>
                        <tr class="odd gradeX">
                          <td class="text-center"><a id="collapseOne" onclick="ir(this,'collapseOne');">Equipo 11</a></td>
                          <td class="text-center">220</td>
                          <td class="text-center">205</td>
                          <td class="text-center">160</td>
                          <td class="text-center">135</td>
                          <td class="text-center"><a href="listado.xlsx" target="blank"><p class="fa fa-download"></p></a></td>
                        </tr>
                        <tr class="odd gradeX">
                          <td class="text-center"><a id="collapseOne" onclick="ir(this,'collapseOne');">Equipo 12</a></td>
                          <td class="text-center">220</td>
                          <td class="text-center">205</td>
                          <td class="text-center">160</td>
                          <td class="text-center">135</td>
                          <td class="text-center"><a href="listado.xlsx" target="blank"><p class="fa fa-download"></p></a></td>
                        </tr>
                        <tr class="odd gradeX">
                          <td class="text-center"><a id="collapseOne" onclick="ir(this,'collapseOne');">Equipo 13</a></td>
                          <td class="text-center">220</td>
                          <td class="text-center">205</td>
                          <td class="text-center">160</td>
                          <td class="text-center">135</td>
                          <td class="text-center"><a href="listado.xlsx" target="blank"><p class="fa fa-download"></p></a></td>
                        </tr>
                        <tr class="odd gradeX">
                          <td class="text-center"><a id="collapseOne" onclick="ir(this,'collapseOne');">Equipo 14</a></td>
                          <td class="text-center">220</td>
                          <td class="text-center">205</td>
                          <td class="text-center">160</td>
                          <td class="text-center">135</td>
                          <td class="text-center"><a href="listado.xlsx" target="blank"><p class="fa fa-download"></p></a></td>
                        </tr>
                        <tr class="odd gradeX">
                          <td class="text-center"><a id="collapseOne" onclick="ir(this,'collapseOne');">Equipo 15</a></td>
                          <td class="text-center">220</td>
                          <td class="text-center">205</td>
                          <td class="text-center">160</td>
                          <td class="text-center">135</td>
                          <td class="text-center"><a href="listado.xlsx" target="blank"><p class="fa fa-download"></p></a></td>
                        </tr>
                        <tr class="odd gradeX">
                          <td class="text-center"><a id="collapseOne" onclick="ir(this,'collapseOne');">Equipo 16</a></td>
                          <td class="text-center">220</td>
                          <td class="text-center">205</td>
                          <td class="text-center">160</td>
                          <td class="text-center">135</td>
                          <td class="text-center"><a href="listado.xlsx" target="blank"><p class="fa fa-download"></p></a></td>
                        </tr>
                        <tr class="odd gradeX">
                          <td class="text-center"><a id="collapseOne" onclick="ir(this,'collapseOne');">Equipo 17</a></td>
                          <td class="text-center">220</td>
                          <td class="text-center">205</td>
                          <td class="text-center">160</td>
                          <td class="text-center">135</td>
                          <td class="text-center"><a href="listado.xlsx" target="blank"><p class="fa fa-download"></p></a></td>
                        </tr>
                        <tr class="odd gradeX">
                          <td class="text-center"><a id="collapseOne" onclick="ir(this,'collapseOne');">Equipo 18</a></td>
                          <td class="text-center">220</td>
                          <td class="text-center">205</td>
                          <td class="text-center">160</td>
                          <td class="text-center">135</td>
                          <td class="text-center"><a href="listado.xlsx" target="blank"><p class="fa fa-download"></p></a></td>
                        </tr>
                        <tr class="odd gradeX" style="background-color: #205081; color: #FFF; ">
                          <td class="text-center">TOTAL</td>
                          <td class="text-center">3.960</td>
                          <td class="text-center">3.690</td>
                          <td class="text-center">2.880</td>
                          <td class="text-center">2.430</td>
                          <td class="text-center"></td>
                        </tr>
                    </tbody>
                  </table>
                  <div class="row">
                    <div class="col-lg-12 text-center">
                        <div class="pull-left">
                          <!-- <div class="dropdown"> -->
                          <span>Registros por página</span>
                          <button class="btn btn-default dropdown-toggle" type="button" id="dropdownMenu1" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true">
                          18
                          <span class="caret"></span>
                          </button>
                          <ul class="dropdown-menu" aria-labelledby="dropdownMenu1">
                              <li><a href="#">18</a></li>
                              <li><a href="#">36</a></li>
                              <li><a href="#">54</a></li>
                          </ul>
                          <!-- </div> -->
                        </div>
                        <div class="btn-group pull-right">
                          <button type="button" class="btn btn-default"><span class="glyphicon glyphicon-menu-left" aria-hidden="true"></span></button>
                          <button type="button" class="btn" style="background-color: #205081; color: #FFF;">1</button>
                          <button type="button" class="btn btn-default">2</button>
                          <button type="button" class="btn btn-default">3</button>
                          <button type="button" class="btn btn-default"><span class="glyphicon glyphicon-menu-right" aria-hidden="true"></span></button>
                        </div>
                    </div>
                  </div>
              </div>
            </div>
        </div>
      </div>
  </div>
  </xsl:template>
</xsl:stylesheet>
