<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform"  >
  <xsl:output method="html" version="1.0" encoding="utf-8" indent="yes" omit-xml-declaration="yes" standalone="yes" doctype-public="-//W3C//DTD XHTML 1.1//EN" doctype-system="http://www.w3.org/TR/xhtml11/DTD/xhtml11.dtd" ></xsl:output>
  <xsl:include href="GrillaPaginada.xsl"/>
  <xsl:template match="/">
    <div class="row">
      <div class="col-lg-12 col-md-12 col-xs-12">
        <div class="panel panel-default">
            <div class="panel-body">
              <div class="dataTable_wrapper">
                  <table width="100%" class="table table-striped table-bordered table-hover" id="dataTables-example">
                    <thead>
                        <tr>
                          <th>Coordinador</th>
                          <th>Enviados a Adm</th>
                          <th>Enviados a CM</th>
                          <th>Enviados a Registro</th>
                          <th>Aprobados</th>
                          <th>Listado</th>
                        </tr>
                    </thead>
                    <tbody>
                      <xsl:for-each select="//app/DETALLE_PRODUCCION/Datos/row">
                         <tr class="odd gradeX">
                          <td class="text-center"><xsl:value-of select="@COOR" />-<xsl:value-of select="@NOMBRE_COOR"/></td>
                          <td class="text-center"><xsl:value-of select="@ADM"/></td>
                          <td class="text-center">
                            <a  class="">
                              <xsl:attribute name="onclick">DetalleProduccionCM('<xsl:value-of select="@COOR" />');
                              </xsl:attribute>
                              <xsl:value-of select="@CM"/>
                            </a>
                          </td>
                          <td class="text-center">
                            <a  class="">
                              <xsl:attribute name="onclick">DetalleProduccionREG('<xsl:value-of select="@COOR" />');
                              </xsl:attribute>
                              <xsl:value-of select="@REGISTRO"/>
                            </a>
                          </td>
                          <td class="text-center"><xsl:value-of select="@APROBADOS"/></td>
                          <td class="text-center">
                            <a>
                            <xsl:attribute name="onclick">ExcelDetalleProduccion(this,'<xsl:value-of select="@COOR" />','<xsl:value-of select="@NOMBREEXCEL" />');
                            </xsl:attribute><p class="fa fa-download"></p></a></td>
                        </tr>
                      </xsl:for-each>
                      <xsl:for-each select="//app/DETALLE_PRODUCCION/Datos/row1">
                        <tr class="odd gradeX" style="background-color: #205081; color: #FFF; ">
                          <td class="text-center">TOTAL</td>
                          <td class="text-center"><xsl:value-of select="@T_ADM"/></td>
                          <td class="text-center"><xsl:value-of select="@T_CM"/></td>
                          <td class="text-center"><xsl:value-of select="@T_REGISTRO"/></td>
                          <td class="text-center"><xsl:value-of select="@T_APROBADOS"/></td>
                          <td class="text-center"></td>
                        </tr>
                       </xsl:for-each>
                    </tbody>
                  </table>
                  <div class="row">
                    <div class="col-lg-12 text-center">
                      <div class="col-sm-2 pull-left">
                        <xsl:call-template name="DibujaRegistrosPorPagina">
                          <xsl:with-param name="tipo" select="//app/Accion" />
                        </xsl:call-template>
                      </div>
                      <div class="col-sm-6 text-center">
                        <button type="button" class="btn" style="background-color: #205081; color: #FFF;" onclick="javascript:volver();">
                          <i class="fa fa-reply" aria-hidden="true"></i> Volver
                        </button>
                      </div>
                      <div class="col-sm-4 btn-group pull-right">
                        <xsl:call-template name="DibujaPaginacion">
                          <xsl:with-param name="tipo" select="//app/Accion" />
                        </xsl:call-template>
                      </div>
                    </div>
                  </div>
              </div>
            </div>
        </div>
      </div>
  </div>
  </xsl:template>
</xsl:stylesheet>
