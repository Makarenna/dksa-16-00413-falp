﻿<?xml version="1.0" encoding="utf-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
    xmlns:msxsl="urn:schemas-microsoft-com:xslt" exclude-result-prefixes="msxsl"
>
    <xsl:output method="xml" indent="yes"/>
  <xsl:template name="DibujaRegistrosPorPagina">
    <xsl:param name="tipo"/>
    <div>
      <input name="Pag" id="Pag" type="hidden" alias="PageNumber" class="form-control" Tipo="Recorrer">
        <xsl:attribute name="name">PagNumber<xsl:value-of select="$tipo"/>
        </xsl:attribute>
        <xsl:attribute name="id">PagNumber<xsl:value-of select="$tipo"/>
        </xsl:attribute>
      </input>
      <input name="ColOrder" id="ColOrder" type="hidden" alias="ColOrder" class="form-control" Tipo="Recorrer">
        <xsl:attribute name="name">ColOrder<xsl:value-of select="$tipo"/>
        </xsl:attribute>
        <xsl:attribute name="id">ColOrder<xsl:value-of select="$tipo"/>
        </xsl:attribute>
      </input>
      <input name="Ordenamiento" id="Ordenamiento" type="hidden" alias="Ordenamiento" class="form-control" Tipo="Recorrer">
        <xsl:attribute name="name">
          Ordenamiento<xsl:value-of select="$tipo"/>
        </xsl:attribute>
        <xsl:attribute name="id">
          Ordenamiento<xsl:value-of select="$tipo"/>
        </xsl:attribute>
      </input>
    </div>
    <!--<div class="row">
      <div class="col-sm-12">-->
        <div id="example-table_length" class="dataTables_length">
          <label>
            Registros por página
            <select size="1" name="RegPag" id="RegPag" class="form-control input-sm" alias="PageSize" onchange="ChangeFalso();" aria-controls="example-table">
              <xsl:attribute name="onchange">ChangeRegPag(this,'<xsl:value-of select="$tipo"/>');
              </xsl:attribute>
              <xsl:attribute name="name">PagSize<xsl:value-of select="$tipo"/>
              </xsl:attribute>
              <xsl:attribute name="id">PagSize<xsl:value-of select="$tipo"/>
              </xsl:attribute>
              <xsl:for-each select="//app/parametros/Paginacion/RegPag">
                <option>
                  <xsl:attribute name="value">
                    <xsl:value-of select="@valor"/>
                  </xsl:attribute>
                  <xsl:if test="//app/RegPag=@valor">
                    <xsl:attribute name="selected">selected</xsl:attribute>
                  </xsl:if>
                  <xsl:value-of select="@valor"/>
                </option>
              </xsl:for-each>
            </select>
          </label>
        </div>
      <!--</div>
    </div>-->
  </xsl:template>
  <xsl:template name="DibujaPaginacion">
    <xsl:param name="tipo"/>
    <xsl:variable name="V1">M_Paginacion<xsl:value-of select="$tipo"/></xsl:variable>
    <xsl:variable name="MyPaginacion" select="//raiz/*[local-name() =  $V1]" />
    <!--<div class="row">
      <div class="col-sm-12">-->
        <xsl:if test="$MyPaginacion/NumPaginas > 1">
          <div class="dataTables_paginate paging_simple_numbers" id="tabla-casos_paginate">
            <ul class="pagination">
              <li class="paginate_button previous disabled" aria-controls="tabla-casos" tabindex="0" id="tabla-casos_previous">
                <xsl:attribute name="class">
                  <xsl:choose>
                    <xsl:when test="$MyPaginacion/PagAnt[.!=0]">paginate_button previous</xsl:when>
                    <xsl:otherwise>paginate_button previous disabled</xsl:otherwise>
                  </xsl:choose>
                </xsl:attribute>
                <a href="#">
                  <xsl:attribute name="href">
                    <xsl:choose>
                      <xsl:when test="$MyPaginacion/PagAnt[.!=0]">
                        javascript:IrAPag('<xsl:value-of select="$MyPaginacion/PagAnt"/>','<xsl:value-of select="$tipo"/>');
                      </xsl:when>
                      <xsl:otherwise>#</xsl:otherwise>
                    </xsl:choose>
                  </xsl:attribute>
                  <span class="glyphicon glyphicon-menu-left" aria-hidden="true"></span>
                </a>
              </li>
              <xsl:if test="$MyPaginacion/PagAntAnt[.!=0]">
                <li class="paginate_button" aria-controls="tabla-casos" tabindex="0">
                  <a href="#">
                    <xsl:attribute name="href">
                      javascript:IrAPag('<xsl:value-of select="$MyPaginacion/PagAntAnt"/>','<xsl:value-of select="$tipo"/>');
                    </xsl:attribute>
                    <xsl:value-of select="$MyPaginacion/PagAntAnt" />
                  </a>
                </li>
              </xsl:if>
              <xsl:if test="$MyPaginacion/PagAnt[.!=0]">
                <li class="paginate_button " aria-controls="tabla-casos" tabindex="0">
                  <a href="#">
                    <xsl:attribute name="href">
                      javascript:IrAPag('<xsl:value-of select="$MyPaginacion/PagAnt"/>','<xsl:value-of select="$tipo"/>');
                    </xsl:attribute>
                    <xsl:value-of select="$MyPaginacion/PagAnt" />
                  </a>
                </li>
              </xsl:if>
              <li class="paginate_button active" aria-controls="tabla-casos" tabindex="0">
                <a href="#">
                  <xsl:value-of select="$MyPaginacion/PagAct" />
                </a>
              </li>
              <xsl:if test="$MyPaginacion/PagSgte[.!=0]">
                <li class="paginate_button " aria-controls="tabla-casos" tabindex="0">
                  <a href="#">
                    <xsl:attribute name="href">
                      javascript:IrAPag('<xsl:value-of select="$MyPaginacion/PagSgte"/>','<xsl:value-of select="$tipo"/>');
                    </xsl:attribute>
                    <xsl:value-of select="$MyPaginacion/PagSgte" />
                  </a>
                </li>
              </xsl:if>
              <xsl:if test="$MyPaginacion/PagSgteSgte[.!=0]">
                <li class="paginate_button " aria-controls="tabla-casos" tabindex="0">
                  <a href="#">
                    <xsl:attribute name="href">
                      javascript:IrAPag('<xsl:value-of select="$MyPaginacion/PagSgteSgte"/>','<xsl:value-of select="$tipo"/>');
                    </xsl:attribute>
                    <xsl:value-of select="$MyPaginacion/PagSgteSgte" />
                  </a>
                </li>
              </xsl:if>
              <li class="paginate_button next" aria-controls="tabla-casos" tabindex="0" id="tabla-casos_next">
                <xsl:attribute name="class">
                  <xsl:choose>
                    <xsl:when test="$MyPaginacion/PagSgte[.!=0]">paginate_button next</xsl:when>
                    <xsl:otherwise>paginate_button next disabled</xsl:otherwise>
                  </xsl:choose>
                </xsl:attribute>
                <a href="#">
                  <xsl:attribute name="href">
                    <xsl:choose>
                      <xsl:when test="$MyPaginacion/PagSgte[.!=0]">
                        javascript:IrAPag('<xsl:value-of select="$MyPaginacion/PagSgte"/>','<xsl:value-of select="$tipo"/>');
                      </xsl:when>
                      <xsl:otherwise>#</xsl:otherwise>
                    </xsl:choose>
                  </xsl:attribute>
                  <span class="glyphicon glyphicon-menu-right" aria-hidden="true"></span>
                </a>
              </li>
            </ul>
          </div>
        </xsl:if>
      <!--</div>
    </div>-->
  </xsl:template>
  <xsl:template name="DibujaExportaExcel">
    <xsl:param name="tipo"/>
    <a href="#" class="pull-right" title="Exportar Grilla Excel">
      <xsl:attribute name="href">
        javascript:ExportarExcel('<xsl:value-of select="$tipo"/>');
      </xsl:attribute>
      <img src="../img/excel.png" width="40" height="40" class="img-responsive"/>
    </a>
  </xsl:template>
</xsl:stylesheet>
