<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform"  >
  <xsl:output method="html" version="1.0" encoding="utf-8" indent="yes" omit-xml-declaration="yes" standalone="yes" doctype-public="-//W3C//DTD XHTML 1.1//EN" doctype-system="http://www.w3.org/TR/xhtml11/DTD/xhtml11.dtd" ></xsl:output>
  <xsl:include href="GrillaPaginada.xsl"/>
  <xsl:template match="/">
    <div class="row">
      <div class="col-lg-12 col-md-12 col-xs-12">
        <div class="panel panel-default">
            <div class="panel-body">
              <div class="dataTable_wrapper">
                  <table width="100%" class="table table-striped table-bordered table-hover" id="dataTables-example">
                    <thead>
                        <tr>
                          <th>Equipo</th>
                          <th>Titulares Promedio</th>
                          <th>Cierre Empresas</th>
                          <th>Cierre PPE</th>
                        </tr>
                    </thead>
                    <tbody>
                      <xsl:for-each select="//app/INICIO_CANTIDAD/Datos/row">
                         <tr class="odd gradeX">
                          <td class="text-center">
                            <xsl:choose>
                                <xsl:when test="//app/perID = 'CONV_ROL_COORDINADOR'">
                                     <xsl:value-of select="@SUPERVISOR" />-<xsl:value-of select="@NOMBRE"/>
                                </xsl:when>
                                <xsl:otherwise>
                                    <a id="" onclick="">
                                      <xsl:attribute name="onclick">IrDetalleCantidad('<xsl:value-of select="@SUPERVISOR" />','<xsl:value-of select="@NOMBRE"/>');
                                      </xsl:attribute>
                                      <xsl:value-of select="@SUPERVISOR" />-<xsl:value-of select="@NOMBRE"/>
                                    </a>
                                </xsl:otherwise>
                              </xsl:choose>
                            </td>
                          <td class="text-center success"><xsl:value-of select="@PRO"/></td>
                          <td class="text-center warning"><xsl:value-of select="@EMP"/></td>
                          <td class="text-center danger"><xsl:value-of select="@PPE"/></td>
                        </tr>
                      </xsl:for-each>
                        <tr class="odd gradeX" style="background-color: #205081; color: #FFF; ">
                          <td class="text-center">TOTAL</td>
                          <td class="text-center"><xsl:value-of select="//app/INICIO_CANTIDAD/Datos/row1/@T_PRO"/></td>
                          <td class="text-center"><xsl:value-of select="//app/INICIO_CANTIDAD/Datos/row1/@T_EMP"/></td>
                          <td class="text-center"><xsl:value-of select="//app/INICIO_CANTIDAD/Datos/row1/@T_PPE"/></td>
                        </tr>
                    </tbody>
                  </table>
                  <div class="row">
                    <div class="col-lg-12 text-center">
                      <div class="col-sm-2 pull-left">
                        <xsl:call-template name="DibujaRegistrosPorPagina">
                          <xsl:with-param name="tipo" select="//app/Accion" />
                        </xsl:call-template>
                      </div>
                      <div class="col-sm-6 text-center">
                        <!--<button type="button" class="btn" style="background-color: #205081; color: #FFF;" onclick="javascript:volver();">
                          <i class="fa fa-reply" aria-hidden="true"></i> Volver
                        </button>-->
                      </div>
                      <div class="col-sm-4 btn-group pull-right">
                        <xsl:call-template name="DibujaPaginacion">
                          <xsl:with-param name="tipo" select="//app/Accion" />
                        </xsl:call-template>
                      </div>
                    </div>
                  </div>
              </div>
            </div>
        </div>
      </div>
  </div>
  </xsl:template>
</xsl:stylesheet>
