<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform"  >
  <xsl:output method="html" version="1.0" encoding="utf-8" indent="yes" omit-xml-declaration="yes" standalone="yes" doctype-public="-//W3C//DTD XHTML 1.1//EN" doctype-system="http://www.w3.org/TR/xhtml11/DTD/xhtml11.dtd" ></xsl:output>
  <xsl:include href="GrillaPaginada.xsl"/>
  <xsl:template match="/">
    <div class="row">
      <div class="col-lg-12 col-md-12 col-xs-12">
        <div class="panel panel-default">
            <div class="panel-body">
              <div class="dataTable_wrapper">
                  <table width="100%" class="table table-striped table-bordered table-hover" id="dataTables-example">
                    <thead>
                        <tr>
                          <th>Coordinador</th>
                          <th>Contralía</th>
                          <th>DTA-CRM</th>
                          <th>Total</th>
                        </tr>
                    </thead>
                    <tbody>
                       <xsl:for-each select="//app/DETALLE_PENDIENTES/Datos/row">
                          <tr class="odd gradeX">
                            <td class="text-center"><xsl:value-of select="@COOR" /> - <xsl:value-of select="@NOMBRE_COOR"/>
                            </td>
                            <td class="text-center">
                              <a  class="">
                                <xsl:attribute name="onclick">DetallePendienteCM('<xsl:value-of select="@COOR" />');
                                </xsl:attribute>
                                <xsl:value-of select="@CON"/>
                              </a>
                            </td>
                            <td class="text-center">
                              <a  class="">
                                <xsl:attribute name="onclick">DetallePendienteDTA('<xsl:value-of select="@COOR" />');
                                </xsl:attribute>
                                <xsl:value-of select="@DTA"/>
                              </a>
                            </td>
                            <td class="text-center"><xsl:value-of select="@CON + @DTA"/></td>
                          </tr>
                        </xsl:for-each>
                      <xsl:for-each select="//app/DETALLE_PENDIENTES/Datos/row1">
                         <tr class="odd gradeX" style="background-color: #205081; color: #FFF; ">
                          <td class="text-center">TOTAL</td>
                          <td class="text-center"><xsl:value-of select="@T_CON"/></td>
                          <td class="text-center"><xsl:value-of select="@T_DTA"/></td>
                          <td class="text-center"><xsl:value-of select="@T_CON + @T_DTA"/></td>
                        </tr>
                      </xsl:for-each>
                    </tbody>
                  </table>
                  <div class="row">
                    <div class="col-lg-12 text-center">
                      <div class="col-sm-2 pull-left">
                        <xsl:call-template name="DibujaRegistrosPorPagina">
                          <xsl:with-param name="tipo" select="//app/Accion" />
                        </xsl:call-template>
                      </div>
                      <div class="col-sm-6 text-center">
                        <button type="button" class="btn" style="background-color: #205081; color: #FFF;" onclick="javascript:volver();">
                          <i class="fa fa-reply" aria-hidden="true"></i> Volver
                        </button>
                      </div>
                      <div class="col-sm-4 btn-group pull-right">
                        <xsl:call-template name="DibujaPaginacion">
                          <xsl:with-param name="tipo" select="//app/Accion" />
                        </xsl:call-template>
                      </div>
                    </div>
                  </div>
              </div>
            </div>
        </div>
      </div>
  </div>
  </xsl:template>
</xsl:stylesheet>
