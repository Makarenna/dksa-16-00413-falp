<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform"  >
  <xsl:output method="html" version="1.0" encoding="utf-8" indent="yes" omit-xml-declaration="yes" standalone="yes" doctype-public="-//W3C//DTD XHTML 1.1//EN" doctype-system="http://www.w3.org/TR/xhtml11/DTD/xhtml11.dtd" ></xsl:output>
  <xsl:template match="/">
    <xsl:choose>
          <xsl:when test="//app/Accion='ValidaLogin'">
           <input type="hidden" id="idRut" name="idRut" value="0">
             <xsl:attribute name="value">
               <xsl:choose>
                 <xsl:when test="count(//ValidaLogin/Datos/row) > 0">
                   <xsl:value-of select="//ValidaLogin/Datos/row[position()=1]/@Usuario"/>
                 </xsl:when>
                 <xsl:otherwise>0</xsl:otherwise>
               </xsl:choose>
             </xsl:attribute>
           </input>
           <input type="hidden" id="IdLogin" name="IdLogin" value="0">
             <xsl:attribute name="value">
               <xsl:choose>
                 <xsl:when test="count(//ValidaLogin/Datos/row) > 0">
                   <xsl:value-of select="//ValidaLogin/Datos/row[position()=1]/@IdUsuario"/>
                 </xsl:when>
                 <xsl:otherwise>0</xsl:otherwise>
               </xsl:choose>
             </xsl:attribute>
           </input>
           <input type="hidden" id="x" name="x" value="">
             <xsl:attribute name="value">
               <xsl:choose>
                 <xsl:when test="count(//ValidaLogin/Datos/row) > 0">
                   <xsl:choose>
                    <xsl:when test="//ValidaLogin/Datos/row/@Estado = 'BLOQUEADO'">0</xsl:when>
                    <xsl:when test="//ValidaLogin/Datos/row/@Estado = 'REINICIO'">2</xsl:when>
                     <xsl:when test="//ValidaLogin/Datos/row/@UserExists = 'false'">3</xsl:when>
                    <xsl:otherwise>1</xsl:otherwise>
                  </xsl:choose>
                 </xsl:when>
                 <xsl:otherwise>
                   <xsl:choose>
                    <xsl:when test="//ValidaLogin/Datos/row1/@Estado = 'BLOQUEADO'">0</xsl:when>
                    <xsl:when test="//ValidaLogin/Datos/row1/@Estado = 'REINICIO'">2</xsl:when>
                     <xsl:when test="//ValidaLogin/Datos/row/@UserExists = 'false'">3</xsl:when>
                    <xsl:otherwise>1</xsl:otherwise>
                  </xsl:choose>
                 </xsl:otherwise>
               </xsl:choose>
             </xsl:attribute>
           </input>
           <!-- begin modal intentos -->
            <div id="ModalClave" class="modal fade" role="dialog">
              <div class="modal-dialog modal-md">
                <!-- Modal content-->
                <div class="modal-content">
                  <div class="modal-header" style="background-color: #34495e; color: #fff;">
                    <a class="close" data-dismiss="modal">×</a>
                    <h4 class="modal-title">Error de inicio de sesión</h4>
                  </div>
                  <div class="modal-body">
                    <form action="" method="post">
                      <div class="form-group">
                        <p style="padding-left:15px; padding-right:15px; text-align: center;">
                          <xsl:choose>
                            <xsl:when test="//ValidaLogin/Datos/row1/@Estado = 'BLOQUEADO'">
                              <xsl:text>Su usuario está bloqueado por intentos erróneos, debe contactarse con el Administrador. </xsl:text>
                            </xsl:when>
                            <xsl:otherwise>
                              <xsl:choose>
                                <xsl:when test="//ValidaLogin/Datos/row1/@Intento = 1">
                                  <xsl:text>Primer intento de ingreso erróneo. Quedan 2 intentos. </xsl:text>
                                </xsl:when>
                                <xsl:when test="//ValidaLogin/Datos/row1/@Intento = 2">
                                  <xsl:text>Segundo intento de ingreso erróneo. Queda 1 intentos. </xsl:text>
                                </xsl:when>
                                <xsl:otherwise>
                                  <xsl:text>* Su usuario está bloqueado por intentos erróneos, debe contactarse con el Administrador. </xsl:text>
                                </xsl:otherwise>
                               </xsl:choose>
                             </xsl:otherwise>
                          </xsl:choose>
                        </p>
                      </div>
                    </form>
                  </div>
                  <div class="modal-footer">
                    <a type="button" class="btn btn-default" data-dismiss="modal">Salir</a>
                  </div>
                </div>
              </div>
            </div>
           <!-- end modal intentos -->
           <xsl:choose>
             <xsl:when test="//ValidaLogin/Datos/row/@TotalInstituciones > 1">
               <!-- editar para más de una institución (foreach) -->
               <input type="hidden" id="IdInstitucion" name="IdInstitucion" value="0"></input>
             </xsl:when>
             <xsl:otherwise>
               <input type="hidden" id="IdInstitucion" name="IdInstitucion" value="">
                 <!-- //ValidaLogin/Datos/row1/@IdInstitucion -->
                 <xsl:attribute name="value">
                    <xsl:value-of select="//ValidaLogin/Datos/row1/@IdInstitucion" />
                 </xsl:attribute>
               </input>
             </xsl:otherwise>
           </xsl:choose>
           <xsl:choose>
             <xsl:when test="//ValidaLogin/Datos/row/@TotalSedes > 1">
               <!-- begin modal institución -->
               <div class="modal modal-flex fade" id="ModalInstitucion" tabindex="-1" role="dialog" aria-labelledby="flexModalLabel" aria-hidden="true">
                 <div class="modal-dialog">
                   <div class="modal-content">
                     <div class="modal-header">
                       <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
                       <h4 class="modal-title" id="flexModalLabel">Selecciona la Institución y sede de conexión:</h4>
                     </div>
                     <div class="modal-body" id="InstSede">
                       <div class="form-group">
                        <div class="row">
                          <label class="col-sm-2 control-label">Instituci&#243;n:</label>
                          <div class="col-sm-10">
                            <select id="cmbInstitucion" name="cmbInstitucion" class="form-control" alias="Institucion" Req="S">
                              <xsl:for-each select="//ValidaLogin/Datos/row1">
                                <option>
                                  <xsl:attribute name="value">
                                    <xsl:value-of select="@IdInstitucion" />
                                  </xsl:attribute>
                                  <xsl:value-of select="@NombreInstitucion" />
                                </option>
                              </xsl:for-each>
                            </select>
                          </div>
                        </div>
                        <br/>
                        <div class="row">
                          <label class="col-sm-2 control-label">Sede:</label>
                          <div class="col-sm-10">
                            <select id="cmbSede" name="cmbSede" class="form-control" alias="Sede" Req="S">
                              <option idpadre="" value="">Seleccione...</option>
                              <xsl:for-each select="//ValidaLogin/Datos/row2">
                                <option idpadre="">
                                  <xsl:attribute name="value">
                                    <xsl:value-of select="@IdSede" />
                                  </xsl:attribute>
                                  <xsl:value-of select="@NombreSede" />
                                </option>
                              </xsl:for-each>
                            </select>
                          </div>
                        </div>
                       </div>
                     </div>
                     <div class="modal-footer">
                       <a id="btnAccion" name="btnAccion" class="btn btn-default">Ingresar</a>
                       <a class="btn btn-default" data-dismiss="modal">Salir</a>
                     </div>
                   </div>
                 </div>
               </div>
               <!-- end modal institución -->
               <input type="hidden" id="IdSede" name="IdSede" value="0"></input>
             </xsl:when>
             <xsl:otherwise>
               <input type="hidden" id="IdSede" name="IdSede" value="">
                 <xsl:attribute name="value">
                    <xsl:value-of select="//ValidaLogin/Datos/row2/@IdSede" />
                 </xsl:attribute>
               </input>
             </xsl:otherwise>
           </xsl:choose>
          </xsl:when>
      
          <xsl:when test="//app/Accion='ValidaInstSede'">
            <input type="hidden" id="IDUSUPERFIL" name="IDUSUPERFIL">
              <xsl:attribute name="value">
                <xsl:choose>
                  <xsl:when test="count(//ValidaInstSede/Datos/row) > 0">
                    <xsl:value-of select="//ValidaInstSede/Datos/row[position()=1]/@IDUSUPERFIL"/>
                  </xsl:when>
                  <xsl:otherwise>0</xsl:otherwise>
                </xsl:choose>
              </xsl:attribute>
            </input>
          </xsl:when>

    </xsl:choose>
  </xsl:template>
</xsl:stylesheet>
