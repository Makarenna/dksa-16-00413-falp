//jQuery_1_9(function() {

//    //jQuery_1_9('#side-menu').metisMenu();
    
//});

//Loads the correct sidebar on window load,
//collapses the sidebar on window resize.
// Sets the min-height of #page-wrapper to window size
jQuery_1_9(function() {
    jQuery_1_9(window).bind("load resize", function() {
        var topOffset = 50;
        var width = (this.window.innerWidth > 0) ? this.window.innerWidth : this.screen.width;
        if (width < 768) {
            jQuery_1_9('div.navbar-collapse').addClass('collapse');
            topOffset = 100; // 2-row-menu
        } else {
            jQuery_1_9('div.navbar-collapse').removeClass('collapse');
        }

        var height = ((this.window.innerHeight > 0) ? this.window.innerHeight : this.screen.height) - 1;
        height = height - topOffset;
        if (height < 1) height = 1;
        if (height > topOffset) {
            jQuery_1_9("#page-wrapper").css("min-height", (height) + "px");
        }
    });

    var url = window.location;
    // var element = jQuery_1_9('ul.nav a').filter(function() {
    //     return this.href == url;
    // }).addClass('active').parent().parent().addClass('in').parent();
    var element = jQuery_1_9('ul.nav a').filter(function() {
     return this.href == url;
    }).addClass('active').parent();

    while(true){
        if (element.is('li')){
            element = element.parent().addClass('in').parent();
        } else {
            break;
        }
    }
});
