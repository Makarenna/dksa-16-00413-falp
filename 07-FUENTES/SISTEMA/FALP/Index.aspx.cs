﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

using System.Xml;
using System.Xml.Xsl;
using System.IO;
using System.Data.SqlClient;

using library.xml;
using library.common;
using library.Security;

public partial class Index : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        XmlDocument XmlFinal = null, XmlAuxA = null;

        XmlFinal = XmlHelper.CrearPaginaEsquemaAll(XmlFinal, this);

        string usuario = XmlHelper.strGetNodoTexto(XmlFinal, "//requestFormPost/usuario");

        XmlDocument XmlParametrosSql = XmlHelper.StpCrearParametros();
        XmlHelper.StpAgregarNodoParametros(XmlParametrosSql, "Tipo", "Crear");
        XmlHelper.StpAgregarNodoParametros(XmlParametrosSql, "usuario", usuario);
        XmlHelper.StpAgregarNodoParametros(XmlParametrosSql, "ses_remote_addr", Request.ServerVariables["REMOTE_ADDR"].ToString());
        XmlHelper.StpAgregarNodoParametros(XmlParametrosSql, "ses_remote_user", Request.ServerVariables["REMOTE_USER"].ToString());
        XmlHelper.StpAgregarNodoParametros(XmlParametrosSql, "ses_remote_host", Request.ServerVariables["REMOTE_HOST"].ToString());
        XmlHelper.StpAgregarNodoParametros(XmlParametrosSql, "ses_user_agent", Request.ServerVariables["HTTP_USER_AGENT"].ToString());
        XmlHelper.StpAgregarNodoParametros(XmlParametrosSql, "ses_language", Request.ServerVariables["HTTP_ACCEPT_LANGUAGE"].ToString());
        XmlHelper.StpAgregarNodoParametros(XmlParametrosSql, "ses_referer", Request.ServerVariables["HTTP_REFERER"].ToString());
        XmlHelper.StpAgregarNodoParametros(XmlParametrosSql, "ses_cookie", (Request.ServerVariables["HTTP_COOKIE"] == null ? "" : Request.ServerVariables["HTTP_COOKIE"].ToString()));
        XmlHelper.StpAgregarNodoParametros(XmlParametrosSql, "ses_host", Request.ServerVariables["HTTP_HOST"].ToString());

        XmlDocument _XmlLogin = XmlHelper.CrearXmlDocumentConGetRow("Login", new cBD().Ejecuta_PA_Oracle_Historico(XmlParametrosSql.OuterXml, "PCK_CONV_DKERNEL.DK_VALIDA_USUARIO"));

        string sesID = XmlHelper.strGetNodoGetAttributo(_XmlLogin, "//Login/Datos/row", "SESID");
        string usuID = XmlHelper.strGetNodoGetAttributo(_XmlLogin, "//Login/Datos/row", "USUID");
        string perID = XmlHelper.strGetNodoGetAttributo(_XmlLogin, "//Login/Datos/row", "PERID");

        if (!sesID.Equals(string.Empty))
        {
            Session.Add("sesID", sesID);
            Session.Add("usuID", usuID);
            Session.Add("perID", perID);

            string url = sessionHelper.GetSession("url", XmlHelper.strGetNodoTexto(XmlFinal, "//requestFormPost/url"), this);

            if (url != "")
            {
                Response.Redirect(url);
            }
            else
            {
                Response.Redirect("pages/inicio.aspx");
                //System.Web.UI.AttributeCollection aCol = iframe.Attributes;
                //aCol.Add("src", "pages/inicio.aspx");
            }
        }
    }
}