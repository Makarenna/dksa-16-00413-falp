﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="Acceso.aspx.cs" Inherits="Acceso" %>

<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta charset="utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <meta name="description" content="" />
    <meta name="author" content="" />
    <meta http-equiv="X-UA-Compatible" content="IE=Edge" />
    <title>..: FALP :..</title>
    <script src="http://ajax.googleapis.com/ajax/libs/jquery/1.10.2/jquery.min.js"></script>
    <script type="text/javascript">
        var jQuery_1_9 = $.noConflict(true);
    </script>
    <script src="Acceso.js"></script>
</head>
<body>
    <form method="post" role="form" name="form" id="form">
        <input type="hidden" name="Accion" id="Accion" />
        <div class="form-group">
            <input type="text" placeholder="Usuario" name="txtUsuario" id="txtUsuario" autofocus="true" alias="usuario" value="" />
            <a id="btnIngresar" onclick="Ingresar();">Ejecutar</a>
        </div>
    </form>    
</body>
</html>
